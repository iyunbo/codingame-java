package io.github.iyunbo.container.num.items;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class MemorizationTest {

	private final Memorization solution = new Memorization();

	@Test
	public void test_base_case() {
		assertEquals(List.of(2), solution.numberOfItems("*|*|*|", List.of(1), List.of(6)));
		assertEquals(List.of(0), solution.numberOfItems("*|*|", List.of(1), List.of(3)));
		assertEquals(List.of(2, 3), solution.numberOfItems("|**|*|*", List.of(1, 1), List.of(5, 6)));
	}

	@Test
	public void test_special_case() {
		assertEquals(List.of(0), solution.numberOfItems("****", List.of(1), List.of(3)));
		assertEquals(List.of(0), solution.numberOfItems("*|**", List.of(1), List.of(3)));
		assertEquals(List.of(0), solution.numberOfItems("||||", List.of(1), List.of(3)));
		assertEquals(List.of(), solution.numberOfItems("", List.of(), List.of()));
	}
}