package io.github.iyunbo.container.with.most.water;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	final Solution solution = new Solution();

	@Test
	public void test_basic_case() {
		assertEquals(2, solution.maxArea(new int[]{1, 2, 1}));
	}

	@Test
	public void test_empty_height() {
		assertEquals(0, solution.maxArea(new int[0]));
	}

	@Test
	public void test_small_container() {
		assertEquals(1, solution.maxArea(new int[]{1, 1}));
	}

	@Test
	public void test_full_container() {
		assertEquals(16, solution.maxArea(new int[]{4, 3, 2, 1, 4}));
	}

	@Test
	public void test_typical_example() {
		assertEquals(49, solution.maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}));
	}
}