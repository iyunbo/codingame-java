package io.github.iyunbo.container.parenthese.valid;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case(){
		assertTrue(solution.isValid("{}[]()"));
		assertTrue(solution.isValid("({}[])"));
		assertFalse(solution.isValid("({}[]"));
	}

}