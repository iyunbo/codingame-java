package io.github.iyunbo.connected.largest.item.association;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;


class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		List<String> actual = solution.largestItemAssociation(List.of(new PairString("item1", "item2"), new PairString("item3", "item4"), new PairString("item4", "item5")));
		assertEquals(List.of("item3", "item4", "item5"), actual);
	}
}