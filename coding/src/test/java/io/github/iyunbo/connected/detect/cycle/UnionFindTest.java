package io.github.iyunbo.connected.detect.cycle;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class UnionFindTest {

	private final UnionFind solution = new UnionFind();

	@Test
	public void test_base_cases() {
		assertTrue(solution.hasCircle(new int[][]{{0, 1}, {1, 2}, {2, 0}}, 3));
		assertTrue(solution.hasCircle(new int[][]{{0, 1}, {1, 2}, {2, 3}, {1, 3}}, 4));
		assertFalse(solution.hasCircle(new int[][]{{0, 1}, {1, 2}, {2, 3}}, 4));
	}

}