package io.github.iyunbo.search.topk.frequent;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		int[] result = solution.topKFrequent(new int[]{1, 1, 1, 2, 2, 3}, 2);
		assertArrayEquals(new int[]{2, 1}, result);
	}

}