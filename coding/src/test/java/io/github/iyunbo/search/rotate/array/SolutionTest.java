package io.github.iyunbo.search.rotate.array;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(4, solution.search(new int[]{4, 5, 6, 7, 0, 1, 2}, 0));
	}

	@Test
	public void test_negative_case() {
		assertEquals(-1, solution.search(new int[]{4, 5, 6, 7, 0, 1, 2}, 8));
	}

	@Test
	public void test_special_case() {
		assertEquals(-1, solution.search(new int[]{1, 3}, 0));
	}

	@Test
	public void test_special_case_2() {
		assertEquals(1, solution.search(new int[]{8, 9, 2, 3, 4}, 9));
	}

}