package io.github.iyunbo.search.merge.intervals;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		int[][] result = solution.merge(new int[][]{{1, 4}, {0, 2}, {3, 5}});
		assertArrayEquals(new int[][]{{0, 5}}, result);
	}

	@Test
	public void test_base_case2() {
		int[][] result = solution.merge(new int[][]{{1, 3}, {2, 6}, {8, 10}, {15, 18}});
		assertArrayEquals(new int[][]{{1, 6}, {8, 10}, {15, 18}}, result);
	}

	@Test
	public void test_equal_case(){
		int[][] result = solution.merge(new int[][]{{1, 4}, {4, 5}});
		assertArrayEquals(new int[][]{{1, 5}}, result);
	}

}