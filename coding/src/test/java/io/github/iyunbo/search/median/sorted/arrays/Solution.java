package io.github.iyunbo.search.median.sorted.arrays;

public class Solution {
	public double findMedianSortedArrays(int[] nums1, int[] nums2) {
		if (nums1.length > nums2.length)
			return findMedianSortedArrays(nums2, nums1);

		//x is the array with shorter length
		// partition is the position from which an array is splitted by two
		// we control x's partition, and the calculated y partition will never be negative
		final int xMax = nums1.length;
		final int yMax = nums2.length;

		int low = 0;
		int high = xMax;

		// T = O(log(min(X,Y))
		// S = O(1)
		while (low <= high) {
			// partition is the start index of the right part
			int partitionX = (low + high) / 2;
			int partitionY = (xMax + yMax + 1) / 2 - partitionX;

			int maxLeftX = partitionX == 0 ? Integer.MIN_VALUE : nums1[partitionX - 1];
			int maxLeftY = partitionY == 0 ? Integer.MIN_VALUE : nums2[partitionY - 1];
			int minRightX = partitionX == xMax ? Integer.MAX_VALUE : nums1[partitionX];
			int minRightY = partitionY == yMax ? Integer.MAX_VALUE : nums2[partitionY];

			if (maxLeftX <= minRightY && maxLeftY <= minRightX) {
				if ((xMax + yMax) % 2 == 0) {
					return (Integer.max(maxLeftX, maxLeftY) + Integer.min(minRightX, minRightY)) / 2.0;
				} else {
					return Integer.max(maxLeftX, maxLeftY);
				}
				/*
				[5,[6]]
				[1,2,3,[4],7]
				 */
			} else if (maxLeftX > minRightY) {
				// too far to the right, move x to left
				high = partitionX - 1;

			} else {
				// too far to the left, move x to right
				low = partitionX + 1;
			}
		}

		throw new IllegalArgumentException("Somthing went wrong, it's probablely because of the code!");

	}
}
