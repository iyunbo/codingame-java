package io.github.iyunbo.search.fresh.promotion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		String[][] codeList = {{"apple", "apple"}, {"banana", "anything", "banana"}};
		String[] shoppingCart = {"orange", "apple", "apple", "banana", "orange", "banana"};
		assertEquals(1, solution.winPrize(codeList, shoppingCart));
	}

	@Test
	public void test_base_case2() {
		String[][] codeList = {{"apple", "apple"}, {"banana", "anything", "banana"}};
		String[] shoppingCart = {"banana", "orange", "banana", "apple", "apple"};
		assertEquals(0, solution.winPrize(codeList, shoppingCart));
	}

}