package io.github.iyunbo.search.median.sorted.arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(2.5, solution.findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4}));
		assertEquals(2, solution.findMedianSortedArrays(new int[]{1, 3}, new int[]{2}));
	}

	@Test
	public void test_base_case2() {
		assertEquals(4, solution.findMedianSortedArrays(new int[]{5, 6}, new int[]{1, 2, 3, 4, 7}));
	}

}