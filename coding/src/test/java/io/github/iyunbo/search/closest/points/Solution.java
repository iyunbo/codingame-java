package io.github.iyunbo.search.closest.points;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class Solution {
	public int[][] kClosest(int[][] points, int K) {
		if (points.length == K) return points;

		// T = O(N)
		final Map<int[], Integer> distances = new LinkedHashMap<>();
		for (int[] point : points) {
			distances.put(point, distant(point));
		}

		// T = O(N * logK)
		final Queue<int[]> heap = new PriorityQueue<>(Comparator.comparingInt(distances::get).reversed());
		for (int[] point : points) {
			heap.add(point);
			if (heap.size() > K) heap.poll();
		}

		// T = O(K)
		final int[][] result = new int[K][];
		int i = 0;
		while (!heap.isEmpty()) {
			result[i++] = heap.poll();
		}

		return result;

	}

	private int distant(int[] point) {
		return point[0] * point[0] + point[1] * point[1];
	}
}
