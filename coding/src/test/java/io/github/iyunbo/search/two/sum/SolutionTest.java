package io.github.iyunbo.search.two.sum;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {
	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertArrayEquals(new int[]{1, 2}, solution.twoSum(new int[]{2, 7, 11, 15}, 9));
	}

	@Test
	public void test_base_case2() {
		assertArrayEquals(new int[]{2, 3}, solution.twoSum(new int[]{5, 25, 75}, 100));
	}

	@Test
	public void test_base_case3() {
		assertArrayEquals(new int[]{3, 6}, solution.twoSum(new int[]{3, 24, 50, 79, 88, 150, 345}, 200));
	}
}