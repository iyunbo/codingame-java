package io.github.iyunbo.search.topk.frequent;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class Solution {
	public int[] topKFrequent(int[] nums, int k) {
		if (nums.length == k) return nums;

		// T = O(N)
		final Map<Integer, Integer> counter = new LinkedHashMap<>();
		for (int num : nums) {
			counter.putIfAbsent(num, 0);
			counter.put(num, counter.get(num) + 1);
		}

		final Queue<Integer> heap = new PriorityQueue<>(Comparator.comparingInt(counter::get));

		// T = O(N * logK)
		for (Integer i : counter.keySet()) {
			heap.add(i);
			if (heap.size() > k) heap.poll();
		}

		// T = O(K)
		final int[] result = new int[k];
		int n = 0;
		while (!heap.isEmpty()) {
			result[n++] = heap.poll();
		}
		return result;
	}
}
