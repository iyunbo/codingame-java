package io.github.iyunbo.search.closest.points;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		int[][] actual = solution.kClosest(new int[][]{{1, 3}, {-2, 2}}, 1);
		assertArrayEquals(new int[][]{{-2, 2}}, actual);
	}

}