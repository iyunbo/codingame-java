package io.github.iyunbo.search.meeting.rooms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_cases() {
		assertEquals(2, solution.minMeetingRooms(new int[][]{{0, 30}, {5, 10}, {15, 20}}));
		assertEquals(1, solution.minMeetingRooms(new int[][]{{7, 10}, {2, 4}}));
	}

	@Test
	public void test_special_case() {
		assertEquals(0, solution.minMeetingRooms(new int[][]{}));
	}

	@Test
	public void test_special_case_with_equal_interval() {
		assertEquals(2, solution.minMeetingRooms(new int[][]{{1, 5}, {8, 9}, {8, 9}}));
	}

	@Test
	public void test_base_case2(){
		assertEquals(3, solution.minMeetingRooms(new int[][]{{6, 15}, {13, 20}, {6, 17}}));
	}

	@Test
	public void test_base_case3(){
		assertEquals(2, solution.minMeetingRooms(new int[][]{{0, 30}, {5, 10}, {15, 20}}));
	}
}