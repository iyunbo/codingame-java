package io.github.iyunbo.arrays.merge.k.linkedlist;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;

import org.junit.jupiter.api.Test;

import io.github.iyunbo.arrays.two.numbers.ListNode;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_one_list() {
		ListNode merged = solution.mergeKLists(new ListNode[]{ListNode.of(0, 1, 2)});
		assertEquals(List.of(0, 1, 2), merged.toList(false));
	}

	@Test
	public void test_null_list() {
		ListNode merged = solution.mergeKLists(new ListNode[]{null, null, null});
		assertNull(merged);
	}

	@Test
	public void test_two_list() {
		ListNode merged = solution.mergeKLists(new ListNode[]{ListNode.of(0, 1, 2), ListNode.of(1, 2, 3)});
		assertEquals(List.of(0, 1, 1, 2, 2, 3), merged.toList(false));
	}

	@Test
	public void test_3_list() {
		ListNode merged = solution.mergeKLists(new ListNode[]{ListNode.of(0, 1, 2), ListNode.of(1, 2, 3), ListNode.of(2, 3, 4)});
		assertEquals(List.of(0, 1, 1, 2, 2, 2, 3, 3, 4), merged.toList(false));
	}

	@Test
	public void test_lists_with_null() {
		ListNode merged = solution.mergeKLists(new ListNode[]{null, ListNode.of(0, 1, 2), ListNode.of(1, 2, 3), null, ListNode.of(2, 3, 4), null});
		assertEquals(List.of(0, 1, 1, 2, 2, 2, 3, 3, 4), merged.toList(false));
	}

	@Test
	public void test_5_list() {
		ListNode merged = solution.mergeKLists(new ListNode[]{
				ListNode.of(0, 1, 2),
				ListNode.of(1, 2, 3),
				ListNode.of(2, 3, 4),
				ListNode.of(5, 6, 7),
				ListNode.of(8, 9, 10, 11, 12)});
		assertEquals(List.of(0, 1, 1, 2, 2, 2, 3, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12), merged.toList(false));
	}

}