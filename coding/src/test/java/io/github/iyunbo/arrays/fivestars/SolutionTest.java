package io.github.iyunbo.arrays.fivestars;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(3, solution.fiveStarReviews(List.of(List.of(4, 4), List.of(1, 2), List.of(3, 6)), 77));
	}

}