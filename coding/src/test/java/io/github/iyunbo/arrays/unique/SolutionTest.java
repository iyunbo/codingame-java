package io.github.iyunbo.arrays.unique;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(6, solution.firstUniqChar("abddfdslfdab"));
	}

}