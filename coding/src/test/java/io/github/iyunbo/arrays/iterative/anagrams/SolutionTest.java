package io.github.iyunbo.arrays.iterative.anagrams;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case(){
		final List<List<String>> anagrams = solution.groupAnagrams(new String[]{"eat", "tea", "tan", "ate", "nat", "bat"});
		assertEquals(List.of(
				List.of("eat","tea","ate"),
				List.of("bat"),
				List.of("tan","nat")
				), anagrams);
	}
}