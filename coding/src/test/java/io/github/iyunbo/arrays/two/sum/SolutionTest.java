package io.github.iyunbo.arrays.two.sum;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();
	private final FasterSolution fasterSolution = new FasterSolution();

	@Test
	public void should_throw_if_empty_input() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			solution.twoSum(new int[0], 2);
			fasterSolution.twoSum(new int[0], 2);
		});
	}

	@Test
	public void should_return_unique_answer_if_only_two_numbers() {
		assertArrayEquals(new int[]{0, 1}, solution.twoSum(new int[]{1, 1}, 2));
		assertArrayEquals(new int[]{0, 1}, fasterSolution.twoSum(new int[]{1, 1}, 2));
	}

	@Test
	public void should_return_answer_on_normal_case() {
		assertArrayEquals(new int[]{0, 5}, solution.twoSum(new int[]{1, 2, 3, 4, 5, 6}, 7));
		assertArrayEquals(new int[]{0, 5}, fasterSolution.twoSum(new int[]{1, 2, 3, 4, 5, 6}, 7));
	}

	@Test
	public void should_return_answer_when_number_is_half_of_target() {
		assertArrayEquals(new int[]{1, 2}, solution.twoSum(new int[]{3, 2, 4}, 6));
		assertArrayEquals(new int[]{1, 2}, fasterSolution.twoSum(new int[]{3, 2, 4}, 6));
	}

	@Test
	public void should_answer_correctly_on_large_array() {
		int[] nums = IntStream.range(0, 10_000_000).toArray();
		assertArrayEquals(new int[]{1, 10_000_000 - 1}, solution.twoSum(nums, 10_000_000));
		assertArrayEquals(new int[]{1, 10_000_000 - 1}, fasterSolution.twoSum(nums, 10_000_000));
	}
}