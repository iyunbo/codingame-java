package io.github.iyunbo.arrays.atoi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {
	final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(1, solution.myAtoi("1"));
		assertEquals(42, solution.myAtoi("42"));
	}

	@Test
	public void test_negative_number() {
		assertEquals(-42, solution.myAtoi("-42"));
	}

	@Test
	public void should_ignore_suffix_non_digit() {
		assertEquals(-42, solution.myAtoi("-42xxxx"));
	}

	@Test
	public void should_ignore_prefix_empty() {
		assertEquals(-42, solution.myAtoi("   -42xxxx"));
	}

	@Test
	public void should_invalidate_alpha_prefix() {
		assertEquals(0, solution.myAtoi("abc  -42xxxx"));
	}

	@Test
	public void should_manage_out_of_range() {
		assertEquals(Integer.MIN_VALUE, solution.myAtoi("-91283472332"));
		assertEquals(Integer.MAX_VALUE, solution.myAtoi("21474836460"));
	}

	@Test
	public void should_ignore_everything_after_alpha() {
		assertEquals(3, solution.myAtoi("3.14159"));
	}

	@Test
	public void should_consider_plus() {
		assertEquals(1, solution.myAtoi("+1"));
	}

	@Test
	public void should_detect_plus_before_minus() {
		assertEquals(0, solution.myAtoi("+-1"));
	}

	@Test
	public void should_ignore_second_number() {
		assertEquals(0, solution.myAtoi("00000-42a1234"));
	}

	@Test
	public void should_detect_space_between_digit() {
		assertEquals(0, solution.myAtoi("   +0 123"));
	}

	@Test
	public void should_remove_suffix() {
		assertEquals(-13, solution.myAtoi("-13+8"));
	}

	@Test
	public void should_accept_empty() {
		assertEquals(0, solution.myAtoi(""));
		assertEquals(0, solution.myAtoi(" "));
	}

}