package io.github.iyunbo.arrays.linkedlist;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		Node copied = solution.copyRandomList(Node.of(1, 2, 3, 4, 5, 6));
		assertEquals(List.of(1, 2, 3, 4, 5, 6), copied.toInts());
	}

}