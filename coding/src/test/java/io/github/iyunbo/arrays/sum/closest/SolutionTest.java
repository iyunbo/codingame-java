package io.github.iyunbo.arrays.sum.closest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(2, solution.twoPointers(new int[]{-1, 2, 1, -4}, 1));
	}

	@Test
	public void test_two_sum_closest() {
		assertEquals(-1, solution.threeSumClosest(new int[]{2, 1, -4}, 0));
	}

	@Test
	public void test_base_case2() {
		assertEquals(-1, solution.threeSumClosest(new int[]{1, 1, -1, -1, 3}, -1));
	}

}