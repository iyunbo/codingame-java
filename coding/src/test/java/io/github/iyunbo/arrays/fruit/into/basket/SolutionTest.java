package io.github.iyunbo.arrays.fruit.into.basket;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(3, solution.totalFruit(new int[]{1, 2, 1}));
	}

	@Test
	public void test_simple_case() {
		assertEquals(3, solution.totalFruit(new int[]{0, 1, 2, 2}));
	}

	@Test
	public void test_simple_case2() {
		assertEquals(4, solution.totalFruit(new int[]{1, 2, 3, 2, 2}));
	}

	@Test
	public void test_simple_case3() {
		assertEquals(5, solution.totalFruit(new int[]{3, 3, 3, 1, 2, 1, 1, 2, 3, 3, 4}));
	}

}