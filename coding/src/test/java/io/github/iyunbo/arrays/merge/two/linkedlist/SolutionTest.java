package io.github.iyunbo.arrays.merge.two.linkedlist;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.github.iyunbo.arrays.two.numbers.ListNode;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void mergeTwoLists() {
		assertEquals(
				ListNode.of(1, 1, 2, 3, 4, 4).toList(false),
				solution.mergeTwoLists(ListNode.of(1, 2, 4), ListNode.of(1, 3, 4)).toList(false)
		);
	}

}