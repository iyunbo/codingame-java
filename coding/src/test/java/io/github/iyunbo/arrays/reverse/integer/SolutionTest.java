package io.github.iyunbo.arrays.reverse.integer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	private Solution solution = new Solution();

	@Test
	public void test_base_case(){
		assertEquals(321, solution.reverse(123));
	}

	@Test
	public void test_base_case2(){
		assertEquals(-321, solution.reverse(-123));
	}

	@Test
	public void test_edge_cases(){
		assertEquals(0, solution.reverse(Integer.MAX_VALUE));
		assertEquals(0, solution.reverse(Integer.MIN_VALUE));
		assertEquals(2147483641, solution.reverse(1463847412));
		assertEquals(0, solution.reverse(1463847422));
	}
}