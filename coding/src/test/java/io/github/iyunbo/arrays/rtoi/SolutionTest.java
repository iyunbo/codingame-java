package io.github.iyunbo.arrays.rtoi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


class SolutionTest {
	private final Solution solution = new Solution();

	@Test
	public void test_one() {
		assertEquals(1, solution.romanToInt("I"));
		assertEquals(3, solution.romanToInt("III"));
	}

	@Test
	public void test_five() {
		assertEquals(5, solution.romanToInt("V"));
	}

	@Test
	public void test_8() {
		assertEquals(8, solution.romanToInt("VIII"));
	}

	@Test
	public void test_four_nine() {
		assertEquals(4, solution.romanToInt("IV"));
		assertEquals(9, solution.romanToInt("IX"));
	}

	@Test
	public void test_bigger() {
		assertEquals(58, solution.romanToInt("LVIII"));
		assertEquals(1994, solution.romanToInt("MCMXCIV"));
	}
}