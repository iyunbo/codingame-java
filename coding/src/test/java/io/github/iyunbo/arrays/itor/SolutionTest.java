package io.github.iyunbo.arrays.itor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_one() {
		assertEquals("I", solution.intToRoman(1));
		assertEquals("III", solution.intToRoman(3));
	}

	@Test
	public void test_five() {
		assertEquals("V", solution.intToRoman(5));
		assertEquals("VIII", solution.intToRoman(8));
	}

	@Test
	public void test_four_nine() {
		assertEquals("IV", solution.intToRoman(4));
		assertEquals("IX", solution.intToRoman(9));
	}

	@Test
	public void test_bigger() {
		assertEquals("LVIII", solution.intToRoman(58));
		assertEquals("MCMXCIV", solution.intToRoman(1994));
	}

}