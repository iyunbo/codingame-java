package io.github.iyunbo.arrays.iterative.version.number;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {
	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(1, solution.compareVersion("1.1", "0.1"));
	}
}