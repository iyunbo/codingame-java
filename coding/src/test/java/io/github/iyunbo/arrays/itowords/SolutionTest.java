package io.github.iyunbo.arrays.itowords;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals("One Hundred Twenty Three Million Four Hundred Fifty Six Thousand Seven Hundred Eighty Nine", solution.numberToWords(123456789));
	}

	@Test
	public void test_special_cases() {
		assertEquals("Zero", solution.numberToWords(0));
		assertEquals("One Hundred", solution.numberToWords(100));
		assertEquals("One Thousand", solution.numberToWords(1000));
		assertEquals("One Million", solution.numberToWords(1000000));
		assertEquals("One Billion", solution.numberToWords(1000000000));
	}

}