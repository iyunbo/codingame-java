package io.github.iyunbo.arrays.reverse.kgroup;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.github.iyunbo.arrays.two.numbers.ListNode;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(ListNode.of(2, 1, 4, 3, 5).toList(false), solution.reverseKGroup(ListNode.of(1, 2, 3, 4, 5), 2).toList(false));
	}

	@Test
	public void test_simple_case() {
		assertEquals(ListNode.of(1).toList(false), solution.reverseKGroup(ListNode.of(1), 1).toList(false));
	}

	@Test
	public void test_tail_case() {
		assertEquals(ListNode.of(3, 2, 1, 4, 5).toList(false), solution.reverseKGroup(ListNode.of(1, 2, 3, 4, 5), 3).toList(false));
	}

}