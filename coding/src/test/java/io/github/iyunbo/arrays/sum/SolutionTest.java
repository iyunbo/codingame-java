package io.github.iyunbo.arrays.sum;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case_of_three_sum() {
		List<List<Integer>> list = solution.threeSum(new int[]{-1, 0, 1, 2, -1, -4});
		assertEquals(Arrays.asList(List.of(-1, -1, 2), List.of(-1, 0, 1)), list);
	}

	@Test
	public void test_base_case_2_of_three_sum() {
		List<List<Integer>> list = solution.threeSum(new int[]{-2, 0, 1, 1, 2});
		assertEquals(Arrays.asList(List.of(-2, 0, 2), List.of(-2, 1, 1)), list);
	}

	@Test
	public void test_empty_array() {
		List<List<Integer>> list = solution.threeSum(new int[0]);
		assertTrue(list.isEmpty());
	}

	@Test
	public void test_less_than_3_elements() {
		List<List<Integer>> list = solution.threeSum(new int[]{0});
		assertTrue(list.isEmpty());
		list = solution.threeSum(new int[]{0, 0});
		assertTrue(list.isEmpty());
	}

	@Test
	public void test_easy_case() {
		List<List<Integer>> list = solution.threeSum(new int[]{-1, 0, 1});
		assertEquals(List.of(List.of(-1, 0, 1)), list);
	}

	@Test
	public void test_easy_case2() {
		List<List<Integer>> list = solution.threeSum(new int[]{-1, 0, 1, 0});
		assertEquals(List.of(List.of(-1, 0, 1)), list);
	}

	@Test
	public void test_two_identical_list() {
		List<List<Integer>> list = solution.threeSum(new int[]{-2, 0, 0, 2, 2});
		assertEquals(List.of(List.of(-2, 0, 2)), list);
	}

	@Test
	public void test_3_zeros() {
		List<List<Integer>> list = solution.threeSum(new int[]{0, 0, 0});
		assertEquals(List.of(List.of(0, 0, 0)), list);
	}

	@Test
	public void should_use_only_once_first_num() {
		List<List<Integer>> list = solution.threeSum(new int[]{1, 2, -2, -1});
		assertTrue(list.isEmpty());
	}

}