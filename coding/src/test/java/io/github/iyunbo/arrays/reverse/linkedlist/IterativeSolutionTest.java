package io.github.iyunbo.arrays.reverse.linkedlist;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.github.iyunbo.arrays.two.numbers.ListNode;

class IterativeSolutionTest {

	private final IterativeSolution solution = new IterativeSolution();

	@Test
	public void should_return_as_is_if_only_one() {
		Assertions.assertEquals(ListNode.of(1).toList(), solution.reverseList(ListNode.of(1)).toList(false));
	}

	@Test
	public void should_return_as_is_if_null() {
		Assertions.assertNull(solution.reverseList(null));
	}

	@Test
	public void should_reverse_list() {
		Assertions.assertEquals(ListNode.of(1, 2, 3).toList(true), solution.reverseList(ListNode.of(1, 2, 3)).toList(false));
	}

	@Test
	public void should_reverse_longer_list() {
		ListNode list = ListNode.of(1, 2, 3, 4, 5, 6, 7, 8, 11, 1213, 44, 13, 5, 1, 3, 5, 5, 5, 5, 6, 76, 7, 7, 90, -1, 1);
		Assertions.assertEquals(list.toList(true), solution.reverseList(list).toList(false));
	}

}