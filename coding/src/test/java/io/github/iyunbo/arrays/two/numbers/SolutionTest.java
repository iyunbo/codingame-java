package io.github.iyunbo.arrays.two.numbers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_to_int() {
		assertEquals(List.of(1), new ListNode(1).toList());
		assertEquals(List.of(2, 1), new ListNode(1, new ListNode(2)).toList());
	}

	@Test
	public void test_equals() {
		assertEquals(List.of(3, 2, 1), ListNode.of(1, 2, 3).toList());
		assertEquals(List.of(3, 0, 0, 0, 1), ListNode.of(1, 0, 0, 0, 3).toList());
	}

	@Test
	public void test_base_case() {
		assertEquals(List.of(8, 9, 7), solution.addTwoNumbers(ListNode.of(2, 3, 4), ListNode.of(5, 6, 4)).toList());
	}

	@Test
	public void test_carrier_case() {
		assertEquals(List.of(8, 0, 7), solution.addTwoNumbers(ListNode.of(2, 4, 3), ListNode.of(5, 6, 4)).toList());
	}

	@Test
	public void test_both_zeros() {
		assertEquals(List.of(0), solution.addTwoNumbers(ListNode.of(0), ListNode.of(0)).toList());
	}

	@Test
	public void test_different_length() {
		assertEquals(List.of(1, 0, 0, 0, 9, 9, 9, 8), solution.addTwoNumbers(ListNode.of(9, 9, 9, 9, 9, 9, 9), ListNode.of(9, 9, 9, 9)).toList());
	}

	@Test
	public void test_238_plus_129() {
		assertEquals(List.of(3, 6, 7), solution.addTwoNumbers(ListNode.of(8, 3, 2), ListNode.of(9, 2, 1)).toList());
	}

	@Test
	public void test_199_plus_1() {
		assertEquals(List.of(2, 0, 0), solution.addTwoNumbers(ListNode.of(1), ListNode.of(9, 9, 1)).toList());
	}

	@Test
	public void test_5_plus_5() {
		assertEquals(List.of(1, 0), solution.addTwoNumbers(ListNode.of(5), ListNode.of(5)).toList());
	}

}