package io.github.iyunbo.arrays.missing.number;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	@Test
	public void test_base_case() {
		Solution solution = new Solution();
		assertEquals(1, solution.missingNumber(new int[]{0, 2, 3, 4, 5}));
	}

	@Test
	public void test_bitwize() {
		Bitwise solution = new Bitwise();
		assertEquals(1, solution.missingNumber(new int[]{0, 2, 3, 4, 5}));
	}

}