package io.github.iyunbo.dp.house.thief;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.maxSteal(new int[]{2, 5, 1, 3, 6, 2, 4})).isEqualTo(15);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.maxSteal(new int[]{2, 10, 14, 8, 1})).isEqualTo(18);
	}

}