package io.github.iyunbo.dp.rod.cutting;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DPTest {
	final DP solution = new DP();

	@Test
	public void test_base_case(){
		assertEquals(14, solution.cutRod(new int[]{1, 2, 3, 4, 5}, new int[]{2, 6, 7, 10, 13}, 5));
	}
}