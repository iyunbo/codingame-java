package io.github.iyunbo.dp.max.subarray;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class GreedyTest {

	private final Greedy solution = new Greedy();

	@Test
	public void test_base_case() {
		assertEquals(6, solution.maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
	}

}