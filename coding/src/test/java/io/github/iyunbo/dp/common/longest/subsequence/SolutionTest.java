package io.github.iyunbo.dp.common.longest.subsequence;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.longestCommonSubsequence("abdca", "cbda")).isEqualTo(3);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.longestCommonSubsequence("passport", "ppsspt")).isEqualTo(5);
	}

}