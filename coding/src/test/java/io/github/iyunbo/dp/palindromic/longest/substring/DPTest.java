package io.github.iyunbo.dp.palindromic.longest.substring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_case_cases() {
		assertEquals("bab", solution.longestPalindrome("babad"));
		assertEquals("bb", solution.longestPalindrome("cbbd"));
		assertEquals("a", solution.longestPalindrome("a"));
		assertEquals("a", solution.longestPalindrome("ac"));
	}

	@Test
	public void test_simple_cases() {
		assertEquals("bdb", solution.longestPalindrome("abdbca"));
		assertEquals("dpd", solution.longestPalindrome("cddpd"));
		assertEquals("p", solution.longestPalindrome("pqr"));
	}

}