package io.github.iyunbo.dp.lcs;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(4, solution.longestCommonString("bbbbabcdcccc", "ccccabcddddd"));
	}

}