package io.github.iyunbo.dp.palindromic.min.deletion;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class MinInsertionTest {

	private final MinInsertion solution = new MinInsertion();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.minDeletions("abdbca")).isEqualTo(1);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.minDeletions("cddpd")).isEqualTo(2);
	}

	@Test
	public void test_simple_case3() {
		Assertions.assertThat(solution.minDeletions("pqr")).isEqualTo(2);
	}

}