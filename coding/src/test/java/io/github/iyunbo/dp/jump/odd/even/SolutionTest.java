package io.github.iyunbo.dp.jump.odd.even;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(3, solution.oddEvenJumps(new int[]{2, 3, 1, 1, 4}));
	}

	@Test
	public void test_base_case2() {
		assertEquals(3, solution.oddEvenJumps(new int[]{5, 1, 3, 4, 2}));
	}

	@Test
	public void test_base_case3() {
		assertEquals(2, solution.oddEvenJumps(new int[]{10, 13, 12, 14, 15}));
	}

	@Test
	public void test_string_builder() {
		StringBuilder builder = new StringBuilder();
		builder.insert(0, "h");
		builder.insert(0, "i");
		assertEquals("ih", builder.toString());
	}

}