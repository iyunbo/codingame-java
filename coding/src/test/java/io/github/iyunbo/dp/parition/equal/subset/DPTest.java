package io.github.iyunbo.dp.parition.equal.subset;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_base_case() {
		assertFalse(solution.canPartition(new int[]{1, 2, 3, 4, 5}));
		assertTrue(solution.canPartition(new int[]{3, 3, 3, 4, 5}));
	}

	@Test
	public void test_base_case2(){
		assertTrue(solution.canPartition(new int[]{1, 1, 3, 4, 7}));
	}

	@Test
	public void test_base_case3(){
		assertTrue(solution.canPartition(new int[]{1, 2, 3, 4}));
	}

	@Test
	public void test_base_case4(){
		assertFalse(solution.canPartition(new int[]{2, 3, 4, 6}));
	}

}