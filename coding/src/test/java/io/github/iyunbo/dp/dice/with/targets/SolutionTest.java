package io.github.iyunbo.dp.dice.with.targets;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case(){
		assertEquals(1,solution.numRollsToTarget(1, 6, 3));
		assertEquals(0,solution.numRollsToTarget(1, 2, 3));
	}

	@Test
	public void test_simple_case(){
		assertEquals(6,solution.numRollsToTarget(2, 6, 7));
	}

	@Test
	public void test_medium_case(){
		assertEquals(222616187,solution.numRollsToTarget(30, 30, 500));
	}

}