package io.github.iyunbo.dp.partition.min.diff.subset;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_base_case() {
		assertEquals(3, solution.minDiffSubset(new int[]{1, 2, 3, 9}));
	}

	@Test
	public void test_base_case2() {
		assertEquals(0, solution.minDiffSubset(new int[]{1, 2, 7, 1, 5}));
	}

	@Test
	public void test_base_case3() {
		assertEquals(92, solution.minDiffSubset(new int[]{1, 3, 100, 4}));
	}

}