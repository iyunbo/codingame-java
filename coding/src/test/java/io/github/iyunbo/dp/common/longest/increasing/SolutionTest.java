package io.github.iyunbo.dp.common.longest.increasing;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.longestIncreasingSubsequence(new int[]{4, 2, 3, 6, 10, 1, 12})).isEqualTo(5);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.longestIncreasingSubsequence(new int[]{-4, 10, 3, 7, 15})).isEqualTo(4);
	}

	@Test
	public void test_simple_case3() {
		Assertions.assertThat(solution.longestIncreasingSubsequence(new int[]{4, 10, 4, 3, 8, 9})).isEqualTo(3);
	}

}