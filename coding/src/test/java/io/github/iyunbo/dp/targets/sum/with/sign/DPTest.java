package io.github.iyunbo.dp.targets.sum.with.sign;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_base_case() {
		assertEquals(3, solution.findTargetSumWays(new int[]{1, 1, 2, 3}, 1));
	}

	@Test
	public void test_special_case() {
		assertEquals(2, solution.findTargetSumWays(new int[]{1, 2, 7, 1}, 9));
	}

}