package io.github.iyunbo.dp.knapsack;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Random;

import org.junit.jupiter.api.Test;

class BetterDPTest {

	private final BetterDP solution = new BetterDP();

	@Test
	public void test_base_case0() {
		assertEquals(220, solution.knapsack(50, new int[]{10, 20, 30}, new int[]{60, 100, 120}, 3));
	}

	@Test
	public void test_base_case1() {
		assertEquals(50, solution.knapsack(2, new int[]{1, 1, 1}, new int[]{10, 20, 30}, 3));
	}

	@Test
	public void test_slow_case() {
		Random random = new Random();
		int n = 50;
		int[] w = new int[n];
		int[] v = new int[n];
		for (int i = 0; i < n; i++) {
			w[i] = random.nextInt(2000);
			v[i] = random.nextInt(2000);
		}
		assertTrue(solution.knapsack(9000, w, v, n) > 0);
	}

}