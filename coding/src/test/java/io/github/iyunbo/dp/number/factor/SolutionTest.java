package io.github.iyunbo.dp.number.factor;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case(){
		assertEquals(4, solution.countWays(4));
	}

	@Test
	public void test_base_case2(){
		assertEquals(6, solution.countWays(5));
	}

	@Test
	public void test_base_case3(){
		assertEquals(9, solution.countWays(6));
	}

}