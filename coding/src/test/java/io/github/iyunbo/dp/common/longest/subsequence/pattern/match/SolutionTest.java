package io.github.iyunbo.dp.common.longest.subsequence.pattern.match;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.subsequencePatternMatch("baxmx", "ax")).isEqualTo(2);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.subsequencePatternMatch("tomorrow", "tor")).isEqualTo(4);
	}

}