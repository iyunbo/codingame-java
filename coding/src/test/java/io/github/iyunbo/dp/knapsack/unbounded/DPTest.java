package io.github.iyunbo.dp.knapsack.unbounded;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_base_case() {
		assertEquals(80, solution.knapsack(5, new int[]{1, 2, 3}, new int[]{15, 20, 50}));
	}

	@Test
	public void test_base_case2() {
		assertEquals(140, solution.knapsack(8, new int[]{1, 3, 4, 5}, new int[]{15, 50, 60, 90}));
	}

}