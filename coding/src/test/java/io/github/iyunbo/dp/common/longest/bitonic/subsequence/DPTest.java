package io.github.iyunbo.dp.common.longest.bitonic.subsequence;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.longestBitonicSubsequence(new int[]{4, 2, 3, 6, 10, 1, 12})).isEqualTo(5);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.longestBitonicSubsequence(new int[]{4, 2, 5, 9, 7, 6, 10, 3, 1})).isEqualTo(7);
	}

}