package io.github.iyunbo.dp.twokeys.keyboard;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	void minSteps() {
		assertEquals(3, solution.minSteps(3));
	}
}