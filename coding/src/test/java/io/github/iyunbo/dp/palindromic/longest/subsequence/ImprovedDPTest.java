package io.github.iyunbo.dp.palindromic.longest.subsequence;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ImprovedDPTest {

	private final ImprovedDP solution = new ImprovedDP();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.findLPSLength("abdbca")).isEqualTo(5);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.findLPSLength("cddpd")).isEqualTo(3);
	}

	@Test
	public void test_base_case() {
		Assertions.assertThat(solution.findLPSLength("pqr")).isEqualTo(1);
	}
}