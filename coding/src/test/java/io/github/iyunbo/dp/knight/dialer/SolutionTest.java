package io.github.iyunbo.dp.knight.dialer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_cases() {
		assertEquals(10, solution.knightDialer(1));
		assertEquals(20, solution.knightDialer(2));
		assertEquals(46, solution.knightDialer(3));
		assertEquals(104, solution.knightDialer(4));
	}

}