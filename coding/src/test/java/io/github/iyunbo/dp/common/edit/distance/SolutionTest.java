package io.github.iyunbo.dp.common.edit.distance;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.editDistance("bat", "but")).isEqualTo(1);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.editDistance("abdca", "cbda")).isEqualTo(2);
	}

	@Test
	public void test_simple_case3() {
		Assertions.assertThat(solution.editDistance("passpot", "ppsspqrt")).isEqualTo(3);
	}

	@Test
	public void test_simple_case4() {
		Assertions.assertThat(solution.editDistance("horse", "ros")).isEqualTo(3);
	}

}