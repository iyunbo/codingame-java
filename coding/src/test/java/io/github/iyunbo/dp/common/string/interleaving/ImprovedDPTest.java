package io.github.iyunbo.dp.common.string.interleaving;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ImprovedDPTest {

	private final ImprovedDP solution = new ImprovedDP();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.stringInterleaving("abd", "cef", "abcdef")).isTrue();
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.stringInterleaving("abd", "cef", "adcbef")).isFalse();
	}

	@Test
	public void test_simple_case3() {
		Assertions.assertThat(solution.stringInterleaving("abc", "def", "abdccf")).isFalse();
	}

	@Test
	public void test_simple_case4() {
		Assertions.assertThat(solution.stringInterleaving("abcdef", "mnop", "mnaobcdepf")).isTrue();
	}

	@Test
	public void test_simple_case5() {
		Assertions.assertThat(solution.stringInterleaving("", "", "a")).isFalse();
	}

	@Test
	public void test_simple_case6() {
		Assertions.assertThat(solution.stringInterleaving("", "", "")).isTrue();
	}

	@Test
	public void test_simple_case7() {
		Assertions.assertThat(solution.stringInterleaving("a", "", "a")).isTrue();
	}
}