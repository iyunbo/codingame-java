package io.github.iyunbo.dp.palindromic.longest.substring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_case_cases() {
		assertEquals("bab", solution.longestPalindrome("babad"));
		assertEquals("bb", solution.longestPalindrome("cbbd"));
		assertEquals("a", solution.longestPalindrome("a"));
		assertEquals("a", solution.longestPalindrome("ac"));
	}

}