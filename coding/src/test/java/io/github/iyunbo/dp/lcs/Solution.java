package io.github.iyunbo.dp.lcs;

public class Solution {

	public int longestCommonString(String str1, String str2) {
		final int n = str1.length();
		final int m = str2.length();
		// S = O(N*M)
		final int[][] lcs = new int[n][m];
		int result = 0;
		// T = O(N*M)
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (i == 0 || j == 0) {
					lcs[i][j] = 0;
				} else if (str1.charAt(i) == str2.charAt(j)) {
					lcs[i][j] = lcs[i - 1][j - 1] + 1;
					result = Integer.max(result, lcs[i][j]);
				} else {
					lcs[i][j] = 0;
				}
			}
		}
		return result;
	}
}
