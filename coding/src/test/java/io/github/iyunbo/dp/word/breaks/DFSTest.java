package io.github.iyunbo.dp.word.breaks;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

class DFSTest {

	private final DFS solution = new DFS();

	@Test
	public void test_base_cases() {
		assertTrue(solution.wordBreak("leetcode", List.of("leet", "code")));
		assertTrue(solution.wordBreak("applepenapple", List.of("apple", "pen")));
		assertFalse(solution.wordBreak("catsandog", List.of("cats", "dog", "sand", "and", "cat")));
		assertFalse(solution.wordBreak("dcatsandog", List.of("cats", "dog", "sand", "and", "cat")));
	}

}