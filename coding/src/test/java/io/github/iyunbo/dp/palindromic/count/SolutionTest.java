package io.github.iyunbo.dp.palindromic.count;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


class SolutionTest {
	private final Solution solution = new Solution();

	@Test
	public void test_case_cases() {
		assertEquals(7, solution.countPalindromicStrings("babad"));
		assertEquals(5, solution.countPalindromicStrings("cbbd"));
		assertEquals(1, solution.countPalindromicStrings("a"));
		assertEquals(2, solution.countPalindromicStrings("ac"));
	}

	@Test
	public void test_simple_cases() {
		assertEquals(7, solution.countPalindromicStrings("abdbca"));
		assertEquals(7, solution.countPalindromicStrings("cddpd"));
		assertEquals(3, solution.countPalindromicStrings("pqr"));
	}
}