package io.github.iyunbo.dp.jump.min.with.fee;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.minFee(new int[]{1, 2, 5, 2, 1, 2})).isEqualTo(3);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.minFee(new int[]{2, 3, 4, 5})).isEqualTo(5);
	}

}