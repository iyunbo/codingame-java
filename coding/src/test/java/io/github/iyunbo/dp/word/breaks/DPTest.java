package io.github.iyunbo.dp.word.breaks;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_base_cases() {
		assertTrue(solution.wordBreak("leetcode", List.of("leet", "code")));
		assertTrue(solution.wordBreak("applepenapple", List.of("apple", "pen")));
		assertFalse(solution.wordBreak("catsandog", List.of("cats", "dog", "sand", "and", "cat")));
		assertFalse(solution.wordBreak("dcatsandog", List.of("cats", "dog", "sand", "and", "cat")));
	}

}