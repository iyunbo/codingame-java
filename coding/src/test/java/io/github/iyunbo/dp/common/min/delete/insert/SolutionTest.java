package io.github.iyunbo.dp.common.min.delete.insert;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import io.github.iyunbo.other.util.Pair;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.minDeletionInsertion("abc", "fbc")).isEqualTo(Pair.of(1, 1));
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.minDeletionInsertion("abdca", "cbda")).isEqualTo(Pair.of(2, 1));
	}

	@Test
	public void test_simple_case3() {
		Assertions.assertThat(solution.minDeletionInsertion("passport", "ppsspt")).isEqualTo(Pair.of(3, 1));
	}

}