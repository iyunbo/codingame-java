package io.github.iyunbo.dp.best.buyandsell;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_cases() {
		assertEquals(5, solution.maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
		assertEquals(0, solution.maxProfit(new int[]{7, 6, 4, 3, 1}));
	}

}