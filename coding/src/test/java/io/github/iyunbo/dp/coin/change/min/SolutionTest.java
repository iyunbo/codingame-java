package io.github.iyunbo.dp.coin.change.min;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {
	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(2, solution.minCoinChange(new int[]{1,2,3}, 5));
	}

	@Test
	public void test_special_case(){
		assertEquals(4, solution.minCoinChange(new int[]{1,2,3}, 11));
	}

	@Test
	public void test_base_case2() {
		assertEquals(3, solution.minCoinChange(new int[]{1, 2, 5}, 11));
	}

	@Test
	public void test_special_case2(){
		assertEquals(-1, solution.minCoinChange(new int[]{2}, 3));
	}
}