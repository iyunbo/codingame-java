package io.github.iyunbo.dp.common.edit.distance;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class DP2Test {

	private final DP2 solution = new DP2();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.editDistance("bat", "but")).isEqualTo(1);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.editDistance("abdca", "cbda")).isEqualTo(2);
	}

	@Test
	public void test_simple_case3() {
		Assertions.assertThat(solution.editDistance("passpot", "ppsspqrt")).isEqualTo(3);
	}

	@Test
	public void test_simple_case4() {
		Assertions.assertThat(solution.editDistance("horse", "ros")).isEqualTo(3);
	}

	@Test
	public void test_simple_case5() {
		Assertions.assertThat(solution.editDistance("ab", "bc")).isEqualTo(2);
	}

	@Test
	public void test_simple_case6() {
		Assertions.assertThat(solution.editDistance("", "bc")).isEqualTo(2);
	}

}