package io.github.iyunbo.dp.common.longest.subsequence.pattern.match;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ImprovedTest {

	private final Improved solution = new Improved();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.subsequencePatternMatch("baxmx", "ax")).isEqualTo(2);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.subsequencePatternMatch("tomorrow", "tor")).isEqualTo(4);
	}

}