package io.github.iyunbo.dp.jump.min;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DPTest {

	final DP solution = new DP();

	@Test
	public void test_base_case() {
		assertEquals(3, solution.countMinJumps(new int[]{2, 1, 1, 1, 4}));
	}

	@Test
	public void test_base_case2() {
		assertEquals(4, solution.countMinJumps(new int[]{1, 1, 3, 6, 9, 3, 0, 1, 3}));
	}

}