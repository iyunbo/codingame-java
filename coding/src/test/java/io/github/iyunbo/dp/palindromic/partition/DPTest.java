package io.github.iyunbo.dp.palindromic.partition;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.minPalindromePartition("abdbca")).isEqualTo(3);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.minPalindromePartition("cddpd")).isEqualTo(2);
	}

	@Test
	public void test_simple_case3() {
		Assertions.assertThat(solution.minPalindromePartition("pqr")).isEqualTo(2);
	}

	@Test
	public void test_simple_case4() {
		Assertions.assertThat(solution.minPalindromePartition("pp")).isEqualTo(0);
	}

	@Test
	public void test_simple_case5() {
		Assertions.assertThat(solution.minPalindromePartition("madam")).isEqualTo(0);
	}
}