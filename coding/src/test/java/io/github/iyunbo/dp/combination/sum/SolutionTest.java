package io.github.iyunbo.dp.combination.sum;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(7, solution.combinationSum4(new int[]{1, 2, 3}, 4));
	}

}