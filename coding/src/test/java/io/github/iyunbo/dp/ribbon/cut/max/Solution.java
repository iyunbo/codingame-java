package io.github.iyunbo.dp.ribbon.cut.max;

import java.util.Arrays;

public class Solution {
	private static final int INVALID_VAL = -1;

	public int maxRibbonPieces(int[] ribbonLengths, int total) {
		int n = ribbonLengths.length;

		int[][] dp = new int[n][total + 1];

		for (int[] arr : dp) {
			Arrays.fill(arr, INVALID_VAL);
		}

		for (int i = 0; i < n; i++) {
			dp[i][0] = 0;
			int len = ribbonLengths[i];
			for (int l = 1; l <= total; l++) {
				int withoutPiece = INVALID_VAL, withPiece = INVALID_VAL;
				if (i > 0) {
					withoutPiece = dp[i - 1][l];
				}
				if (len <= l && dp[i][l - len] != INVALID_VAL) {
					withPiece = dp[i][l - len] + 1;
				}
				dp[i][l] = Integer.max(withoutPiece, withPiece);
			}
		}

		return dp[n - 1][total];
	}
}
