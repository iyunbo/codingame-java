package io.github.iyunbo.dp.common.longest.repeating.subsequence;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ImprovedTest {

	private final Improved solution = new Improved();

	@Test
	public void test_base_case() {
		Assertions.assertThat(solution.longestRepeatingSubsequence("")).isEqualTo(0);
	}

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.longestRepeatingSubsequence("tomorrow")).isEqualTo(2);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.longestRepeatingSubsequence("aabdbcec")).isEqualTo(3);
	}

	@Test
	public void test_simple_case3() {
		Assertions.assertThat(solution.longestRepeatingSubsequence("fmff")).isEqualTo(2);
	}

	@Test
	public void test_simple_case4() {
		Assertions.assertThat(solution.longestRepeatingSubsequence("fmffabcdegz")).isEqualTo(2);
	}


}