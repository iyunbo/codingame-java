package io.github.iyunbo.dp.common.shortest.supersequence;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ImprovedTest {

	private final Improved solution = new Improved();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.shortestCommonSupersequence("abcf", "bdcf")).isEqualTo(5);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.shortestCommonSupersequence("dynamic", "dynprogrammicng")).isEqualTo(15);
	}
}