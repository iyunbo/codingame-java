package io.github.iyunbo.dp.dice.simulation;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case(){
		assertEquals(34, solution.dieSimulator(2, new int[]{1,1,2,2,2,3}));
	}

	@Test
	public void test_base_case2(){
		assertEquals(30, solution.dieSimulator(2, new int[]{1,1,1,1,1,1}));
	}

	@Test
	public void test_base_case3(){
		assertEquals(181, solution.dieSimulator(3, new int[]{1,1,1,2,2,3}));
	}
}