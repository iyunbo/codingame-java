package io.github.iyunbo.dp.targets.sum;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_base_case() {
		assertEquals(10, solution.findTargetSumWays(new int[]{1, 1, 1, 1, 1}, 3));
	}

	@Test
	public void test_special_case() {
		assertEquals(256, solution.findTargetSumWays(new int[]{1, 0, 0, 0, 0, 0, 0, 0, 0}, 1));
	}

	@Test
	public void test_base_case2() {
		assertEquals(1, solution.findTargetSumWays(new int[]{1, 2, 3, 7}, 6));
	}

	@Test
	public void test_base_case3() {
		assertEquals(2, solution.findTargetSumWays(new int[]{1, 2, 7, 1, 5}, 10));
	}

	@Test
	public void test_base_case4() {
		assertEquals(0, solution.findTargetSumWays(new int[]{1, 3, 4, 8}, 6));
	}

}