package io.github.iyunbo.dp.common.shortest.supersequence;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.shortestCommonSupersequence("abcf", "bdcf")).isEqualTo(5);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.shortestCommonSupersequence("dynamic", "dynprogrammicng")).isEqualTo(15);
	}
}