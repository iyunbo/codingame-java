package io.github.iyunbo.dp.common.longest.substring;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class DPTest {

	private final DP solution = new DP();

	@Test
	public void test_simple_case(){
		Assertions.assertThat(solution.longestCommonSubstring("abdca", "cbda")).isEqualTo(2);
	}

	@Test
	public void test_simple_case2(){
		Assertions.assertThat(solution.longestCommonSubstring("passport", "ppsspt")).isEqualTo(3);
	}

}