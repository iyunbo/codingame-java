package io.github.iyunbo.dp.ribbon.cut.max;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(2, solution.maxRibbonPieces(new int[]{2, 3, 5}, 5));
	}

	@Test
	public void test_base_case2() {
		assertEquals(3, solution.maxRibbonPieces(new int[]{2, 3}, 7));
	}

	@Test
	public void test_base_case3() {
		assertEquals(3, solution.maxRibbonPieces(new int[]{3, 5, 7}, 13));
	}

}