package io.github.iyunbo.dp.path;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(28, solution.uniquePaths(7, 3));
		assertEquals(28, solution.uniquePaths(3, 7));
	}

}