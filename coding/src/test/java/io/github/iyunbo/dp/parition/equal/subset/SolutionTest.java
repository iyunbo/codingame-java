package io.github.iyunbo.dp.parition.equal.subset;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertFalse(solution.canPartition(new int[]{1, 2, 3, 4, 5}));
		assertTrue(solution.canPartition(new int[]{3, 3, 3, 4, 5}));
	}

}