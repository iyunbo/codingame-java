package io.github.iyunbo.dp.jump.game;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertFalse(solution.canJump(new int[]{3, 2, 1, 0, 4}));
	}

	@Test
	public void test_base_case2() {
		assertTrue(solution.canJump(new int[]{3, 2, 1, 1, 4}));
	}

	@Test
	public void test_base_case3() {
		assertTrue(solution.canJump(new int[]{5, 9, 3, 2, 1, 0, 2, 3, 3, 1, 0, 0}));
	}

	@Test
	public void test_base_case4() {
		assertTrue(solution.canJump(new int[]{1,2,3}));
	}


}