package io.github.iyunbo.dp.common.min.deletion.sorted;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.minDeletionsForSorted(new int[]{4, 2, 3, 6, 10, 1, 12})).isEqualTo(2);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.minDeletionsForSorted(new int[]{-4, 10, 3, 7, 15})).isEqualTo(1);
	}

	@Test
	public void test_simple_case3() {
		Assertions.assertThat(solution.minDeletionsForSorted(new int[]{3, 2, 1, 0})).isEqualTo(3);
	}

}