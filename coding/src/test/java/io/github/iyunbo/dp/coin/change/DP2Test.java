package io.github.iyunbo.dp.coin.change;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DP2Test {

	private final DP2 solution = new DP2();

	@Test
	public void test_base_case() {
		assertEquals(5, solution.coinChange(new int[]{1, 2, 3}, 5));
	}

}