package io.github.iyunbo.strings.multipy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(String.valueOf(123 * 456), solution.multiply("123", "456"));
	}

	@Test
	public void test_edge_case() {
		assertEquals(String.valueOf(0), solution.multiply("0", "52"));
	}

}