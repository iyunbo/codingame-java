package io.github.iyunbo.strings.word.ladder;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void should_return_0_if_empty() {
		assertEquals(0, solution.ladderLength("a", "b", List.of()));
	}

	@Test
	public void should_find_shortest_path() {
		assertEquals(5, solution.ladderLength("hit", "cog", List.of("hot", "dot", "dog", "lot", "log", "cog")));
	}

}