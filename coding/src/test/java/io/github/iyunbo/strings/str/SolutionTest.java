package io.github.iyunbo.strings.str;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		assertEquals(0, solution.strStr("xx", ""));
	}

	@Test
	public void test_default_case() {
		assertEquals(-1, solution.strStr("xx", "a"));
	}

	@Test
	public void test_base_case() {
		assertEquals(2, solution.strStr("hello", "ll"));
	}

	@Test
	public void test_identical_strings() {
		assertEquals(0, solution.strStr("hello", "hello"));
	}

	@Test
	public void test_multiple_occurrences() {
		assertEquals(1, solution.strStr("xhelloyhellozhello", "hello"));
	}

}