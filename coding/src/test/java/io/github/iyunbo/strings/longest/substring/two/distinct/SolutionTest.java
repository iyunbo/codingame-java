package io.github.iyunbo.strings.longest.substring.two.distinct;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(3, solution.lengthOfLongestSubstringTwoDistinct("eceba"));
		assertEquals(5, solution.lengthOfLongestSubstringTwoDistinct("ccaabbb"));
	}

}