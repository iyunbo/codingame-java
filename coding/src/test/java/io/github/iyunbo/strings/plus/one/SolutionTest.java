package io.github.iyunbo.strings.plus.one;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertArrayEquals(new int[]{2, 4, 9, 4, 0}, solution.plusOne(new int[]{2, 4, 9, 3, 9}));
	}

	@Test
	public void test_base_case1() {
		assertArrayEquals(new int[]{1, 0}, solution.plusOne(new int[]{9}));
		assertArrayEquals(new int[]{1, 0, 0}, solution.plusOne(new int[]{9, 9}));
	}

}