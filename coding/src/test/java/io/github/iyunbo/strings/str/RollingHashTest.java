package io.github.iyunbo.strings.str;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class RollingHashTest {
	final RollingHash solution = new RollingHash();

	@Test
	public void test_simple_case() {
		assertEquals(0, solution.strStr("xx", ""));
		assertEquals(0, solution.strStr("a", ""));
	}

	@Test
	public void test_default_case() {
		assertEquals(-1, solution.strStr("xx", "a"));
	}

	@Test
	public void test_base_case() {
		assertEquals(2, solution.strStr("hello", "ll"));
	}

	@Test
	public void test_identical_strings() {
		assertEquals(0, solution.strStr("hello", "hello"));
	}

	@Test
	public void test_multiple_occurrences() {
		assertEquals(1, solution.strStr("xhelloyhellozhello", "hello"));
	}

	@Test
	public void test_hash() {
		assertEquals(0 * 26 * 26 + 1 * 26 + 2, solution.hash("abc", 3));
		assertEquals(0 * 26 * 26 * 26 + 1 * 26 * 26 + 2 * 26 + 3, solution.hash("abcd", 4));
	}

	@Test
	public void test_bigger_needle() {
		assertEquals(-1, solution.strStr("xx", "aaa"));
		assertEquals(-1, solution.strStr("", "aaa"));
	}

	@Test
	public void test_last_position() {
		assertEquals(2, solution.strStr("abc", "c"));
	}

	@Test
	public void test_special_case() {
		assertEquals(6, solution.strStr("ababcaababcaabc", "ababcaabc"));
	}

	@Test
	public void test_long_string() {
		assertEquals(107, solution.strStr("baabbaaaaaaabbaaaaabbabbababaabbabbbbbabbabbbbbbabababaabbbbbaaabbbbabaababababbbaabbbbaaabbaababbbaabaabbabbaaaabababaaabbabbababbabbaaabbbbabbbbabbabbaabbbaa", "bbaaaababa"));
	}

	@Test
	public void test_exp() {
		long x = 1;
		for (int i = 0; i < 31; i++) {
			x *= 2;
		}
		assertEquals(Integer.MAX_VALUE + 1L, x);
	}
}