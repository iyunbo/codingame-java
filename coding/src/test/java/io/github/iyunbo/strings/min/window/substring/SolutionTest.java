package io.github.iyunbo.strings.min.window.substring;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_edge_case() {
		assertEquals("", solution.minWindow("abc", "aa"));
	}

	@Test
	public void test_long_string_case() throws IOException, URISyntaxException {
		String expected = readToString("/minwindow/expected.txt");
		String input1 = readToString("/minwindow/input1.txt");
		String input2 = readToString("/minwindow/input2.txt");
		assertEquals(expected, solution.minWindow(input1, input2));
	}

	private String readToString(String path) throws IOException, URISyntaxException {
		return Files.readString(Path.of(getClass().getResource(path).toURI()));
	}

	@Test
	public void test_integer_comapraison(){
		assertFalse(new Integer(1) == new Integer(1));
		assertSame(Integer.valueOf(1), Integer.valueOf(1));
	}
}