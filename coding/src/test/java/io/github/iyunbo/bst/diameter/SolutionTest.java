package io.github.iyunbo.bst.diameter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.github.iyunbo.bst.validation.TreeNode;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertEquals(5, solution.diameterOfBinaryTree(TreeNode.of(0, 1, 2, 3, 4, 5, null, 7)));
	}

	@Test
	public void test_simple_case() {
		assertEquals(3, solution.diameterOfBinaryTree(TreeNode.of(1, 2, 3, 4, 5)));
	}

	@Test
	public void test_diameter_at_child() {
		assertEquals(4, solution.diameterOfBinaryTree(TreeNode.of(0, null, 2, null, null, 4, 5, null, null, null, null, 6, null, null, 7)));
	}
}