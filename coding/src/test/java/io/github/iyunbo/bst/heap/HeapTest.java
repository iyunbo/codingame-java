package io.github.iyunbo.bst.heap;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.github.iyunbo.bst.heap.Heap;

class HeapTest {

	private final Heap heap = new Heap();

	@Test
	public void test_heapify_empty() {
		int[] array = {};
		heap.heapifyMin(array);
		assertArrayEquals(new int[0], array);
	}

	@Test
	public void test_heapify_basic_array() {
		int[] array = {5, 4, 6, 7, 8, 2, 1, 3, 9};
		heap.heapifyMin(array);
		assertArrayEquals(new int[]{1, 3, 2, 4, 8, 5, 6, 7, 9}, array);
	}

	@Test
	public void test_extract_heap() {
		int[] array = {5, 4, 6, 7, 8, 2, 1, 3, 9};
		heap.heapifyMin(array);
		int min = heap.extractMin(array);
		assertEquals(1, min);
		assertArrayEquals(new int[]{2, 3, 5, 4, 8, 9, 6, 7, Integer.MAX_VALUE}, array);
		min = heap.extractMin(array);
		assertEquals(2, min);
		min = heap.extractMin(array);
		assertEquals(3, min);
		min = heap.extractMin(array);
		assertEquals(4, min);
	}

}