package io.github.iyunbo.bst.level.order;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import io.github.iyunbo.bst.validation.TreeNode;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void should_return_empty_if_null_root() {
		assertEquals(List.of(), solution.levelOrder(null));
	}

	@Test
	public void should_return_root_if_only_one() {
		TreeNode root = new TreeNode(1);
		assertEquals(List.of(List.of(root.val)), solution.levelOrder(root));
	}

	@Test
	public void test_base_case() {
		TreeNode root = new TreeNode(1, new TreeNode(2), new TreeNode(3));
		assertEquals(List.of(List.of(1), List.of(2, 3)), solution.levelOrder(root));
	}

	@Test
	public void test_deeper_tree() {
		TreeNode root = new TreeNode(1,
				new TreeNode(2,
						new TreeNode(4,
								new TreeNode(8), new TreeNode(9)),
						new TreeNode(5,
								new TreeNode(10), new TreeNode(11))),
				new TreeNode(3,
						new TreeNode(6,
								new TreeNode(12), new TreeNode(13)),
						new TreeNode(7,
								new TreeNode(14), new TreeNode(15))));
		assertEquals(List.of(List.of(1), List.of(2, 3), List.of(4, 5, 6, 7), List.of(8, 9, 10, 11, 12, 13, 14, 15)), solution.levelOrder(root));
	}

}