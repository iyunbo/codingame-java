package io.github.iyunbo.bst.hoffman;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


class DecodeTest {

	private final Decode solution = new Decode();
	private final Encode encode = new Encode();

	@Test
	public void test_base_case() {
		assertEquals("ABACA", solution.decode("1001011", encode.buildTree("ABACA")));
	}

	@Test
	public void test_encoding() {
		String input = "ABACA";
		Node tree = encode.buildTree(input);
		assertEquals("1001011", encode.encode(input, tree));
	}

}