package io.github.iyunbo.bst.symmetry;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import io.github.iyunbo.bst.validation.TreeNode;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void should_valid_only_one_node() {
		assertTrue(solution.isSymmetric(new TreeNode(1)));
	}

	@Test
	public void should_valid_null_node() {
		assertTrue(solution.isSymmetric(null));
	}

	@Test
	public void test_simple_case() {
		assertTrue(solution.isSymmetric(new TreeNode(1, new TreeNode(2), new TreeNode(2))));
	}

	@Test
	public void test_base_case() {
		assertTrue(solution.isSymmetric(new TreeNode(1,
				new TreeNode(2, new TreeNode(3), new TreeNode(4)), new TreeNode(2, new TreeNode(4), new TreeNode(3))))
		);
	}

}