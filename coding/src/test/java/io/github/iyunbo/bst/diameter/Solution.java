package io.github.iyunbo.bst.diameter;

import io.github.iyunbo.bst.validation.TreeNode;

public class Solution {
	private int longestDiameter = 0;

	public int diameterOfBinaryTree(TreeNode root) {

		traverse(root);

		return longestDiameter;
	}

	/*
		   1
		2     3
	 4    5
	 */
	// T = O(N), visit every node to get the longest
	// S = O(N), worst case: tree is list
	private int traverse(TreeNode node) {
		if (node == null) return 0;
		final int leftLength = traverse(node.left);
		final int rightLength = traverse(node.right);
		int diameter = leftLength + rightLength;
		if (diameter > this.longestDiameter) {
			this.longestDiameter = diameter;
		}
		return Math.max(leftLength, rightLength) + 1;
	}
}
