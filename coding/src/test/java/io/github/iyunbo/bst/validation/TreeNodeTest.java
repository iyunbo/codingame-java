package io.github.iyunbo.bst.validation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

class TreeNodeTest {

	@Test
	public void build_simple_tree() {
		TreeNode root = TreeNode.of(0, 1, 2, 3, 4, null, null);
		assertEquals(4, root.left.right.val);
		assertNull(root.right.right);
	}

	@Test
	public void build_longer_tree() {
		TreeNode root = TreeNode.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, null, null);
		assertEquals(4, root.left.right.val);
		assertEquals(6, root.right.right.val);
	}

}