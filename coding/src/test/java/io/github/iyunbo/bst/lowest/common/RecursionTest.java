package io.github.iyunbo.bst.lowest.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.github.iyunbo.bst.validation.TreeNode;

class RecursionTest {

	final private Recursion solution = new Recursion();

	@Test
	public void test_base_case() {
		TreeNode root = new TreeNode();
		TreeNode n1 = new TreeNode();
		TreeNode n2 = new TreeNode();
		root.left = n1;
		root.right = n2;
		assertEquals(root, solution.lowestCommonAncestor(root, n1, n2));
	}

	@Test
	public void test_simple_case() {
		TreeNode n1 = new TreeNode();
		TreeNode n2 = new TreeNode();
		TreeNode root = new TreeNode(1,
				new TreeNode(1, new TreeNode(1, n1, new TreeNode()), new TreeNode()),
				new TreeNode(1, new TreeNode(1, new TreeNode(), new TreeNode()), new TreeNode(1, n2, new TreeNode())));
		assertEquals(root, solution.lowestCommonAncestor(
				root
				, n1, n2));
	}

	@Test
	public void test_simple_case2() {
		TreeNode root = TreeNode.of(3, 5, 1, 6, 2, 0, 8, null, null, 7, 4);
		assertEquals(root.left, solution.lowestCommonAncestor(root, root.left, root.left.right.right));
	}

	@Test
	public void test_special_case() {
		TreeNode root = TreeNode.of(37, -34, -48, null, -100, -101, 48, null, null, null, null, -54, null, -71, -22, null, null, null, 8);
		assertEquals(root.right.right, solution.lowestCommonAncestor(root, root.right.right.left, root.right.right));
	}

}