package io.github.iyunbo.bst.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		final TreeNode t = new TreeNode(1);
		assertTrue(solution.isValidBST(t));
	}

	@Test
	public void test_null_case() {
		assertTrue(solution.isValidBST(null));
	}

	@Test
	public void test_basic_tree() {
		final TreeNode t = new TreeNode(2,
				new TreeNode(1), new TreeNode(3)
		);
		assertTrue(solution.isValidBST(t));
	}

	@Test
	public void test_easy_case() {
		final TreeNode t = new TreeNode(5,
				new TreeNode(2, new TreeNode(1), new TreeNode(4)), new TreeNode(8, new TreeNode(6), null)
		);
		assertTrue(solution.isValidBST(t));
	}

	@Test
	public void should_consider_invalid_for_identical_value() {
		TreeNode t = new TreeNode(1,
				new TreeNode(1), null
		);
		assertFalse(solution.isValidBST(t));
		t = new TreeNode(1,
				null, new TreeNode(1)
		);
		assertFalse(solution.isValidBST(t));
	}

}