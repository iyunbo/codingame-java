package io.github.iyunbo.sliding.window.min.sum.subarray;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	@Test
	public void test_base_case() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.minSubarray(7, new int[]{2, 1, 5, 2, 3, 2})).isEqualTo(2);
	}

	@Test
	public void test_base_case2() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.minSubarray(7, new int[]{2, 1, 5, 2, 8})).isEqualTo(1);
	}

	@Test
	public void test_base_case3() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.minSubarray(8, new int[]{3, 4, 1, 1, 6})).isEqualTo(3);
	}

}