package io.github.iyunbo.sliding.window.fruits.into.baskets;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	@Test
	public void test_simple_case() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.maxFruits(new char[]{'A', 'B', 'C', 'A', 'C'})).isEqualTo(3);
	}

	@Test
	public void test_simple_case2() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.maxFruits(new char[]{'A', 'B', 'C', 'B', 'B', 'C'})).isEqualTo(5);
	}

	@Test
	public void test_simple_case3() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.maxFruits(new char[]{'A', 'B', 'C'})).isEqualTo(2);
	}
}