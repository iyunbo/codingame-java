package io.github.iyunbo.sliding.window.longest.k.distinct;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	@Test
	public void test_base_case() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.longestDistinct("araaci", 2)).isEqualTo(4);
	}

	@Test
	public void test_base_case2() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.longestDistinct("araaci", 1)).isEqualTo(2);
	}

	@Test
	public void test_base_case3() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.longestDistinct("cbbebi", 3)).isEqualTo(5);
	}

	@Test
	public void test_simple_case() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.longestDistinct("a", 1)).isEqualTo(1);
	}

	@Test
	public void test_simple_case2() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.longestDistinct("a", 2)).isEqualTo(1);
	}


}