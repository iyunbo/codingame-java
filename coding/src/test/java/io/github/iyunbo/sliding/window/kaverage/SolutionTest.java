package io.github.iyunbo.sliding.window.kaverage;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.average(5, new int[]{1, 3, 2, 6, -1, 4, 1, 8, 2}))
				.isEqualTo(new double[]{2.2, 2.8, 2.4, 3.6, 2.8});
	}

}