package io.github.iyunbo.sliding.window.longest.substring.with.same.letter.after.replacement;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	@Test
	public void test_simple_case() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.findLength("aabccbb", 2)).isEqualTo(5);
	}

	@Test
	public void test_simple_case2() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.findLength("abbcb", 1)).isEqualTo(4);
	}

	@Test
	public void test_simple_case3() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.findLength("abccde", 1)).isEqualTo(3);
	}

	@Test
	public void test_simple_case4() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.findLength("ABBB", 2)).isEqualTo(4);
	}

	@Test
	public void test_simple_case5() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.findLength("ABBBA", 2)).isEqualTo(5);
	}

	@Test
	public void test_base_case() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.findLength("aaaaaa", 3)).isEqualTo(6);
	}

	@Test
	public void test_base_case2() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.findLength("abcdefghij", 3)).isEqualTo(4);
	}
}