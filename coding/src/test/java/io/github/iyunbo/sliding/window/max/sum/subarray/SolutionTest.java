package io.github.iyunbo.sliding.window.max.sum.subarray;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		Assertions.assertThat(solution.maxSumSubArray(3, new int[]{2, 1, 5, 1, 3, 2})).isEqualTo(9);
	}

	@Test
	public void test_simple_case2() {
		Assertions.assertThat(solution.maxSumSubArray(2, new int[]{2, 3, 4, 1, 5})).isEqualTo(7);
	}

}