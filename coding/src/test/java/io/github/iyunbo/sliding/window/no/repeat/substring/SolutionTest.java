package io.github.iyunbo.sliding.window.no.repeat.substring;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	@Test
	public void test_simple_case() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.longestNoRepeatSubstr("aabccbb")).isEqualTo(3);
	}

	@Test
	public void test_simple_case2() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.longestNoRepeatSubstr("abbbb")).isEqualTo(2);
		Assertions.assertThat(solution.longestNoRepeatSubstr("aaaaaabbbbccccc")).isEqualTo(2);
	}

	@Test
	public void test_simple_case3() {
		Solution solution = new Solution();
		Assertions.assertThat(solution.longestNoRepeatSubstr("abccde")).isEqualTo(3);
	}

}