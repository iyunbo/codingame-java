package io.github.iyunbo.other.prision.afterndays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_encoding() {
		assertEquals(0, solution.encode(new int[]{0, 0, 0, 0, 0, 0, 0, 0}));
		assertEquals(1, solution.encode(new int[]{0, 0, 0, 0, 0, 0, 0, 1}));
		assertEquals(3, solution.encode(new int[]{0, 0, 0, 0, 0, 0, 1, 1}));
		assertEquals(131, solution.encode(new int[]{1, 0, 0, 0, 0, 0, 1, 1}));
	}

	@Test
	public void test_base_case() {
		assertArrayEquals(new int[]{0, 0, 1, 1, 1, 1, 1, 0}, solution.prisonAfterNDays(new int[]{1, 0, 0, 1, 0, 0, 1, 0}, 1000000000));
	}

	@Test
	public void test_simple_case() {
		assertArrayEquals(new int[]{0, 1, 1, 0, 0, 0, 0, 0}, solution.prisonAfterNDays(new int[]{0, 1, 0, 1, 1, 0, 0, 1}, 1));
		assertArrayEquals(new int[]{0, 0, 0, 0, 1, 1, 1, 0}, solution.prisonAfterNDays(new int[]{0, 1, 0, 1, 1, 0, 0, 1}, 2));
		assertArrayEquals(new int[]{0, 1, 1, 0, 0, 1, 0, 0}, solution.prisonAfterNDays(new int[]{0, 1, 0, 1, 1, 0, 0, 1}, 3));
		assertArrayEquals(new int[]{0, 0, 0, 0, 0, 1, 0, 0}, solution.prisonAfterNDays(new int[]{0, 1, 0, 1, 1, 0, 0, 1}, 4));
		assertArrayEquals(new int[]{0, 1, 1, 1, 0, 1, 0, 0}, solution.prisonAfterNDays(new int[]{0, 1, 0, 1, 1, 0, 0, 1}, 5));
		assertArrayEquals(new int[]{0, 0, 1, 0, 1, 1, 0, 0}, solution.prisonAfterNDays(new int[]{0, 1, 0, 1, 1, 0, 0, 1}, 6));
		assertArrayEquals(new int[]{0, 0, 1, 1, 0, 0, 0, 0}, solution.prisonAfterNDays(new int[]{0, 1, 0, 1, 1, 0, 0, 1}, 7));
	}


}