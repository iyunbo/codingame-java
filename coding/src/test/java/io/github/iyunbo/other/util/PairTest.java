package io.github.iyunbo.other.util;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class PairTest {

	@Test
	public void test_equals() {
		Assertions.assertThat(Pair.of(List.of(1, 2), Set.of(3, 4)).equals(Pair.of(List.of(1, 2), Set.of(4, 3)))).isTrue();
		Assertions.assertThat(Pair.of(List.of(1, 2), Set.of(3, 4)).equals(Pair.of(List.of(2, 1), Set.of(4, 3)))).isFalse();
	}

	@Test
	public void test_hashcode() {
		Assertions.assertThat(Pair.of(List.of(1, 2), Set.of(3, 4)).hashCode()).isEqualTo(Pair.of(Arrays.asList(1, 2), Set.of(4, 3)).hashCode());
		Assertions.assertThat(Pair.of(Pair.of(1, 2), Pair.of("x", "y")).hashCode()).isEqualTo(Pair.of(Pair.of(1, 2), Pair.of("x", "y")).hashCode());
	}

}