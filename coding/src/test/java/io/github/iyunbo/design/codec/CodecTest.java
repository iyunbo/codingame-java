package io.github.iyunbo.design.codec;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.github.iyunbo.bst.validation.TreeNode;

class CodecTest {

	private final Codec codec = new Codec();

	@Test
	public void test_ser_deser() {
		TreeNode root = new TreeNode(1, new TreeNode(2), new TreeNode(3));
		assertEquals(root, codec.deserialize(codec.serialize(root)));
	}

	@Test
	public void test_ser() {
		TreeNode root = new TreeNode(1, new TreeNode(2), new TreeNode(3));
		assertEquals("1,2,#,#,3,#,#,", codec.serialize(root));
	}

}