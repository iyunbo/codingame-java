package io.github.iyunbo.design.median;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MedianForSmallNumTest {

	@Test
	public void test_base_case() {
		MedianForSmallNum finder = new MedianForSmallNum();
		finder.addNum(1);
		finder.addNum(2);
		Assertions.assertEquals(1.5, finder.findMedian());
		finder.addNum(3);
		Assertions.assertEquals(2, finder.findMedian());
	}


}