package io.github.iyunbo.design.tictacktoe;

public class TicTacToe {

	private final int[][] filledColumns;
	private final int[][] filledRows;
	private final int[] filledDiagonal;
	private final int[] filledAntiDiagonal;

	/**
	 * Initialize your data structure here.
	 */
	public TicTacToe(int n) {
		filledColumns = new int[2][n];
		filledRows = new int[2][n];
		filledDiagonal = new int[2];
		filledAntiDiagonal = new int[2];
	}

	/**
	 * Player {player} makes a move at ({row}, {col}).
	 *
	 * @param row    The row of the board.
	 * @param col    The column of the board.
	 * @param player The player, can be either 1 or 2.
	 * @return The current winning condition, can be either:
	 * 0: No one wins.
	 * 1: Player 1 wins.
	 * 2: Player 2 wins.
	 */
	public int move(int row, int col, int player) {
		final int i = player - 1;
		filledColumns[i][col] += 1;
		filledRows[i][row] += 1;
		if (row == col) {
			filledDiagonal[i] += 1;
		}
		int n = filledColumns[0].length;
		if (row + col == n - 1) {
			filledAntiDiagonal[i] += 1;
		}
		if (filledColumns[i][col] == n || filledRows[i][row] == n || filledDiagonal[i] == n || filledAntiDiagonal[i] == n) {
			return player;
		}
		return 0;
	}
}