package io.github.iyunbo.design.tictacktoe;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TicTacToeTest {

	@Test
	public void let_us_play(){
		final TicTacToe game = new TicTacToe(3);
		assertEquals(0, game.move(0,0,1));
		assertEquals(0, game.move(0,2,2));
		assertEquals(0, game.move(2,2,1));
		assertEquals(0, game.move(1,1,2));
		assertEquals(0, game.move(2,0,1));
		assertEquals(0, game.move(1,0,2));
		assertEquals(1, game.move(2,1,1));
	}

	@Test
	public void let_us_play_2x2(){
		final TicTacToe game = new TicTacToe(2);
		/*
		. X
		. .
		 */
		assertEquals(0, game.move(0,1,1));
		/*
		. X
		. O
		 */
		assertEquals(0, game.move(1,1,2));
		/*
		. X
		X O
		 */
		assertEquals(1, game.move(1,0,1));
	}

}