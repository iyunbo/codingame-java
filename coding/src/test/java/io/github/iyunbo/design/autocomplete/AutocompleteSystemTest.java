package io.github.iyunbo.design.autocomplete;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class AutocompleteSystemTest {

	@Test
	public void test_base_case() {
		final AutocompleteSystem system = new AutocompleteSystem(
				new String[]{"i love you", "island", "ironman", "i love leetcode"},
				new int[]{5, 3, 2, 2}
		);
		assertEquals(List.of("i love you", "island", "i love leetcode"), system.input('i'));
		assertEquals(List.of("i love you", "i love leetcode"), system.input(' '));
		assertEquals(List.of(), system.input('a'));
		assertEquals(List.of(), system.input('#'));
	}

	@Test
	public void test_base_case2() {
		final AutocompleteSystem system = new AutocompleteSystem(
				new String[]{"i love you", "island", "ironman", "i love leetcode"},
				new int[]{5, 3, 2, 2}
		);
		assertEquals(List.of("i love you", "island", "i love leetcode"), system.input('i'));
		assertEquals(List.of("i love you", "i love leetcode"), system.input(' '));
		assertEquals(List.of(), system.input('a'));
		assertEquals(List.of(), system.input('#'));
		assertEquals(List.of("i love you", "island", "i love leetcode"), system.input('i'));
		assertEquals(List.of("i love you", "i love leetcode", "i a"), system.input(' '));
		assertEquals(List.of("i a"), system.input('a'));
		assertEquals(List.of(), system.input('#'));
		assertEquals(List.of("i love you", "island", "i a"), system.input('i'));
		assertEquals(List.of("i love you", "i a", "i love leetcode"), system.input(' '));
		assertEquals(List.of("i a"), system.input('a'));
		assertEquals(List.of(), system.input('#'));
	}

}