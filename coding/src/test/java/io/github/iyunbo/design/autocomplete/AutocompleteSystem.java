package io.github.iyunbo.design.autocomplete;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

public class AutocompleteSystem {

	//S = O(100 * S * 2) + O(S)
	private Map<String, Set<String>> dict;
	private Map<String, Integer> counter;
	private String currentInput;

	public AutocompleteSystem(String[] sentences, int[] times) {
		initialize(sentences, times);
	}

	private void initialize(String[] sentences, int[] times) {
		this.dict = new HashMap<>();
		this.counter = new HashMap<>();
		this.currentInput = "";

		for (int i = 0; i < sentences.length; i++) {
			recordSentence(sentences[i], times[i]);
		}

	}

	// T = O(1)
	private void recordSentence(String sentence, int frequency) {
		for (int i = 1; i <= sentence.length(); i++) {
			String prefix = sentence.substring(0, i);
			Set<String> q = dict.getOrDefault(prefix, new HashSet<>());
			q.add(sentence);
			dict.put(prefix, q);
		}
		counter.put(sentence, frequency);
	}

	private int compareSentences(String s1, String s2) {
		if (!counter.getOrDefault(s1, 0).equals(counter.getOrDefault(s2, 0))) {
			return Integer.compare(counter.getOrDefault(s2, 0), counter.getOrDefault(s1, 0));
		}
		return s1.compareTo(s2);
	}

	// T = O(S)
	public List<String> input(char c) {
		if (c == '#') {
			int f = counter.getOrDefault(this.currentInput, 0);
			recordSentence(this.currentInput, f + 1);
			this.currentInput = "";
		} else {
			this.currentInput += c;
			if (dict.containsKey(this.currentInput)) {
				return getTop3(this.currentInput);
			}
		}
		return List.of();
	}

	// T = O(S*log3) = O(S)
	private List<String> getTop3(String sentence) {
		final Queue<String> queue = new PriorityQueue<>((s1, s2) -> -compareSentences(s1, s2));
		for (String s : dict.get(sentence)) {
			queue.add(s);
			if (queue.size() > 3) {
				queue.poll();
			}
		}
		List<String> ret = Arrays.asList(queue.toArray(new String[0]));
		ret.sort(this::compareSentences);
		return ret;
	}
}
