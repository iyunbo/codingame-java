package io.github.iyunbo.design.lru;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class LRUCacheTest {

	@Test
	public void test_basic_ops() {
		final LRUCache cache = new LRUCache(100);
		cache.put(1, 2);
		assertEquals(2, cache.get(1));
	}

	@Test
	public void test_lru_case() {
		final LRUCache cache = new LRUCache(2);
		cache.put(1, 1);
		cache.put(2, 2);
		assertEquals(1, cache.get(1));
		cache.put(3, 3);
		assertEquals(-1, cache.get(2));
		cache.put(4, 4);
		assertEquals(-1, cache.get(1));
		assertEquals(3, cache.get(3));
		assertEquals(4, cache.get(4));
	}

	@Test
	public void test_refresh_key() {
		final LRUCache cache = new LRUCache(2);
		cache.put(2, 1);
		cache.put(1, 1);
		cache.put(2, 3);
		cache.put(4, 1);
		assertEquals(-1, cache.get(1));
		assertEquals(3, cache.get(2));
	}

	@Test
	public void test_special_case() {
		final LRUCache cache = new LRUCache(3);
		cache.put(1, 1);
		cache.put(2, 2);
		cache.put(3, 3);
		cache.put(4, 4);
		assertEquals(4, cache.get(4));
		assertEquals(3, cache.get(3));
		assertEquals(2, cache.get(2));
		assertEquals(-1, cache.get(1));
		cache.put(5, 5);
		assertEquals(-1, cache.get(1));
		assertEquals(2, cache.get(2));
		assertEquals(3, cache.get(3));
		assertEquals(-1, cache.get(4));
		assertEquals(5, cache.get(5));
	}

}