package io.github.iyunbo.design.minstack;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class MinStackTest {

	@Test
	public void test_basic_ops() {
		MinStack stack = new MinStack();
		stack.push(-1);
		stack.push(0);
		stack.push(-3);
		assertEquals(-3, stack.getMin());
		stack.pop();
		assertEquals(0, stack.top());
		assertEquals(-1, stack.getMin());
	}

}