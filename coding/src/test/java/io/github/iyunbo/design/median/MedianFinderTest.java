package io.github.iyunbo.design.median;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MedianFinderTest {

	@Test
	public void test_base_case() {
		MedianFinder finder = new MedianFinder();
		finder.addNum(1);
		finder.addNum(2);
		Assertions.assertEquals(1.5, finder.findMedian());
		finder.addNum(3);
		Assertions.assertEquals(2, finder.findMedian());
	}


}