package io.github.iyunbo.graph.matrix.ilands.number;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class BFSTest {

	private final BFS solution = new BFS();

	@Test
	public void should_detect_one_iland() {
		assertEquals(1, solution.numIslands(new char[][]{{'1'}}));
	}

	@Test
	public void should_detect_zero_iland_if_empty() {
		assertEquals(0, solution.numIslands(new char[0][]));
	}

	@Test
	public void should_detect_one_iland_of_2x2_grid() {
		assertEquals(1, solution.numIslands(new char[][]{
				{'1', '0'},
				{'0', '0'}
		}));
	}

	@Test
	public void should_detect_one_iland_of_2x1_grid() {
		assertEquals(1, solution.numIslands(new char[][]{
				{'1'}, {'1'}
		}));
	}

	@Test
	public void should_detect_one_iland_of_3x3_grid() {
		assertEquals(1, solution.numIslands(new char[][]{
				{'0', '0', '0'},
				{'0', '1', '0'},
				{'0', '0', '0'},
		}));
	}

	@Test
	public void should_detect_one_iland_of_filled_3x3_grid() {
		assertEquals(1, solution.numIslands(new char[][]{
				{'1', '1', '0'},
				{'1', '1', '0'},
				{'0', '0', '0'},
		}));
	}

	@Test
	public void should_detect_one_iland_of_full_3x3_grid() {
		assertEquals(1, solution.numIslands(new char[][]{
				{'1', '1', '1'},
				{'1', '1', '1'},
				{'1', '1', '1'},
		}));
	}

	@Test
	public void should_detect_3_ilands_of_3x3_grid() {
		assertEquals(3, solution.numIslands(new char[][]{
				{'1', '0', '0'},
				{'0', '1', '0'},
				{'0', '0', '1'},
		}));
	}

	@Test
	public void test_special_case() {
		assertEquals(1, solution.numIslands(new char[][]{
				{'1', '0', '1', '1', '1'},
				{'1', '0', '1', '0', '1'},
				{'1', '1', '1', '0', '1'},
		}));
	}

	@Test
	public void test_special_case2() {
		assertEquals(3, solution.numIslands(new char[][]{
				{'1', '1', '0', '0', '0'},
				{'1', '1', '0', '0', '0'},
				{'0', '0', '1', '0', '0'},
				{'0', '0', '0', '1', '1'},
		}));
	}

}