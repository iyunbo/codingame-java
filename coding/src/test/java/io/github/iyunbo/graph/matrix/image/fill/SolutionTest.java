package io.github.iyunbo.graph.matrix.image.fill;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_base_case() {
		assertArrayEquals(new int[][]{
				{2, 2, 2},
				{2, 2, 0},
				{2, 0, 1}
		}, solution.floodFill(new int[][]{
				{1, 1, 1},
				{1, 1, 0},
				{1, 0, 1}
		}, 1, 1, 2));
	}

	@Test
	public void test_array_hash_and_equals() {
		assertTrue(List.of(new Solution.Coordinate(0, 1), new Solution.Coordinate(1, 2)).contains(new Solution.Coordinate(0, 1)));
		Assertions.assertEquals(new Solution.Coordinate(1, 2).hashCode(), new Solution.Coordinate(1, 2).hashCode());
	}

}