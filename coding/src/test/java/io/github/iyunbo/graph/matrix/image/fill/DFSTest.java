package io.github.iyunbo.graph.matrix.image.fill;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class DFSTest {

	private final DFS solution = new DFS();

	@Test
	public void test_base_case() {
		assertArrayEquals(new int[][]{
				{2, 2, 2},
				{2, 2, 0},
				{2, 0, 1}
		}, solution.floodFill(new int[][]{
				{1, 1, 1},
				{1, 1, 0},
				{1, 0, 1}
		}, 1, 1, 2));
	}
}