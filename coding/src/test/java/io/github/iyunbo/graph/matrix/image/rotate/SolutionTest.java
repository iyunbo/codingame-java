package io.github.iyunbo.graph.matrix.image.rotate;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_rotate90() {
		int[] s = new int[]{0, 0};
		solution.rotate90(s, 3, 0);
		assertArrayEquals(new int[]{0, 2}, s);
		s = new int[]{1, 0};
		solution.rotate90(s, 5, 0);
		assertArrayEquals(new int[]{0, 3}, s);
	}

	@Test
	public void test_base_case() {
		int[][] m = new int[][]{
				{1, 2, 3},
				{4, 5, 6},
				{7, 8, 9}
		};
		solution.rotate(m);
		assertArrayEquals(new int[][]{
				{7, 4, 1},
				{8, 5, 2},
				{9, 6, 3},
		}, m);
	}

	@Test
	public void test_rotate4numbers() {
		int[][] m = new int[][]{
				{1, 2, 3},
				{4, 5, 6},
				{7, 8, 9}
		};
		solution.rotate4Numbers(m, 0, 0, 3);
		assertArrayEquals(new int[][]{
				{7, 2, 1},
				{4, 5, 6},
				{9, 8, 3}
		}, m);
		m = new int[][]{
				{1, 2, 3, 4},
				{5, 6, 7, 8},
				{9, 10, 11, 12},
				{13, 14, 15, 16}
		};
		solution.rotate4Numbers(m, 0, 1, 4);
		assertArrayEquals(new int[][]{
				{1, 9, 3, 4},
				{5, 6, 7, 2},
				{15, 10, 11, 12},
				{13, 14, 8, 16}
		}, m);
	}

	@Test
	public void test_4x4_matrics() {
		int[][] m = new int[][]{
				{5, 1, 9, 11},
				{2, 4, 8, 10},
				{13, 3, 6, 7},
				{15, 14, 12, 16}
		};
		solution.rotate(m);
		assertArrayEquals(new int[][]{
						{15, 13, 2, 5},
						{14, 3, 4, 1},
						{12, 6, 8, 9},
						{16, 7, 10, 11}},
				m);
	}


}