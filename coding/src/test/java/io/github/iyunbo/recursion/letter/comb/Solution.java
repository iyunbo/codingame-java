package io.github.iyunbo.recursion.letter.comb;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Solution {

	private final Map<Character, List<String>> digitToChars = Map.of(
			'2', List.of("a", "b", "c"),
			'3', List.of("d", "e", "f"),
			'4', List.of("g", "h", "i"),
			'5', List.of("j", "k", "l"),
			'6', List.of("m", "n", "o"),
			'7', List.of("p", "q", "r", "s"),
			'8', List.of("t", "u", "v"),
			'9', List.of("w", "x", "y", "z")
	);

	public List<String> letterCombinations(String digits) {
		final List<String> combinations = new ArrayList<>();
		if (digits.length() == 0) return List.of();
		if (digits.length() == 1) return digitToChars.get(digits.charAt(0));
		// T = O(D ^ 4)
		// S = O(D ^ 4)
		final List<String> suffixCombinations = letterCombinations(digits.substring(1));
		for (String prefix : digitToChars.get(digits.charAt(0))) {
			for (String suffix : suffixCombinations) {
				combinations.add(prefix + suffix);
			}
		}
		return combinations;
	}

}
