package io.github.iyunbo.recursion.word.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrieBacktrack {
	private static class TrieNode {
		private final Map<Character, TrieNode> children = new HashMap<>();
		private String word = "";
	}

	private char[][] board;
	private List<String> result;

	public List<String> findWords(char[][] board, String[] words) {
		this.board = board;
		this.result = new ArrayList<>();

		// S = O(Sum(Wi)) = O(S)
		final TrieNode trie = buildTree(words);

		// T = O(N * 4 * 3 ^ (H -1)) = O(N * 3 ^ (H-1)),
		// worst case: visit every cell, 3-braches-tree traversal of trie match, with height H = maximum length of words
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				if (trie.children.containsKey(board[row][col])) {
					backtrack(row, col, trie);
				}
			}
		}
		return this.result;
	}

	private void backtrack(int row, int col, TrieNode trie) {

		final char letter = this.board[row][col];
		final TrieNode subTree = trie.children.get(letter);

		if (!subTree.word.isEmpty()) {
			this.result.add(subTree.word);
			subTree.word = "";
		}

		this.board[row][col] = '#';

		final int[][] directions = new int[][]{{-1, 0}, {0, -1}, {1, 0}, {0, 1}};
		for (int[] direction : directions) {
			final int nextRow = row + direction[0];
			final int nextCol = col + direction[1];
			if (isValid(nextRow, nextCol, subTree)) {
				backtrack(nextRow, nextCol, subTree);
			}
		}

		this.board[row][col] = letter;

		// pruning: very efficient!
		if (subTree.children.isEmpty()) {
			trie.children.remove(letter);
		}


	}

	private boolean isValid(int nextRow, int nextCol, TrieNode trie) {
		final int rowLimit = this.board.length;
		final int colimit = this.board[0].length;
		return nextRow >= 0 && nextRow < rowLimit
				&& nextCol >= 0 && nextCol < colimit
				&& trie.children.containsKey(this.board[nextRow][nextCol]);
	}

	private TrieNode buildTree(String[] words) {
		final TrieNode root = new TrieNode();
		for (String word : words) {
			insert(root, "", word);
		}
		return root;
	}

	private void insert(TrieNode parent, String prefix, String suffix) {
		if (suffix.isEmpty()) {
			parent.word = prefix;
			return;
		}
		final char firstLetter = suffix.charAt(0);
		parent.children.putIfAbsent(firstLetter, new TrieNode());
		TrieNode subTree = parent.children.get(firstLetter);
		insert(subTree, prefix + firstLetter, suffix.substring(1));
	}
}
