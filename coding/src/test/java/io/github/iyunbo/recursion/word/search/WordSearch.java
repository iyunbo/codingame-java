package io.github.iyunbo.recursion.word.search;

import java.util.HashSet;
import java.util.Set;

public class WordSearch {
	public boolean exist(char[][] board, String word) {
		final char firstLetter = word.charAt(0);
		int x, y;
		final Set<Integer> used = new HashSet<>();
		// T = O(N*3^D), worst case: every cell has the same letter
		// S = O(D), the longest depth of the stack
		for (x = 0; x < board.length; x++) {
			for (y = 0; y < board[x].length; y++) {
				if (firstLetter == board[x][y]) {
					used.add(x * 200 + y);
					if (search(board, x, y, word.substring(1), used)) {
						return true;
					} else {
						used.clear();
					}
				}
			}
		}
		return false;
	}

	private boolean search(char[][] board, int x, int y, String word, Set<Integer> used) {
		// T = O(3 ^ D) = O(D), worst case: search tree of 3 directions
		// S = O(D)
		if (word.isEmpty()) return true;
		final char nextLetter = word.charAt(0);
		for (int[] delta : new int[][]{{-1, 0}, {0, -1}, {1, 0}, {0, 1}}) {
			int newX = x + delta[0];
			int newY = y + delta[1];
			if (newX >= 0 && newX < board.length
					&& newY >= 0 && newY < board[0].length
					&& nextLetter == board[newX][newY]
					&& !used.contains(newX * 200 + newY)
			) {
				used.add(newX * 200 + newY);
				if (search(board, newX, newY, word.substring(1), used)) {
					return true;
				} else {
					used.remove(newX * 200 + newY);
				}
			}
		}
		return false;
	}
}
