package io.github.iyunbo.recursion.parentheses;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final Solution solution = new Solution();

	@Test
	public void test_simple_case() {
		assertEquals(List.of("()"), solution.generateParenthesis(1));
	}

	@Test
	public void test_n_2() {
		assertEquals(List.of("(())", "()()"), solution.generateParenthesis(2));
	}

	@Test
	public void test_n_3() {
		assertEquals(List.of("((()))", "(()())", "(())()", "()(())", "()()()"), solution.generateParenthesis(3));
	}

}