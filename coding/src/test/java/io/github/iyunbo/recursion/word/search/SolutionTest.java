package io.github.iyunbo.recursion.word.search;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

class SolutionTest {

	private final WordSearch solution = new WordSearch();

	private final FindWords findWords = new FindWords();

	private final TrieBacktrack trieBacktrack = new TrieBacktrack();

	@Test
	public void test_base_case() {
		assertTrue(solution.exist(new char[][]{
				{'A', 'B', 'C', 'E'},
				{'S', 'F', 'C', 'S'},
				{'A', 'D', 'E', 'E'}
		}, "SEE"));
	}

	@Test
	public void test_find_words() {
		assertEquals(List.of("oath", "eat"), findWords.findWords(new char[][]{
				{'o', 'a', 'a', 'n'},
				{'e', 't', 'a', 'e'},
				{'i', 'h', 'k', 'r'},
				{'i', 'f', 'l', 'v'}
		}, new String[]{"oath", "pea", "eat", "rain"}));
	}

	@Test
	public void test_find_words_with_trie() {
		assertEquals(List.of("oath", "eat"), trieBacktrack.findWords(new char[][]{
				{'o', 'a', 'a', 'n'},
				{'e', 't', 'a', 'e'},
				{'i', 'h', 'k', 'r'},
				{'i', 'f', 'l', 'v'}
		}, new String[]{"oath", "pea", "eat", "rain"}));
	}

	@Test
	public void test_special_case() {
		assertEquals(List.of("a"), trieBacktrack.findWords(new char[][]{
				{'a', 'a'},
		}, new String[]{"a"}));
	}

	@Test
	public void test_spacial_case_2() {
		assertEquals(List.of("aaa", "aaab", "aba", "baa", "aaba"), trieBacktrack.findWords(new char[][]{
				{'a', 'b'},
				{'a', 'a'}
		}, new String[]{"aba", "baa", "bab", "aaab", "aaa", "aaaa", "aaba"}));
	}

}