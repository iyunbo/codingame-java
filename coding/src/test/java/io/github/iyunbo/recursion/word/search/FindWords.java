package io.github.iyunbo.recursion.word.search;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FindWords {
	private final WordSearch solution = new WordSearch();

	public List<String> findWords(char[][] board, String[] words) {
		return Stream.of(words)
				.filter(w -> solution.exist(board, w))
				.collect(Collectors.toList());
	}
}
