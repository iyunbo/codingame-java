package io.github.iyunbo.dp.dice.with.targets;

public class Solution {
	public int numRollsToTarget(int d, int f, int target) {
		int[] ways = new int[target + 1];
		ways[0] = 1;
		for (int n = 0; n < d; n++) {
			int[] newWays = new int[target + 1];
			for (int i = 1; i <= target; i++) {
				for (int j = 1; j <= f; j++) {
					if (j <= i) {
						newWays[i] += ways[i - j];
						newWays[i] %= 1000000007;
					}
				}
			}
			ways = newWays;
		}
		return ways[target];
	}
}
