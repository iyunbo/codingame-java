package io.github.iyunbo.dp.partition.min.diff.subset;

public class DP {
	public int minDiffSubset(int[] nums) {
		int totalSum = 0;
		for (int num : nums) {
			totalSum += num;
		}

		boolean[][] dp = new boolean[nums.length][totalSum / 2 + 1];

		for (int i = 0; i < nums.length; i++) {
			int num = nums[i];
			for (int s = 0; s <= totalSum / 2; s++) {
				if (i == 0) {
					dp[i][s] = s == 0 || s == num;
				} else {
					if (num > s) {
						dp[i][s] = dp[i - 1][s];
					} else {
						dp[i][s] = dp[i - 1][s] || dp[i - 1][s - num];
					}
				}

			}
		}

		int closestSum = 0;
		for (int i = dp[nums.length - 1].length - 1; i >= 0; i--) {
			if (dp[nums.length - 1][i]) {
				closestSum = i;
				break;
			}
		}

		return totalSum - 2 * closestSum;


	}


}
