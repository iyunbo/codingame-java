package io.github.iyunbo.dp.rod.cutting;

public class DP {

	public int cutRod(int[] lengths, int[] prices, int n) {
		if(n == 0 || lengths.length == 0 || lengths.length != prices.length){
			return 0;
		}
		int[][] dp = new int[lengths.length][n+1];
		for(int i = 0; i < lengths.length; i ++){
			int len = lengths[i];
			int price = prices[i];
			for(int c = 0; c <= n; c++){
				int priceWithPiece, priceWithoutPiece;
				if(len <= c){
					priceWithPiece = price + dp[i][c - len];
				}else{
					priceWithPiece = 0;
				}
				if(i > 0){
					priceWithoutPiece = dp[i - 1][c];
				}else{
					priceWithoutPiece = 0;
				}
				dp[i][c] = Integer.max(priceWithPiece, priceWithoutPiece);
			}
		}

		return dp[lengths.length - 1][n];
	}
}
