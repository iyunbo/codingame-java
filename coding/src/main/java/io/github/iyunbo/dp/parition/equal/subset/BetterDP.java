package io.github.iyunbo.dp.parition.equal.subset;

public class BetterDP {
	public boolean canPartition(int[] nums) {
		int totalSum = 0;
		// find sum of all array elements
		for (int num : nums) {
			totalSum += num;
		}
		// if totalSum is odd,it cannot be partitioned into equal sum subset
		if (totalSum % 2 != 0) return false;
		int subSetSum = totalSum / 2;

		// dp[n][s] = true if some subset from 0 to n item can form the sum of 's'
		boolean[] dp = new boolean[subSetSum + 1];
		dp[0] = true;

		// with only first element
		for (int s = 1; s <= subSetSum; s++) {
			dp[s] = nums[0] == s;
		}

		for (int num : nums) {
			boolean[] last = dp;
			dp = new boolean[subSetSum + 1];
			dp[0] = true;
			// for any element x, it should be either in the subset for forming the s or not
			for (int s = 1; s <= subSetSum; s++) {
				if (s < num) {
					dp[s] = last[s];
				} else {
					dp[s] = last[s] || last[s - num];
				}
			}
		}

		return dp[subSetSum - 1];

	}


}