package io.github.iyunbo.dp.palindromic.partition;

/**
 * Problem Statement #
 * Given a string, we want to cut it into pieces such that each piece is a palindrome.
 * Write a function to return the minimum number of cuts needed.
 */
public class DP {


	public int minPalindromePartition(String str) {
		if (str == null || str.length() == 0) return 0;

		int n = str.length();
		boolean[][] palindrome = new boolean[n][n];

		for (int left = n - 1; left >= 0; left--) {
			for (int right = left; right < n; right++) {
				if (left == right) {
					palindrome[left][right] = true;
				} else if (str.charAt(left) == str.charAt(right)) {
					palindrome[left][right] = right - left == 1 || palindrome[left + 1][right - 1];
				}
			}
		}


		int[] partition = new int[n];
		for (int start = n - 1; start >= 0; start--) {
			partition[start] = n;
			for (int end = n - 1; end >= start; end--) {
				if (palindrome[start][end]) {
					partition[start] = end == n - 1 ? 0 : Integer.min(partition[start], 1 + partition[end + 1]);
				}
			}
		}

		return partition[0];
	}


}
