package io.github.iyunbo.dp.max.subarray;

public class Greedy {
	public int maxSubArray(int[] nums) {
		if (nums == null || nums.length == 0) return 0;
		int currentMaxSum = nums[0];
		int maxSum = currentMaxSum;

		for (int i = 1; i < nums.length; i++) {
			currentMaxSum = Integer.max(currentMaxSum + nums[i], nums[i]);
			maxSum = Integer.max(currentMaxSum, maxSum);
		}

		return maxSum;
	}
}
