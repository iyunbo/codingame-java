package io.github.iyunbo.dp.knapsack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {

	private final Map<List<Integer>, Integer> memory = new HashMap<>();

	public int knapsack(int maxWeight, int[] weights, int[] values, int n) {
		if (maxWeight == 0 || n == 0) return 0;

		List<Integer> key = List.of(maxWeight, n);
		if(memory.containsKey(key)) return memory.get(key);

		if (weights[n - 1] > maxWeight) {
			int excludeLast = knapsack(maxWeight, weights, values, n - 1);
			memory.put(key, excludeLast);
			return excludeLast;
		} else {
			int maxValue = Integer.max(
					values[n - 1] + knapsack(maxWeight - weights[n - 1], weights, values, n - 1),
					knapsack(maxWeight, weights, values, n - 1)
			);
			memory.put(key, maxValue);
			return maxValue;
		}
	}
}
