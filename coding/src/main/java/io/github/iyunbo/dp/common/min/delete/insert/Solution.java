package io.github.iyunbo.dp.common.min.delete.insert;

import io.github.iyunbo.other.util.Pair;

/**
 * Problem Statement #
 * Given strings s1 and s2, we need to transform s1 into s2 by deleting and inserting characters.
 * Write a function to calculate the count of the minimum number of deletion and insertion operations.
 */
public class Solution {
	public Pair<Integer, Integer> minDeletionInsertion(String s1, String s2) {
		if (s1 == null || s2 == null) return Pair.of(0, 0);

		int l1 = s1.length(), l2 = s2.length();
		int[][] dp = new int[l1 + 1][l2 + 1];

		for (int i = 1; i <= l1; i++) {
			for (int j = 1; j <= l2; j++) {
				if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					dp[i][j] = 1 + dp[i - 1][j - 1];
				} else {
					dp[i][j] = Integer.max(dp[i][j - 1], dp[i - 1][j]);
				}
			}
		}

		int deletion = Integer.max(l1, l2) - dp[l1][l2];
		int insertion = Integer.min(l1, l2) - dp[l1][l2];

		return Pair.of(deletion, insertion);
	}
}
