package io.github.iyunbo.dp.palindromic.partition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Problem Statement #
 * Given a string, we want to cut it into pieces such that each piece is a palindrome.
 * Write a function to return the minimum number of cuts needed.
 */
public class TopDown {

	private final Map<List<Integer>, Integer> memo = new HashMap<>();

	public int minPalindromePartition(String str) {
		memo.clear();
		return findMPP(str, 0, str.length() - 1);
	}

	private int findMPP(String str, int left, int end) {
		List<Integer> key = List.of(left, end);
		if (memo.containsKey(key)) {
			return memo.get(key);
		}
		if (left >= end || isPalindrome(str, left, end)) {
			memo.put(key, 0);
			return 0;
		}

		int minPalindromePartiiton = end - left;

		for (int i = left; i <= end; i++) {
			if (isPalindrome(str, left, i)) {
				minPalindromePartiiton = Integer.min(minPalindromePartiiton, 1 + findMPP(str, i + 1, end));
			}
		}
		memo.put(key, minPalindromePartiiton);
		return minPalindromePartiiton;
	}

	private boolean isPalindrome(String str, int left, int right) {
		while (left < right) {
			if (str.charAt(left++) != str.charAt(right--)) {
				return false;
			}
		}
		return true;
	}
}
