package io.github.iyunbo.dp.palindromic.partition;

/**
 * Problem Statement #
 * Given a string, we want to cut it into pieces such that each piece is a palindrome.
 * Write a function to return the minimum number of cuts needed.
 */
public class Solution {
	public int minPalindromePartition(String str) {
		return findMPP(str, 0, str.length() - 1);
	}

	private int findMPP(String str, int left, int end) {
		if (left >= end || isPalindrome(str, left, end)) {
			return 0;
		}

		int minPalindromePartiiton = end - left;

		for (int i = left; i <= end; i++) {
			if (isPalindrome(str, left, i)) {
				minPalindromePartiiton = Integer.min(minPalindromePartiiton, 1 + findMPP(str, i + 1, end));
			}
		}
		return minPalindromePartiiton;
	}

	private boolean isPalindrome(String str, int left, int right) {
		while (left < right) {
			if (str.charAt(left++) != str.charAt(right--)) {
				return false;
			}
		}
		return true;
	}
}
