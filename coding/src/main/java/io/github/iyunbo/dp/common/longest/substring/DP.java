package io.github.iyunbo.dp.common.longest.substring;

/**
 * Problem Statement #
 * Given two strings ‘s1’ and ‘s2’, find the length of the longest substring which is common in both the strings.
 */
public class DP {

	/*
	s1=abdca
	s2=cbda

	i=4
	j=
	s1.charAt(i)=a
	s2.charAt(j)=
	dp=[
	[0,0,0,1],
	[0,1,0,0],
	[0,0,2,0],
	[1,0,0,0],
	[1,0,0,1]
	]
	 */
	public int longestCommonSubstring(String s1, String s2) {
		if (s1 == null || s2 == null) return 0;

		int l1 = s1.length(), l2 = s2.length();

		int[][] dp = new int[l1][l2];
		int longest = 0;

		for (int i = 0; i < l1; i++) {
			for (int j = 0; j < l2; j++) {
				if (s1.charAt(i) == s2.charAt(j)) {
					dp[i][j] = (i == 0 || j == 0) ? 1 : 1 + dp[i - 1][j - 1];
					longest = Integer.max(dp[i][j], longest);
				} else {
					dp[i][j] = 0;
				}
			}
		}

		return longest;
	}


}
