package io.github.iyunbo.dp.palindromic.longest.substring;

/**
 * Problem Statement
 * Given a string, find the length of its Longest Palindromic Substring (LPS).
 * In a palindromic string, elements read the same backward and forward.
 */
public class ImprovedDP {


	/*
	clarify:
	"" -> true
	"a" -> true
	"aa" -> true
	"ab" -> false
	"aba" -> true

	T = O(N*N)
	S = O(N)
	 */
	public String longestPalindrome(String s) {
		if (s == null || s.length() <= 1) return s;
		int n = s.length();
		String[] dp = new String[n];

		for (int left = n - 1; left >= 0; left--) {
			String[] last = dp;
			dp = new String[n];
			for (int right = 0; right < n; right++) {
				if (left > right) {
					dp[right] = "";
				} else if (left == right) {
					dp[right] = s.substring(left, left + 1);
				} else {
					// if two pointers equal and inside substring is exactly a palindrome
					if (s.charAt(left) == s.charAt(right) && last[right - 1].length() == right - left - 1) {
						dp[right] = s.charAt(left) + last[right - 1] + s.charAt(right);
					} else {
						String leftSub = dp[right - 1];
						String rightSub = last[right];
						if (leftSub.length() >= rightSub.length()) {
							dp[right] = leftSub;
						} else {
							dp[right] = rightSub;
						}
					}
				}
			}
		}
		return dp[n - 1];
	}


}
