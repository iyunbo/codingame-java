package io.github.iyunbo.dp.common.longest.repeating.subsequence;

/**
 * Problem Statement #
 * Given a sequence, find the length of its longest repeating subsequence (LRS).
 * A repeating subsequence will be the one that appears at least twice in the original sequence and is not overlapping
 * (i.e. none of the corresponding characters in the repeating subsequences have the same index).
 */
public class Improved {
	public int longestRepeatingSubsequence(String str) {
		if (str == null || str.length() == 0) return 0;

		int n = str.length();
		// compare the string with itself but exlude the same position (i != j)
		// dp[j] = longest repeating subsequence of first j chars
		int[] dp = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			int[] last = dp;
			dp = new int[n + 1];
			for (int j = 1; j <= n; j++) {
				if (str.charAt(i - 1) == str.charAt(j - 1) && i != j) {
					dp[j] = 1 + last[j - 1];
				} else {
					dp[j] = Integer.max(dp[j - 1], last[j]);
				}
			}
		}

		return dp[n];
	}
}
