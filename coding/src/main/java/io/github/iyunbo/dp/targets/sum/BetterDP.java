package io.github.iyunbo.dp.targets.sum;

public class BetterDP {


	public int findTargetSumWays(int[] nums, int S) {
		int[] dp = new int[S + 1];
		dp[0] = 1;
		for (int i = 1; i <= nums.length; i++) {
			int[] last = dp;
			dp = new int[S + 1];
			int num = nums[i - 1];
			for (int s = 0; s <= S; s++) {
				if (num > s) {
					dp[s] = last[s];
				} else {
					dp[s] = last[s - num] + last[s];
				}
			}
		}

		return dp[S];
	}

}