package io.github.iyunbo.dp.knapsack;

public class BetterDP {


	/*
	T = O(N*C), S = O(C)
	 */
	public int knapsack(int maxWeight, int[] weights, int[] values, int n) {
		if (n == 0) return 0;
		if (n == 1) return values[0];
		int[] dp = new int[maxWeight + 1];
		for (int i = 1; i < n; i++) {
			int[] last = dp;
			dp = new int[maxWeight + 1];
			int weight = weights[i];
			for (int c = 1; c <= maxWeight; c++) {
				if (weight > c) {
					dp[c] = last[c];
				} else {
					dp[c] = Integer.max(last[c], last[c - weight] + values[i]);
				}
			}
		}

		return dp[maxWeight];
	}
}
