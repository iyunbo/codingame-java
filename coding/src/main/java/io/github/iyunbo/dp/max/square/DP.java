package io.github.iyunbo.dp.max.square;

import java.util.Collections;
import java.util.List;

public class DP {
	public int maximalSquare(char[][] matrix) {
		// T = O(M*N), S = O(M*N)
		if (matrix.length == 0) return 0;
		int[][] dp = new int[matrix.length][matrix[0].length];
		int maxSquareLen = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == '1') {
					if (i == 0 || j == 0) {
						dp[i][j] = 1;
					} else {
						dp[i][j] = Collections.min(List.of(
								dp[i - 1][j],
								dp[i][j - 1],
								dp[i - 1][j - 1]
						)) + 1;
					}
					maxSquareLen = Integer.max(dp[i][j], maxSquareLen);
				}
			}
		}
		return maxSquareLen * maxSquareLen;
	}
}
