package io.github.iyunbo.dp.max.square;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {

	private final Map<List<Integer>, Integer> squareMemo = new HashMap<>();
	private final Map<List<Integer>, Boolean> zeroMemo = new HashMap<>();

	public int maximalSquare(char[][] matrix) {
		if (matrix.length == 0) return 0;

		squareMemo.clear();
		zeroMemo.clear();

		int row = matrix.length;
		int col = matrix[0].length;

		if (row == col) {
			return maxSquare(matrix, 0, 0, row);
		} else if (row < col) {
			int s = 0;
			for (int i = 0; i < col - row + 1; i++) {
				s = Integer.max(s, maxSquare(matrix, 0, i, row));
			}
			return s;
		} else {
			int s = 0;
			for (int i = 0; i < row - col + 1; i++) {
				s = Integer.max(s, maxSquare(matrix, i, 0, col));
			}
			return s;
		}
	}

	private int maxSquare(char[][] matrix, int top, int left, int n) {
		if (n == 0) return 0;

		final List<Integer> key = List.of(top, left, n);

		if (squareMemo.containsKey(key)) {
			return squareMemo.get(key);
		}

		int result;

		if (allOnes(matrix, top, left, n, n)) {
			result = n * n;
		} else {
			result = Collections.max(
					List.of(
							maxSquare(matrix, top, left + 1, n - 1),
							maxSquare(matrix, top, left, n - 1),
							maxSquare(matrix, top + 1, left, n - 1),
							maxSquare(matrix, top + 1, left + 1, n - 1)
					)
			);
		}
		squareMemo.put(key, result);
		return result;
	}

	private boolean allOnes(char[][] matrix, int top, int left, int n, int m) {
		if (n == 0 || m == 0) return true;

		List<Integer> key = List.of(top, left, n, m);
		if (zeroMemo.containsKey(key)) {
			return zeroMemo.get(key);
		}

		boolean result = true;

		if (matrix[top][left] == '0') {
			result = false;
			zeroMemo.put(key, result);
		}

		if (!allOnes(matrix, top + 1, left, n - 1, m)) {
			result = false;
			zeroMemo.put(key, result);
		}

		if (!allOnes(matrix, top, left + 1, n, m - 1)) {
			result = false;
			zeroMemo.put(key, result);
		}

		return result;
	}
}
