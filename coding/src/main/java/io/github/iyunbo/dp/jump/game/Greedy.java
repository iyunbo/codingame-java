package io.github.iyunbo.dp.jump.game;


public class Greedy {

	public boolean canJump(int[] nums) {
		int lastValid = nums.length - 1;

		for (int i = nums.length - 2; i >= 0; i--) {
			if (i + nums[i] >= lastValid) {
				lastValid = i;
			}
		}

		return lastValid == 0;
	}
}