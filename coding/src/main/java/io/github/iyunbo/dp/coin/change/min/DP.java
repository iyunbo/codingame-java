package io.github.iyunbo.dp.coin.change.min;

import java.util.Arrays;

public class DP {
	public int minCoinChange(int[] coins, int amount) {
		if (amount == 0 || coins.length == 0) {
			return 0;
		}

		int[][] dp = new int[coins.length][amount + 1];
		for(int[] arr : dp){
			Arrays.fill(arr, Integer.MAX_VALUE);
		}

		for (int c = 0; c < coins.length; c++) {
			int coin = coins[c];
			dp[c][0] = 0;
			for (int m = 1; m <= amount; m++) {
				int withPiece = Integer.MAX_VALUE, withoutPiece = Integer.MAX_VALUE;
				if (coin <= m && dp[c][m - coin] != Integer.MAX_VALUE) {
					//with piece
					withPiece = dp[c][m - coin] + 1;
				}
				if (c > 0) {
					// without piece
					withoutPiece = dp[c - 1][m];
				}
				dp[c][m] = Integer.min(withPiece, withoutPiece);
			}
		}
		int result = dp[coins.length - 1][amount];
		return result == Integer.MAX_VALUE ? -1 : result;
	}
}
