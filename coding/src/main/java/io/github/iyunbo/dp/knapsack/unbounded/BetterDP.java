package io.github.iyunbo.dp.knapsack.unbounded;

public class BetterDP {

	public int knapsack(int capacity, int[] weights, int[] values) {
		if (capacity == 0 || weights.length != values.length) {
			return 0;
		}
		int[] dp = new int[capacity + 1];
		for (int i = 0; i < weights.length; i++) {
			int weight = weights[i];
			int value = values[i];
			int[] last = dp;
			dp = new int[capacity + 1];
			for (int c = 1; c <= capacity; c++) {
				int value1 = 0, value2 = 0;
				if (weight <= c) {
					value1 = value + dp[c - weight];
				}
				if (i > 0) {
					value2 = last[c];
				}
				dp[c] = Integer.max(value1, value2);
			}

		}
		return dp[capacity];
	}


}
