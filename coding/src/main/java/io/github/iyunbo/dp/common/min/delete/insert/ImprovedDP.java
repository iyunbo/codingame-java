package io.github.iyunbo.dp.common.min.delete.insert;

import io.github.iyunbo.other.util.Pair;

/**
 * Problem Statement #
 * Given strings s1 and s2, we need to transform s1 into s2 by deleting and inserting characters.
 * Write a function to calculate the count of the minimum number of deletion and insertion operations.
 */
public class ImprovedDP {
	public Pair<Integer, Integer> minDeletionInsertion(String s1, String s2) {
		if (s1 == null || s2 == null) return Pair.of(0, 0);

		int l1 = s1.length(), l2 = s2.length();
		int[] dp = new int[l2 + 1];

		for (int i = 1; i <= l1; i++) {
			int[] last = dp;
			dp = new int[l2 + 1];
			for (int j = 1; j <= l2; j++) {
				if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					dp[j] = 1 + last[j - 1];
				} else {
					dp[j] = Integer.max(dp[j - 1], last[j]);
				}
			}
		}

		int deletion = Integer.max(l1, l2) - dp[l2];
		int insertion = Integer.min(l1, l2) - dp[l2];

		return Pair.of(deletion, insertion);
	}
}
