package io.github.iyunbo.dp.common.longest.subsequence;

/**
 * Problem Statement #
 * Given two strings ‘s1’ and ‘s2’, find the length of the longest subsequence which is common in both the strings.
 * <p>
 * A subsequence is a sequence that can be derived from another sequence by deleting some or no elements without changing the order of the remaining elements.
 */
public class ImprovedDP {

	public int longestCommonSubsequence(String s1, String s2) {
		if (s1 == null || s2 == null) return 0;

		int l1 = s1.length(), l2 = s2.length();
		int[] dp = new int[l2 + 1];

		for (int i = 1; i <= l1; i++) {
			int[] last = dp;
			dp = new int[l2 + 1];
			for (int j = 1; j <= l2; j++) {
				if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					dp[j] = 1 + last[j - 1];
				} else {
					dp[j] = Integer.max(last[j], dp[j - 1]);
				}
			}
		}

		return dp[l2];
	}
}
