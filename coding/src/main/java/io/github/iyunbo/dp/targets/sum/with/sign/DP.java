package io.github.iyunbo.dp.targets.sum.with.sign;

import java.util.stream.IntStream;

import io.github.iyunbo.dp.targets.sum.BetterDP;

public class DP {

	public int findTargetSumWays(int[] nums, int S) {

		int totalSum = IntStream.of(nums).sum();

		if (totalSum < S || (S + totalSum) % 2 == 1) {
			return 0;
		}

		BetterDP dp = new BetterDP();
		return dp.findTargetSumWays(nums, (S + totalSum) / 2);
	}

}