package io.github.iyunbo.dp.partition.min.diff.subset;

public class Solution {
	public int minDiffSubset(int[] nums) {
		return minPartitionDiff(nums, 0, 0, 0);
	}

	private int minPartitionDiff(int[] nums, int start, int sum1, int sum2) {
		if (start == nums.length) {
			return Math.abs(sum1 - sum2);
		} else {
			int num = nums[start];
			int diff1 = minPartitionDiff(nums, start + 1, sum1 + num, sum2);
			int diff2 = minPartitionDiff(nums, start + 1, sum1, sum2 + num);

			return Integer.min(diff1, diff2);
		}
	}
}
