package io.github.iyunbo.dp.palindromic.count;

import java.util.ArrayList;
import java.util.List;

/**
 * Problem Statement
 * Given a string, find the total number of palindromic substrings in it.
 * Please note we need to find the total number of substrings and not subsequences.
 */
public class Solution {

	private final List<String> palindromes = new ArrayList<>();

	/*
	T = O(N*N)
	S = O(N)
	 */
	public int countPalindromicStrings(String str) {
		if (str == null || str.length() == 0) return 0;
		int n = str.length();
		if (n == 1) return 1;
		palindromes.clear();
		boolean[] dp = new boolean[n];

		for (int left = n - 1; left >= 0; left--) {
			boolean[] last = dp;
			dp = new boolean[n];
			for (int right = left; right < n; right++) {
				if (left == right) {
					palindromes.add(str.substring(left, right + 1));
					dp[right] = true;
				} else {
					if (str.charAt(left) == str.charAt(right)) {
						if (right - left == 1 || last[right - 1]) {
							dp[right] = true;
							palindromes.add(str.substring(left, right + 1));
						}
					}
				}
			}
		}

		return palindromes.size();
	}
}
