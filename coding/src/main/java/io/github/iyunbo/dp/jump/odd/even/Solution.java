package io.github.iyunbo.dp.jump.odd.even;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {

	private final Map<List<Integer>, Boolean> memory = new HashMap<>();
	private final Map<List<Integer>, Integer> memoryJumps = new HashMap<>();

	public int oddEvenJumps(int[] A) {
		int count = 0;
		memory.clear();
		memoryJumps.clear();
		for(int i = 0; i < A.length; i++){
			if(isGood(i, A, 1)){
				count++;
			}
		}
		return count;
	}

	private boolean isGood(int i, int[] A, int jump){
		if(i == A.length - 1) return true;
		List<Integer> key = List.of(i, jump);
		if(memory.containsKey(key)){
			return memory.get(key);
		}
		int next = jumpNext(i, A, jump);
		boolean result;
		if(next > i){
			result = isGood(next, A, jump + 1);
		}else{
			result = false;
		}
		memory.put(key, result);
		return result;
	}

	private int jumpNext(int i, int[] A, int jump){
		List<Integer> key = List.of(i, jump);
		if(memoryJumps.containsKey(key)){
			return memoryJumps.get(key);
		}
		int next = -1;
		if(jump % 2 == 1){
			int smallest = 100001;
			for(int j = A.length - 1; j > i; j--){
				if(A[j] >= A[i] && A[j] <= smallest){
					next = j;
					smallest = A[j];
				}
			}
		}else{
			int beggest = -1;
			for(int j = A.length - 1; j > i; j--){
				if(A[j] <= A[i] && A[j] >= beggest){
					next = j;
					beggest = A[j];
				}
			}
		}
		memoryJumps.put(key, next);
		return next;
	}
}