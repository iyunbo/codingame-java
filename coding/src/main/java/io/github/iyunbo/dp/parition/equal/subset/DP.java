package io.github.iyunbo.dp.parition.equal.subset;

public class DP {
	public boolean canPartition(int[] nums) {
		int totalSum = 0;
		// find sum of all array elements
		for (int num : nums) {
			totalSum += num;
		}
		// if totalSum is odd,it cannot be partitioned into equal sum subset
		if (totalSum % 2 != 0) return false;
		int subSetSum = totalSum / 2;

		// dp[n][s] = true if some subset from 0 to n (exclusive) item can form the sum of 's'
		final boolean[][] dp = new boolean[nums.length][subSetSum + 1];
		for (int n = 0; n < nums.length; n++) {
			dp[n][0] = true;
		}

		for (int s = 1; s <= subSetSum; s++) {
			dp[0][s] = nums[0] == s;
		}

		for (int n = 1; n < nums.length; n++) {
			// for any element x, it should be either in the subset for forming the s or not
			int x = nums[n];
			for (int s = 1; s <= subSetSum; s++) {
				if (x > s) {
					dp[n][s] = dp[n - 1][s];
				} else {
					dp[n][s] = dp[n - 1][s] || dp[n - 1][s - x];
				}
			}
		}

		return dp[nums.length - 1][subSetSum];

	}


}