package io.github.iyunbo.dp.common.edit.distance;

import java.util.Collections;
import java.util.List;

public class Memo {
	public int editDistance(String s1, String s2) {
		return minRemainingOps(s1, s2, 0, 0, new Integer[s1.length()][s2.length()]);
	}

	private int minRemainingOps(String s1, String s2, int current1, int current2, Integer[][] memo) {
		if (current1 == s1.length())
			return s2.length() - current2;

		if (current2 == s2.length()) {
			return s1.length() - current1;
		}

		if (memo[current1][current2] == null) {

			if (s1.charAt(current1) == s2.charAt(current2)) {
				memo[current1][current2] = minRemainingOps(s1, s2, current1 + 1, current2 + 1, memo);
			} else {
				int deletionCase = 1 + minRemainingOps(s1, s2, current1 + 1, current2, memo);
				int insertionCase = 1 + minRemainingOps(s1, s2, current1, current2 + 1, memo);
				int replacementCase = 1 + minRemainingOps(s1, s2, current1 + 1, current2 + 1, memo);
				memo[current1][current2] = Collections.min(List.of(deletionCase, insertionCase, replacementCase));
			}
		}
		return memo[current1][current2];
	}
}
