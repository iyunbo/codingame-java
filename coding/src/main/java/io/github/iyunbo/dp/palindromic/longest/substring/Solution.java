package io.github.iyunbo.dp.palindromic.longest.substring;

/**
 * Problem Statement
 * Given a string, find the length of its Longest Palindromic Substring (LPS).
 * In a palindromic string, elements read the same backward and forward.
 */
public class Solution {

	private Boolean[][] state = null;

	public String longestPalindrome(String s) {
		state = new Boolean[s.length()][s.length()];
		String longest = "";
		for (int i = 0; i < s.length(); i++) {
			for (int j = i + 1; j <= s.length(); j++) {
				if (j - i > longest.length() && isPalindrome(s, i, j)) {
					longest = s.substring(i, j);
				}
			}
		}

		return longest;
	}

	private boolean isPalindrome(String s, int start, int end) {
		if (this.state[start][end - 1] == null) {
			if (start >= end) {
				this.state[start][end - 1] = true;
			} else if (start + 1 == end) {
				this.state[start][end - 1] = true;
			} else {
				this.state[start][end - 1] = s.charAt(start) == s.charAt(end - 1) && isPalindrome(s, start + 1, end - 1);
			}
		}
		return this.state[start][end - 1];
	}
}
