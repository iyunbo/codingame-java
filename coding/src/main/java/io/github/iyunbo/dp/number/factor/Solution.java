package io.github.iyunbo.dp.number.factor;

public class Solution {
	/*
	number of ways for sum from 1, 3, 4
	 */
	public int countWays(int n) {
		int[] dp = new int[n + 1];

		dp[0] = 1;
		dp[1] = 1;
		dp[2] = 1;
		dp[3] = 2;

		for (int i = 4; i <= n; i++) {
			dp[i] = dp[i - 1] + dp[i - 3] + dp[i - 4];
		}

		return dp[n];
	}
}
