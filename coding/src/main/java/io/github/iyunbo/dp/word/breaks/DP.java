package io.github.iyunbo.dp.word.breaks;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DP {
	public boolean wordBreak(String s, List<String> wordDict) {
		final Set<String> dict = new HashSet<>(wordDict);
		final boolean[] state = new boolean[s.length() + 1];
		// result[i] for s.substring(0, i)
		state[0] = true;
		for(int right = 1; right <= s.length(); right++){
			for(int left = 0; left < right; left ++){
				if (state[left] && dict.contains(s.substring(left, right))) {
					state[right] = true;
					break;
				}
			}
		}

		return state[s.length()];
	}
}
