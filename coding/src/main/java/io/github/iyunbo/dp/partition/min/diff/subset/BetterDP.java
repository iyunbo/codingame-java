package io.github.iyunbo.dp.partition.min.diff.subset;

public class BetterDP {
	public int minDiffSubset(int[] nums) {
		int totalSum = 0;
		for (int num : nums) {
			totalSum += num;
		}

		boolean[] dp = new boolean[totalSum / 2 + 1];
		for (int i = 0; i < nums.length; i++) {
			int num = nums[i];
			boolean[] last = dp;
			dp = new boolean[totalSum / 2 + 1];
			for (int s = 0; s <= totalSum / 2; s++) {
				if (i == 0) {
					dp[s] = s == 0 || s == num;
				} else {
					if (num > s) {
						dp[s] = last[s];
					} else {
						dp[s] = last[s] || last[s - num];
					}
				}

			}
		}

		int closestSum = 0;
		for (int i = dp.length - 1; i >= 0; i--) {
			if (dp[i]) {
				closestSum = i;
				break;
			}
		}

		return totalSum - 2 * closestSum;


	}


}
