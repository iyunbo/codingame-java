package io.github.iyunbo.dp.word.breaks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
dcatsandog -> false
*/
public class DFS {

	private final Map<Integer, Boolean> memo = new HashMap<>();

	public boolean wordBreak(String s, List<String> wordDict) {
		memo.clear();
		return breakWord(s, new HashSet<>(wordDict), 0);
	}

	private boolean breakWord(String s, Set<String> dict, int start) {
		if (start == s.length()) return true;

		if (memo.containsKey(start)) return memo.get(start);

		for (int end = start + 1; end <= s.length(); end++) {
			if (dict.contains(s.substring(start, end)) && breakWord(s, dict, end)) {
				memo.put(start, true);
				return true;
			}
		}

		memo.put(start, false);
		return false;
	}


}