package io.github.iyunbo.dp.knapsack.unbounded;

public class Solution {

	public int knapsack(int capacity, int[] weights, int[] values) {
		return knapsackFrom(capacity, weights, values, 0);
	}

	private int knapsackFrom(int capacity, int[] weights, int[] values, int current) {
		if (capacity == 0 || weights.length != values.length || current >= weights.length) {
			return 0;
		}

		int value1 = 0;
		int weight = weights[current];
		if (weight <= capacity) {
			value1 = values[current] + knapsackFrom(capacity - weight, weights, values, current);
		}
		int value2 = knapsackFrom(capacity, weights, values, current + 1);


		return Integer.max(value1, value2);
	}
}
