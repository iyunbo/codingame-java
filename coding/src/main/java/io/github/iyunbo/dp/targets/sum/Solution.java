package io.github.iyunbo.dp.targets.sum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {

	private final Map<List<Integer>, Integer> memory = new HashMap<>();

	public int findTargetSumWays(int[] nums, int S) {
		if (nums.length == 0) return 0;

		memory.clear();
		return calculate(nums, nums.length, S);
	}

	private int calculate(int[] nums, int n, int S) {
		if (n == 0) {
			return S == 0 ? 1 : 0;
		} else {
			List<Integer> key = List.of(n, S);
			if (memory.containsKey(key)) {
				return memory.get(key);
			}
			int result = calculate(nums, n - 1, S) + calculate(nums, n - 1, S - nums[n - 1]);
			memory.put(key, result);
			return result;
		}
	}
}