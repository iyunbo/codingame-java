package io.github.iyunbo.dp.common.string.interleaving;

/**
 * Problem Statement #
 * Give three strings ‘m’, ‘n’, and ‘p’, write a method to find out if ‘p’ has been formed by interleaving ‘m’ and ‘n’.
 * ‘p’ would be considered interleaving ‘m’ and ‘n’ if it contains all the letters from ‘m’ and ‘n’ and the order of letters is preserved too.
 */
public class DP {
	public boolean stringInterleaving(String s1, String s2, String s3) {
		if (s1 == null || s2 == null || s3 == null) return false;
		int len1 = s1.length(), len2 = s2.length(), len3 = s3.length();
		// dp[i][j][k] = is interleaving string with
		// - first i chars of s1
		// - first j chars of s2
		// - first k chars of s3
		boolean[][][] dp = new boolean[len1 + 1][len2 + 1][len3 + 1];
		dp[0][0][0] = true;

		for (int i = 1; i <= len1; i++) {
			for (int k = 1; k <= len3; k++) {
				dp[i][0][k] = s1.charAt(i - 1) == s3.charAt(k - 1) && dp[i - 1][0][k - 1];
			}
		}

		for (int j = 1; j <= len2; j++) {
			for (int k = 1; k <= len3; k++) {
				dp[0][j][k] = s2.charAt(j - 1) == s3.charAt(k - 1) && dp[0][j - 1][k - 1];
			}
		}

		for (int i = 1; i <= len1; i++) {
			for (int j = 1; j <= len2; j++) {
				for (int k = 1; k <= len3; k++) {
					dp[i][j][k] = (s1.charAt(i - 1) == s3.charAt(k - 1) && dp[i - 1][j][k - 1]) || (s2.charAt(j - 1) == s3.charAt(k - 1) && dp[i][j - 1][k - 1]);
				}
			}
		}

		return dp[len1][len2][len3];
	}
}
