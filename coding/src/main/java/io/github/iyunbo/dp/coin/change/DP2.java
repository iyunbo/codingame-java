package io.github.iyunbo.dp.coin.change;

public class DP2 {
	public int coinChange(int[] coins, int amount) {
		if (amount == 0 || coins.length == 0) {
			return 0;
		}

		int[][] dp = new int[coins.length][amount + 1];

		for (int c = 0; c < coins.length; c++) {
			int coin = coins[c];
			dp[c][0] = 1;
			for (int m = 1; m <= amount; m++) {
				if (coin <= m) {
					//with piece
					dp[c][m] = dp[c][m - coin];
				}
				if (c > 0) {
					// without piece
					dp[c][m] += dp[c - 1][m];
				}
			}
		}

		return dp[coins.length - 1][amount];
	}
}
