package io.github.iyunbo.dp.common.longest.increasing;

/**
 * Problem Statement #
 * Given a number sequence, find the length of its Longest Increasing Subsequence (LIS).
 * In an increasing subsequence, all the elements are in increasing order (from lowest to highest).
 */
public class Memorization {
	public int longestIncreasingSubsequence(int[] nums) {
		return lis(nums, -1, 0, new int[nums.length + 1][nums.length + 1]);
	}

	private int lis(int[] nums, int last, int current, int[][] memo) {
		if (current == nums.length) {
			return 0;
		}

		if (memo[last + 1][current + 1] > 0) {
			return memo[last + 1][current + 1];
		}

		int c1 = 0, c2;
		if (last == -1 || nums[current] > nums[last]) {
			c1 = 1 + lis(nums, current, current + 1, memo);
		}

		c2 = lis(nums, last, current + 1, memo);

		int max = Integer.max(c1, c2);
		memo[last + 1][current + 1] = max;

		return max;
	}
}
