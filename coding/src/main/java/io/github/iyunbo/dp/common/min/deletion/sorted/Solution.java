package io.github.iyunbo.dp.common.min.deletion.sorted;

import java.util.Arrays;

public class Solution {

	public int minDeletionsForSorted(int[] nums) {
		if (nums == null || nums.length == 0) return 0;

		int n = nums.length;
		// dp[i] = max increasing subsequence at index i, with some previous index j
		int[] dp = new int[n];
		// every single char is a increasing subsequence itself
		Arrays.fill(dp, 1);
		int maxLen = 1;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < i; j++) {
				if (nums[i] > nums[j] && dp[j] >= dp[i]) {
					dp[i] = 1 + dp[j];
					maxLen = Integer.max(dp[i], maxLen);
				}
			}
		}

		return n - maxLen;
	}
}
