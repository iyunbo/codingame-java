package io.github.iyunbo.dp.common.longest.increasing;

/**
 * Problem Statement #
 * Given a number sequence, find the length of its Longest Increasing Subsequence (LIS).
 * In an increasing subsequence, all the elements are in increasing order (from lowest to highest).
 */
public class Solution {
	public int longestIncreasingSubsequence(int[] nums) {
		return lis(nums, -1, 0);
	}

	/*
	nums=4, 2, 3, 6, 10, 1, 12
	 */
	private int lis(int[] nums, int last, int current) {
		if (current == nums.length) {
			return 0;
		}

		int c1 = 0, c2;
		if (last == -1 || nums[current] > nums[last]) {
			c1 = 1 + lis(nums, current, current + 1);
		}

		c2 = lis(nums, last, current + 1);

		return Integer.max(c1, c2);
	}
}
