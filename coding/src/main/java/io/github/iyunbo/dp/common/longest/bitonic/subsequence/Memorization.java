package io.github.iyunbo.dp.common.longest.bitonic.subsequence;

public class Memorization {
	public int longestBitonicSubsequence(int[] nums) {
		int longestLength = 0;
		int n = nums.length;
		Integer[][] memo = new Integer[n][n + 1], memoRev = new Integer[n][n + 1];
		for (int i = 0; i < n; i++) {
			int incLength = longestDecreasingRev(nums, i, -1, memo);
			int decLength = longestDecreasing(nums, i, -1, memoRev);
			longestLength = Integer.max(incLength + decLength - 1, longestLength);
		}
		return longestLength;
	}

	private int longestDecreasing(int[] nums, int current, int previous, Integer[][] memo) {
		if (current == nums.length) return 0;
		if (memo[current][previous + 1] == null) {
			int longest = 0;
			if (previous == -1 || nums[current] < nums[previous]) {
				longest = 1 + longestDecreasing(nums, current + 1, current, memo);
			}
			longest = Integer.max(longest, longestDecreasing(nums, current + 1, previous, memo));
			memo[current][previous + 1] = longest;
		}
		return memo[current][previous + 1];
	}

	private int longestDecreasingRev(int[] nums, int current, int previous, Integer[][] memo) {
		if (current == -1) return 0;
		if (memo[current][previous + 1] == null) {
			int longest = 0;
			if (previous == -1 || nums[current] < nums[previous]) {
				longest = 1 + longestDecreasingRev(nums, current - 1, current, memo);
			}
			longest = Integer.max(longest, longestDecreasingRev(nums, current - 1, previous, memo));
			memo[current][previous + 1] = longest;
		}
		return memo[current][previous + 1];
	}


}
