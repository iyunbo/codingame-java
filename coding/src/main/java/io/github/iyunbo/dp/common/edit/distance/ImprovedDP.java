package io.github.iyunbo.dp.common.edit.distance;

/**
 * Problem Statement #
 * Given strings s1 and s2, we need to transform s1 into s2 by deleting, inserting, or replacing characters.
 * Write a function to calculate the count of the minimum number of edit operations.
 */
public class ImprovedDP {

	/*
	s1=ab
	s2=bc

	i1=2, s1.charAt(i1-1)=b
	i2=2, s2.charAt(i2-1)=c
	dp=[
	     b c
	  [0,1,2],
	 a[1,1,1],
	 b[2,1,2]
	]
	 */
	public int editDistance(String s1, String s2) {
		if (s1 == null) return s2.length();
		if (s2 == null) return s1.length();

		int len1 = s1.length(), len2 = s2.length();
		// dp[i2] = the edit distance between
		// 	- frist chars of empty s1
		// 	and
		// 	- frist chars at length i2 of s2
		int[] dp = new int[len2 + 1];
		// when s1 is empty
		for (int i = 0; i <= len2; i++) {
			dp[i] = i;
		}
		for (int i1 = 1; i1 <= len1; i1++) {
			// last[i2] = the edit distance between
			//		 	- frist chars at length i1 - 1 of s1
			//		 	and
			//		 	- frist chars at length i2 of s2
			int[] last = dp;
			dp = new int[len2 + 1];
			dp[0] = i1;
			for (int i2 = 1; i2 <= len2; i2++) {
				if (s1.charAt(i1 - 1) == s2.charAt(i2 - 1)) {
					dp[i2] = last[i2 - 1];
				} else {
					dp[i2] = 1 + Integer.min(
							// deletion
							last[i2],
							Integer.min(
									// insertion
									dp[i2 - 1],
									// replacement
									last[i2 - 1]
							)
					);
				}
			}
		}

		return dp[len2];
	}


}
