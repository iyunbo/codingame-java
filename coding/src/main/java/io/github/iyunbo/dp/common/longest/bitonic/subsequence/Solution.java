package io.github.iyunbo.dp.common.longest.bitonic.subsequence;

public class Solution {
	public int longestBitonicSubsequence(int[] nums) {
		int longestLength = 0;
		for (int i = 0; i < nums.length; i++) {
			int incLength = longestDecreasingRev(nums, i, -1);
			int decLength = longestDecreasing(nums, i, -1);
			longestLength = Integer.max(incLength + decLength - 1, longestLength);
		}
		return longestLength;
	}

	private int longestDecreasing(int[] nums, int current, int previous) {
		if (current == nums.length) return 0;
		int longest = 0;
		if (previous == -1 || nums[current] < nums[previous]) {
			longest = 1 + longestDecreasing(nums, current + 1, current);
		}
		longest = Integer.max(longest, longestDecreasing(nums, current + 1, previous));
		return longest;
	}

	private int longestDecreasingRev(int[] nums, int current, int previous) {
		if (current == -1) return 0;
		int longest = 0;
		if (previous == -1 || nums[current] < nums[previous]) {
			longest = 1 + longestDecreasingRev(nums, current - 1, current);
		}
		longest = Integer.max(longest, longestDecreasingRev(nums, current - 1, previous));
		return longest;
	}


}
