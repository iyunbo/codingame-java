package io.github.iyunbo.dp.common.longest.bitonic.subsequence;

public class DP {
	public int longestBitonicSubsequence(int[] nums) {
		if (nums == null || nums.length == 0) return 0;

		int n = nums.length, longest = 0;
		// decreasing[i] = longest decreasing count at index i
		int[] decreasing = new int[n];
		// increasing[i] = longest increasing count at index i
		int[] increasing = new int[n];

		for (int i = n - 1; i >= 0; i--) {
			// each number itself is one valid decreasing subsequence
			decreasing[i] = 1;
			// j is one of the next values of i for decreasing subsequence
			for (int j = i + 1; j < n; j++) {
				if (nums[i] > nums[j] && decreasing[j] >= decreasing[i]) {
					decreasing[i] = decreasing[j] + 1;
				}
			}
		}

		for (int i = 0; i < n; i++) {
			// each number itself is one valid increasing subsequence
			increasing[i] = 1;
			// k is one of the previous values of i for increasing subsequence
			for (int k = i - 1; k >= 0; k--) {
				if (nums[i] > nums[k] && increasing[k] >= increasing[i]) {
					increasing[i] = increasing[k] + 1;
				}
			}
			longest = Integer.max(longest, decreasing[i] + increasing[i] - 1);
		}
		return longest;
	}


}
