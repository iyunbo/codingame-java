package io.github.iyunbo.dp.common.longest.subsequence.pattern.match;

/**
 * Problem Statement #
 * Given a string and a pattern, write a method to count the number of times the pattern appears in the string as a subsequence.
 */
public class Improved {

	public int subsequencePatternMatch(String str, String pattern) {
		if (str == null || pattern == null)
			return 0;

		int n = str.length(), m = pattern.length();
		if (m == 0) return 1;
		if (m > n || n == 0) return 0;

		// dp[j] = subsequence match count with first j chars of pattern
		int[] dp = new int[m + 1];
		dp[0] = 1;

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				if (str.charAt(i - 1) == pattern.charAt(j - 1)) {
					dp[j] += dp[j - 1];
				}
			}
		}

		return dp[m];
	}
}
