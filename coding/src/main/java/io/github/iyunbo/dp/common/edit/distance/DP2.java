package io.github.iyunbo.dp.common.edit.distance;

/**
 * Problem Statement #
 * Given strings s1 and s2, we need to transform s1 into s2 by deleting, inserting, or replacing characters.
 * Write a function to calculate the count of the minimum number of edit operations.
 */
public class DP2 {

	/*
	s1=ab
	s2=bc

	i1=2, s1.charAt(i1-1)=b
	i2=2, s2.charAt(i2-1)=c
	dp=[
	     b c
	  [0,1,2],
	 a[1,1,1],
	 b[2,1,2]
	]
	 */
	public int editDistance(String s1, String s2) {
		if (s1 == null) return s2.length();
		if (s2 == null) return s1.length();

		int len1 = s1.length(), len2 = s2.length();
		// dp[i1][i2] = the edit distance between
		// 	- frist chars of length i1 of s1
		// 	and
		// 	- frist chars at length i2 of s2
		int[][] dp = new int[len1 + 1][len2 + 1];
		// if s2 is empty, distance = remove all s1
		for (int i = 0; i <= len1; i++) {
			dp[i][0] = i;
		}
		// if s1 is empty, distance = remove all s2
		for (int i = 0; i <= len2; i++) {
			dp[0][i] = i;
		}

		for (int i1 = 1; i1 <= len1; i1++) {
			for (int i2 = 1; i2 <= len2; i2++) {
				if (s1.charAt(i1 - 1) == s2.charAt(i2 - 1)) {
					dp[i1][i2] = dp[i1 - 1][i2 - 1];
				} else {
					dp[i1][i2] = 1 + Integer.min(
							// deletion
							dp[i1 - 1][i2],
							Integer.min(
									// insertion
									dp[i1][i2 - 1],
									// replacement
									dp[i1 - 1][i2 - 1]
							)
					);
				}
			}
		}

		return dp[len1][len2];
	}


}
