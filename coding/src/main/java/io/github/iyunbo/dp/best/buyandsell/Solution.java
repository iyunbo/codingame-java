package io.github.iyunbo.dp.best.buyandsell;

public class Solution {
	public int maxProfit(int[] prices) {
		int lowestPrice = Integer.MAX_VALUE;
		int maxProfit = 0;
		for (int price : prices) {
			if (price < lowestPrice) lowestPrice = price;
			if (price - lowestPrice > maxProfit)
				maxProfit = price - lowestPrice;
		}
		return maxProfit;
	}
}
