package io.github.iyunbo.dp.palindromic.longest.subsequence;

import java.util.Arrays;

/**
 * Problem Statement
 * Given a sequence, find the length of its Longest Palindromic Subsequence (LPS).
 * In a palindromic subsequence, elements read the same backward and forward.
 * <p>
 * A subsequence is a sequence that can be derived from another sequence by deleting some or no elements without changing the order of the remaining elements.
 */
public class Solution {
	/*
	str = "" -> ""
	str = "a" -> a
	str = " a " -> " a "
	str = "abdbca"
	 */
	public int findLPSLength(String str) {
		int n = str.length();
		int[][] dp = new int[n][n];
		for (int i = 0; i < n; i++) {
			Arrays.fill(dp[i], -1);
		}
		return findLPSLRecursive(str, 0, n - 1, dp);
	}

	/*
	T = O(N*N)
	S = O(N*N), depth of call: N
	 */
	private int findLPSLRecursive(String str, int start, int end, int[][] dp) {
		if (start > end) return dp[start][end] = 0;
		if (start == end) dp[start][end] = 1;
		if (dp[start][end] == -1) {
			if (str.charAt(start) == str.charAt(end)) {
				dp[start][end] = 2 + findLPSLRecursive(str, start + 1, end - 1, dp);
			} else {
				dp[start][end] = Integer.max(findLPSLRecursive(str, start + 1, end, dp), findLPSLRecursive(str, start, end - 1, dp));
			}
		}
		return dp[start][end];
	}
}
