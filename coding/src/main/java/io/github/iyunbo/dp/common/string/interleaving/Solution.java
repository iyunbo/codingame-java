package io.github.iyunbo.dp.common.string.interleaving;

/**
 * Problem Statement #
 * Give three strings ‘m’, ‘n’, and ‘p’, write a method to find out if ‘p’ has been formed by interleaving ‘m’ and ‘n’.
 * ‘p’ would be considered interleaving ‘m’ and ‘n’ if it contains all the letters from ‘m’ and ‘n’ and the order of letters is preserved too.
 */
public class Solution {
	public boolean stringInterleaving(String s1, String s2, String s3) {
		return findSIRemaining(s1, s2, s3, 0, 0, 0);
	}

	private boolean findSIRemaining(String s1, String s2, String s3, int i1, int i2, int i3) {
		if (i1 == s1.length() && i2 == s2.length() && i3 == s3.length())
			return true;
		if (i3 == s3.length())
			return false;

		boolean si1 = false, si2 = false;
		if (i1 < s1.length() && s1.charAt(i1) == s3.charAt(i3)) {
			si1 = findSIRemaining(s1, s2, s3, i1 + 1, i2, i3 + 1);
		}
		if (i2 < s2.length() && s2.charAt(i2) == s3.charAt(i3)) {
			si2 = findSIRemaining(s1, s2, s3, i1, i2 + 1, i3 + 1);
		}

		return si1 || si2;
	}
}
