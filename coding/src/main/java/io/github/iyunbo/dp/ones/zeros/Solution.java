package io.github.iyunbo.dp.ones.zeros;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {
	private Map<String, List<Integer>> strToZeroOne;
	private int[][][] memory;

	public int findMaxForm(String[] strs, int m, int n) {
		strToZeroOne = new HashMap<>();
		memory = new int[strs.length][m + 1][n + 1];
		for (String str : strs) {
			if (!strToZeroOne.containsKey(str)) {
				int len = str.length();
				String ones = str.replaceAll("0", "");
				strToZeroOne.put(str, List.of(len - ones.length(), ones.length()));
			}
		}

		return findMax(strs, m, n, strs.length);
	}

	private int findMax(String[] strs, int maxZeros, int maxOnes, int len) {
		if (len == 0 || (maxZeros == 0 && maxOnes == 0)) return 0;

		if (memory[len - 1][maxZeros][maxOnes] != 0) {
			return memory[len - 1][maxZeros][maxOnes];
		}

		List<Integer> zeroOne = strToZeroOne.get(strs[len - 1]);

		int val;
		if (zeroOne.get(0) > maxZeros || zeroOne.get(1) > maxOnes) {
			val = findMax(strs, maxZeros, maxOnes, len - 1);
		} else {
			val = Integer.max(
					findMax(strs, maxZeros, maxOnes, len - 1),
					1 + findMax(strs, maxZeros - zeroOne.get(0), maxOnes - zeroOne.get(1), len - 1)
			);
		}
		memory[len - 1][maxZeros][maxOnes] = val;
		return val;
	}
}
