package io.github.iyunbo.dp.best.buyandsell;

public class DP {
	public int maxProfit(int[] prices) {
		int n = prices.length;
		int[] maxProfit = new int[n];
		int[] lowestPrice = new int[n];
		lowestPrice[0] = prices[0];
		maxProfit[0] = 0;
		for (int i = 1; i < n; i++) {
			int price = prices[i];
			lowestPrice[i] = Integer.min(price, lowestPrice[i - 1]);
			maxProfit[i] = Integer.max(maxProfit[i - 1], price - lowestPrice[i - 1]);
		}
		return maxProfit[n - 1];
	}
}
