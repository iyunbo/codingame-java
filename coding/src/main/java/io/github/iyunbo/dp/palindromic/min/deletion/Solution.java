package io.github.iyunbo.dp.palindromic.min.deletion;

/**
 * Problem Statement #
 * Given a string, find the minimum number of characters that we can remove to make it a palindrome.
 * <p>
 * see also: io.github.iyunbo.dynamic.palindromic.longest.subsequence.ImprovedDP
 */
public class Solution {

	public int minDeletions(String str) {
		int longest = findLongestSubsequence(str);
		return str.length() - longest;
	}

	private int findLongestSubsequence(String str) {
		if (str == null || str.length() == 0) return 0;

		int n = str.length();
		int[][] dp = new int[n][n];

		for (int left = n - 1; left >= 0; left--) {
			for (int right = 0; right < n; right++) {
				if (left > right) {
					dp[left][right] = 0;
				} else if (left == right) {
					dp[left][right] = 1;
				} else {
					if (str.charAt(left) == str.charAt(right)) {
						dp[left][right] = 2 + dp[left + 1][right - 1];
					} else {
						dp[left][right] = Integer.max(dp[left + 1][right], dp[left][right - 1]);
					}
				}
			}
		}

		return dp[0][n - 1];
	}
}
