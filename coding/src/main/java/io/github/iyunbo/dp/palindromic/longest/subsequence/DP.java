package io.github.iyunbo.dp.palindromic.longest.subsequence;

/**
 * Problem Statement
 * Given a sequence, find the length of its Longest Palindromic Subsequence (LPS).
 * In a palindromic subsequence, elements read the same backward and forward.
 * <p>
 * A subsequence is a sequence that can be derived from another sequence by deleting some or no elements without changing the order of the remaining elements.
 */
public class DP {
	/*
	str = "" -> ""
	str = "a" -> a
	str = " a " -> " a "
	str = "abdbca"

	T = O(N*N)
	S = O(N*N)
	 */
	public int findLPSLength(String str) {
		int n = str.length();
		int[][] dp = new int[n][n];
		for (int start = n - 1; start >= 0; start--) {
			for (int end = 0; end < n; end++) {
				if (start == end) {
					dp[start][end] = 1;
				} else if (start > end) {
					dp[start][end] = 0;
				} else {
					if (str.charAt(start) == str.charAt(end)) {
						dp[start][end] = dp[start + 1][end - 1] + 2;
					} else {
						dp[start][end] = Integer.max(dp[start + 1][end], dp[start][end - 1]);
					}
				}
			}
		}
		return dp[0][n - 1];
	}

}
