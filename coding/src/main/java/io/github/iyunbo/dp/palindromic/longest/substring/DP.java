package io.github.iyunbo.dp.palindromic.longest.substring;

/**
 * Problem Statement
 * Given a string, find the length of its Longest Palindromic Substring (LPS).
 * In a palindromic string, elements read the same backward and forward.
 */
public class DP {


	/*
	clarify:
	"" -> true
	"a" -> true
	"aa" -> true
	"ab" -> false
	"aba" -> true

	T = O(N*N)
	S = O(N*N)
	 */
	public String longestPalindrome(String s) {
		if (s == null || s.length() <= 1) return s;
		int n = s.length();
		String[][] dp = new String[n][n];
		for (int left = n - 1; left >= 0; left--) {
			for (int right = 0; right < n; right++) {
				if (left > right) {
					dp[left][right] = "";
				} else if (left == right) {
					dp[left][right] = s.substring(left, left + 1);
				} else {
					// if two pointers equal and inside substring is exactly a palindrome
					if (s.charAt(left) == s.charAt(right) && dp[left + 1][right - 1].length() == right - left - 1) {
						dp[left][right] = s.charAt(left) + dp[left + 1][right - 1] + s.charAt(right);
					} else {
						String leftSub = dp[left][right - 1];
						String rightSub = dp[left + 1][right];
						if (leftSub.length() >= rightSub.length()) {
							dp[left][right] = leftSub;
						} else {
							dp[left][right] = rightSub;
						}
					}
				}
			}
		}
		return dp[0][n - 1];
	}


}
