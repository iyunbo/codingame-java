package io.github.iyunbo.dp.coin.change.min;

import java.util.Arrays;

public class Solution {

	/*
	coins=[1,2,5],amount=11
	state=[0,1,2,.......]
	i=3
	c=3
	s[3]=1
	 */
	public int minCoinChange(int[] coins, int amount) {
		final int[] state = new int[amount + 1];
		Arrays.fill(state, amount + 1);

		state[0] = 0;
		for (int i = 1; i <= amount; i++) {
			for (int coin : coins) {
				if (coin <= i) {
					if (state[i - coin] + 1 < state[i]) {
						state[i] = state[i - coin] + 1;
					}
				}
			}
		}
		return state[amount] > amount ? -1 : state[amount];
	}
}
