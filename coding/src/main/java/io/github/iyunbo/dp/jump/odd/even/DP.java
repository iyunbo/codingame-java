package io.github.iyunbo.dp.jump.odd.even;

import java.util.TreeMap;

public class DP {

	public int oddEvenJumps(int[] A) {
		int n = A.length;
		if (n <= 1) return n;

		boolean[] odd = new boolean[n];
		boolean[] even = new boolean[n];
		odd[n - 1] = even[n - 1] = true;
		final TreeMap<Integer, Integer> map = new TreeMap<>();
		map.put(A[n - 1], n - 1);

		for (int i = n - 2; i >= 0; i--) {
			int val = A[i];
			if (map.containsKey(val)) {
				odd[i] = even[map.get(val)];
				even[i] = odd[map.get(val)];
			} else {
				Integer higher = map.higherKey(val);
				Integer lower = map.lowerKey(val);
				if (higher != null) {
					odd[i] = even[map.get(higher)];
				}
				if (lower != null) {
					even[i] = odd[map.get(lower)];
				}
			}
			map.put(val, i);
		}

		int count = 0;
		for (boolean good : odd) {
			if (good) count++;
		}
		return count;
	}
}
