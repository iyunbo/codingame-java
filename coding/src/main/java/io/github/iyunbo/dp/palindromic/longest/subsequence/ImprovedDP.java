package io.github.iyunbo.dp.palindromic.longest.subsequence;

/**
 * Problem Statement
 * Given a sequence, find the length of its Longest Palindromic Subsequence (LPS).
 * In a palindromic subsequence, elements read the same backward and forward.
 * <p>
 * A subsequence is a sequence that can be derived from another sequence by deleting some or no elements without changing the order of the remaining elements.
 */
public class ImprovedDP {
	/*
	str = "" -> ""
	str = "a" -> a
	str = " a " -> " a "
	str = "abdbca"

	T = O(N*N)
	S = O(N)
	 */
	public int findLPSLength(String str) {
		int n = str.length();
		int[] dp;
		int[] last = new int[n];
		last[n - 1] = 1;
		for (int start = n - 2; start >= 0; start--) {
			dp = new int[n];
			for (int end = 0; end < n; end++) {
				if (start == end) {
					dp[end] = 1;
				} else if (start > end) {
					dp[end] = 0;
				} else {
					if (str.charAt(start) == str.charAt(end)) {
						dp[end] = last[end - 1] + 2;
					} else {
						dp[end] = Integer.max(last[end], dp[end - 1]);
					}
				}
			}
			last = dp;
		}
		return last[n - 1];
	}

}
