package io.github.iyunbo.dp.jump.min;

import java.util.Arrays;

public class DP {
	public int countMinJumps(int[] jumps) {
		if (jumps.length <= 1) return 0;
		int n = jumps.length;

		int[] dp = new int[n];
		Arrays.fill(dp, Integer.MAX_VALUE);
		dp[n - 1] = 0;

		for (int i = n - 2; i >= 0; i--) {
			if (i + jumps[i] >= n - 1) {
				dp[i] = 1;
			} else {
				for (int j = 1; j <= jumps[i]; j++) {
					int jumpTo = dp[i + j] + 1;
					if (dp[i + j] != Integer.MAX_VALUE && jumpTo < dp[i]) {
						dp[i] = jumpTo;
					}
				}
			}
		}

		return dp[0];
	}
}
