package io.github.iyunbo.dp.house.thief;

/**
 * There are ‘n’ houses built in a line. A thief wants to steal maximum possible money from these houses.
 * The only restriction the thief has is that he can’t steal from two consecutive houses, as that would alert the security system.
 * How should the thief maximize his stealing?
 * <p>
 * Problem Statement
 * Given a number array representing the wealth of ‘n’ houses, determine the maximum amount of money the thief can steal without alerting the security system.
 */
public class Solution {
	/*
	clarify:
	wealth > 0
	when wealth.length == 0 -> -1

	[1, 2, 3, 4, 5, 6]
	dp[0] = 1
	dp[1] = max(dp[0], 2) = 2
	dp[2] = max(dp[1], dp[0]+3) = 4
	dp[3] = max(dp[2], dp[1]+4) = 6
	 */
	public int maxSteal(int[] wealth) {
		if (wealth == null || wealth.length == 0) return -1;
		int n = wealth.length;
		if (n == 1) return wealth[0];
		int[] dp = new int[n];
		dp[0] = wealth[0];
		dp[1] = Integer.max(dp[0], wealth[1]);
		for (int i = 2; i < n; i++) {
			dp[i] = Integer.max(dp[i - 1], dp[i - 2] + wealth[i]);
		}
		return dp[n - 1];
	}
}
