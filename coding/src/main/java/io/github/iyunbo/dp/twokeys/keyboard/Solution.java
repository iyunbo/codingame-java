package io.github.iyunbo.dp.twokeys.keyboard;

import java.util.Arrays;

public class Solution {
	public int minSteps(int n) {
		int[] steps = new int[n + 1];
		Arrays.fill(steps, Integer.MAX_VALUE);
		steps[0] = 0;
		steps[1] = 0;
		for(int copy = 1; copy <= n/2; copy++){
			for(int paste = 2; copy * paste <= n; paste ++){
				int num = copy * paste;
				// copyAll + paste until n - 1 because we already have the first copy
				steps[num] = Integer.min(steps[num], steps[copy] + 1 + paste - 1);
			}
		}

		return steps[n];
	}
}