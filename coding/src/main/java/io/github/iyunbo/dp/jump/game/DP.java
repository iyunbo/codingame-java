package io.github.iyunbo.dp.jump.game;


import java.util.HashMap;
import java.util.Map;

public class DP {

	private final Map<Integer, Boolean> memory = new HashMap<>();

	public boolean canJump(int[] nums) {
		memory.clear();
		memory.put(nums.length - 1, true);
		for (int j = nums.length - 2; j >= 0; j--) {
			int furthest = Integer.min(nums[j] + j, nums.length - 1);
			for (int i = j + 1; i <= furthest; i++) {
				if (memory.containsKey(i)) {
					memory.put(j, true);
					break;
				}
			}
		}

		return memory.containsKey(0);
	}
}