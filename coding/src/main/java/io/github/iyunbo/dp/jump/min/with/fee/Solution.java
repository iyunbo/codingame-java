package io.github.iyunbo.dp.jump.min.with.fee;

import java.util.Collections;
import java.util.List;

/**
 * Problem Statement
 * Given a staircase with ‘n’ steps and an array of ‘n’ numbers representing the fee that you have to pay if you take the step.
 * Implement a method to calculate the minimum fee required to reach the top of the staircase (beyond the top-most step).
 * At every step, you have an option to take either 1 step, 2 steps, or 3 steps. You should assume that you are standing at the first step.
 */
public class Solution {

	/*
	clarify:
	fee => 0
	n > 3
	if n == 0 -> -1

	example
	[0, 1, 2, 3, 4, 5, 6]
	6 -> 6
	5 -> 5
	4 -> 4
	3 -> 3 + min(4, 5, 6) = 7
	2 -> 2 + min(7, 5, 6) = 7
	1 -> 1 + min(7, 7, 4) = 5
	0 -> 0 + min(5, 7, 7) = 5

	T = O(N)
	S = O(N)
	 */
	public int minFee(int[] fee) {
		if (fee == null || fee.length == 0) return -1;
		int n = fee.length;
		if (n <= 3) return fee[0];
		int[] dp = new int[n];
		dp[n - 1] = fee[n - 1];
		dp[n - 2] = fee[n - 2];
		dp[n - 3] = fee[n - 3];
		for (int i = n - 4; i >= 0; i--) {
			dp[i] = fee[i] + Collections.min(List.of(dp[i + 1], dp[i + 2], dp[i + 3]));
		}

		return dp[0];
	}
}
