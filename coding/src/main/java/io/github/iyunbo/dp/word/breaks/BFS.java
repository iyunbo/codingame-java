package io.github.iyunbo.dp.word.breaks;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/*
dcatsandog -> false
*/
public class BFS {

	public boolean wordBreak(String s, List<String> wordDict) {
		final Set<Integer> visited = new HashSet<>(s.length());
		final Queue<Integer> q = new LinkedList<>();
		q.add(0);
		final Set<String> dict = new HashSet<>(wordDict);
		while (!q.isEmpty()) {
			int start = q.remove();
			if (start == s.length()) return true;
			if (!visited.contains(start)) {
				for (int end = start + 1; end <= s.length(); end++) {
					if (dict.contains(s.substring(start, end))) {
						q.add(end);
					}
				}
				visited.add(start);
			}
		}
		return false;
	}
}