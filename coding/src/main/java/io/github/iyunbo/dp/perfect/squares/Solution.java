package io.github.iyunbo.dp.perfect.squares;

public class Solution {
	public int numSquares(int n) {
		int[] state = new int[n + 1];
		for (int i = 0; i <= n; i++) {
			state[i] = i;
		}
		for (int i = 2; i <= n; i++) {
			for (int j = 2; j * j <= i; j++) {
				state[i] = Integer.min(state[i], state[i - j * j] + 1);
			}
		}
		return state[n];
	}
}
