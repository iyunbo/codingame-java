package io.github.iyunbo.dp.common.shortest.supersequence;

/**
 * Problem Statement #
 * Given two sequences ‘s1’ and ‘s2’, write a method to find the length of the shortest sequence which has ‘s1’ and ‘s2’ as subsequences.
 */
public class Improved {

	public int shortestCommonSupersequence(String s1, String s2) {
		if (s1 == null || s2 == null) return 0;

		int l1 = s1.length(), l2 = s2.length();
		int[] dp = new int[l2 + 1];

		for (int i = 0; i <= l1; i++) {
			int[] last = dp;
			dp = new int[l2 + 1];
			for (int j = 0; j <= l2; j++) {
				if (i == 0) {
					dp[j] = j;
				} else if (j == 0) {
					dp[j] = i;
				} else if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					dp[j] = 1 + last[j - 1];
				} else {
					dp[j] = 1 + Integer.min(last[j], dp[j - 1]);
				}
			}
		}

		return dp[l2];
	}
}
