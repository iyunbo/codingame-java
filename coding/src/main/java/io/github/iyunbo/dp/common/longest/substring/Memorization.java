package io.github.iyunbo.dp.common.longest.substring;

import java.util.Collections;
import java.util.List;

/**
 * Problem Statement #
 * Given two strings ‘s1’ and ‘s2’, find the length of the longest substring which is common in both the strings.
 */
public class Memorization {
	public int longestCommonSubstring(String s1, String s2) {
		return lcs(s1, s2, 0, 0, 0,
				new Integer[s1.length()][s2.length()][Integer.max(s1.length(), s2.length())]
		);
	}

	/*
	 * s1 = "ab[d]ca"
	 * s2 = "[c]bda"
	 */
	private int lcs(String s1, String s2, int index1, int index2, int length, Integer[][][] memo) {
		if (s1 == null || s2 == null) return 0;

		if (index1 == s1.length() || index2 == s2.length()) return length;

		if (memo[index1][index2][length] != null) {
			return memo[index1][index2][length];
		}

		int count = length;
		if (s1.charAt(index1) == s2.charAt(index2)) {
			count = lcs(s1, s2, index1 + 1, index2 + 1, length + 1, memo);
		}

		count = Collections.max(
				List.of(count,
						lcs(s1, s2, index1 + 1, index2, 0, memo),
						lcs(s1, s2, index1, index2 + 1, 0, memo))
		);
		memo[index1][index2][length] = count;
		return count;
	}
}
