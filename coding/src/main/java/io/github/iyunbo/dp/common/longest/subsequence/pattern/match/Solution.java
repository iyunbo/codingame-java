package io.github.iyunbo.dp.common.longest.subsequence.pattern.match;

/**
 * Problem Statement #
 * Given a string and a pattern, write a method to count the number of times the pattern appears in the string as a subsequence.
 */
public class Solution {

	public int subsequencePatternMatch(String str, String pattern) {
		if (str == null || pattern == null)
			return 0;

		int n = str.length(), m = pattern.length();
		if (m == 0) return 1;
		if (m > n || n == 0) return 0;

		// dp[i][j] = subsequence match count with first i chars of str and first j chars of pattern
		int[][] dp = new int[n + 1][m + 1];
		dp[0][0] = 1;

		for (int i = 1; i <= n; i++) {
			dp[i][0] = 1;
			for (int j = 1; j <= m; j++) {
				if (str.charAt(i - 1) == pattern.charAt(j - 1)) {
					dp[i][j] = dp[i - 1][j - 1];
				}
				dp[i][j] += dp[i - 1][j];
			}
		}

		return dp[n][m];
	}
}
