package io.github.iyunbo.dp.targets.sum;

public class DP {


	public int findTargetSumWays(int[] nums, int S) {
		int[][] dp = new int[nums.length + 1][S + 1];
		dp[0][0] = 1;
		for (int i = 1; i <= nums.length; i++) {
			int num = nums[i - 1];
			for (int s = 0; s <= S; s++) {
				if (num > s) {
					dp[i][s] = dp[i - 1][s];
				} else {
					dp[i][s] = dp[i - 1][s - num] + dp[i - 1][s];
				}
			}
		}

		return dp[nums.length][S];
	}

}