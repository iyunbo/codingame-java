package io.github.iyunbo.dp.max.subarray;

public class DP {
	public int maxSubArray(int[] nums) {
		if (nums == null || nums.length == 0) return 0;
		int[] state = new int[nums.length];
		state[0] = nums[0];
		int maxSum = nums[0];

		for (int i = 1; i < nums.length; i++) {
			if (state[i - 1] > 0) {
				state[i] = nums[i] + state[i - 1];
			} else {
				state[i] = nums[i];
			}
			maxSum = Integer.max(state[i], maxSum);
		}

		return maxSum;
	}
}
