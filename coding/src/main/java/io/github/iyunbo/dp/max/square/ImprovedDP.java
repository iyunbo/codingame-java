package io.github.iyunbo.dp.max.square;

import java.util.Collections;
import java.util.List;

public class ImprovedDP {
	// T = O(M*N), S = O(N)
	public int maximalSquare(char[][] matrix) {
		if (matrix.length == 0) return 0;
		int[] dp = new int[matrix[0].length + 1];
		int maxSquareLen = 0;
		int prev = 0;
		for (int i = 1; i <= matrix.length; i++) {
			for (int j = 1; j <= matrix[0].length; j++) {
				int temp = dp[j];
				if (matrix[i - 1][j - 1] == '1') {
					dp[j] = Collections.min(List.of(
							dp[j - 1], // previous col of the same row: 1
							dp[j], // current col of previous row: 2
							prev // previous col of previous row: 3
							/*
							   |
						3.prev |  2. dp[j]
						--------------
					 1.dp[j-1] |  new dp[j]
							   |

							 */
					)) + 1;
					maxSquareLen = Integer.max(dp[j], maxSquareLen);
				}else{
					dp[j] = 0;
				}
				prev = temp;
			}
		}
		return maxSquareLen * maxSquareLen;
	}
}
