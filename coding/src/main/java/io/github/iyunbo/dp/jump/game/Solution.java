package io.github.iyunbo.dp.jump.game;


import java.util.HashMap;
import java.util.Map;

public class Solution {

	private Map<Integer, Boolean> memory = new HashMap<>();

	public boolean canJump(int[] nums) {
		memory.clear();
		return canJumpFrom(nums, 0);
	}

	private boolean canJumpFrom(int[] nums, int pos){
		if(pos == nums.length - 1) return true;

		if(memory.containsKey(pos)) return memory.get(pos);

		int furthest = Integer.min(nums[pos] + pos, nums.length - 1);
		for(int i = furthest; i > pos; i --){
			if(canJumpFrom(nums, i)){
				memory.put(pos, true);
				memory.put(i, true);
				return true;
			}else{
				memory.put(i, false);
			}
		}

		memory.put(pos, false);
		return false;

	}
}