package io.github.iyunbo.dp.common.longest.substring;

import java.util.Collections;
import java.util.List;

/**
 * Problem Statement #
 * Given two strings ‘s1’ and ‘s2’, find the length of the longest substring which is common in both the strings.
 */
public class Solution {
	public int longestCommonSubstring(String s1, String s2) {
		return lcs(s1, s2, 0, 0, 0);
	}

	/*
	 * s1 = "ab[d]ca"
	 * s2 = "[c]bda"
	 */
	private int lcs(String s1, String s2, int index1, int index2, int len) {
		if (s1 == null || s2 == null) return 0;
		if (index1 == s1.length() || index2 == s2.length()) return len;


		if (s1.charAt(index1) == s2.charAt(index2)) {
			len = lcs(s1, s2, index1 + 1, index2 + 1, len + 1);
		}

		return Collections.max(
				List.of(len,
						lcs(s1, s2, index1 + 1, index2, 0),
						lcs(s1, s2, index1, index2 + 1, 0))
		);
	}
}
