package io.github.iyunbo.dp.knight.dialer;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Solution {
	public int knightDialer(int n) {
		Map<Integer, List<Integer>> nextNums = Map.of(
				0, List.of(4, 6),
				1, List.of(6, 8),
				2, List.of(7, 9),
				3, List.of(4, 8),
				4, List.of(0, 3, 9),
				5, List.of(),
				6, List.of(0, 1, 7),
				7, List.of(2, 6),
				8, List.of(1, 3),
				9, List.of(2, 4)
		);
		int[] prevDp = new int[10];
		int[] dp = new int[10];
		Arrays.fill(prevDp, 1);
		Arrays.fill(dp, 1);
		for (int i = 2; i <= n; i++) {
			dp = new int[10];
			for (int pre = 0; pre < 10; pre++) {
				for (int current : nextNums.get(pre)) {
					dp[current] += prevDp[pre];
					dp[current] %= 1000000007;
				}
			}
			prevDp = dp;
		}
		int sum = 0;
		for (int s : dp) {
			sum += s;
			sum %= 1000000007;
		}
		return sum;
	}
}
