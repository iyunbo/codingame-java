package io.github.iyunbo.dp.min.cost.tickets;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
state[i] = cost from i to end
state[i] = wait until next traval day if not traval day
else state[i] = min(dp[i+1] + cost[0], dp[i+7] + cost[1], dp[i+30] + cost[3])
 */
public class Solution {

	private int[] costs;
	private Set<Integer> days;
	private Integer[] state;

	public int mincostTickets(int[] days, int[] costs) {
		if (days == null || days.length == 0) return 0;
		this.costs = costs;
		this.state = new Integer[days[days.length - 1] + 1];
		this.days = new HashSet<>();
		for (int d : days) this.days.add(d);

		return dp(days[0], days[days.length - 1]);
	}

	private int dp(int i, int lastDay) {
		if (i > lastDay) return 0;
		if (state[i] != null) return state[i];
		int result;
		if (days.contains(i)) {
			result = Collections.min(List.of(
					dp(i + 1, lastDay) + this.costs[0],
					dp(i + 7, lastDay) + this.costs[1],
					dp(i + 30, lastDay) + this.costs[2]
			));
		} else {
			result = dp(i + 1, lastDay);
		}
		state[i] = result;
		return result;
	}
}
