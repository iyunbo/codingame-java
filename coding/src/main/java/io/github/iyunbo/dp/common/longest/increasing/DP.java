package io.github.iyunbo.dp.common.longest.increasing;

import java.util.Arrays;

/**
 * Problem Statement #
 * Given a number sequence, find the length of its Longest Increasing Subsequence (LIS).
 * In an increasing subsequence, all the elements are in increasing order (from lowest to highest).
 */
public class DP {
	/*
	-4, 10, 3, 7, 15
	n = 5
	i = 4, nums[i] = 15
	j = 3, nums[j] = 7
	dp = 1, 2, 2, 3, 4
	 */
	public int longestIncreasingSubsequence(int[] nums) {
		if (nums == null || nums.length == 0) return 0;

		int n = nums.length;
		int[] dp = new int[n];
		Arrays.fill(dp, 1);
		int maxLength = 1;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < i; j++) {
				if (nums[i] > nums[j] && dp[i] <= dp[j]) {
					dp[i] = dp[j] + 1;
					maxLength = Integer.max(maxLength, dp[i]);
				}
			}
		}
		return maxLength;
	}


}
