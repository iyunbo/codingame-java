package io.github.iyunbo.dp.common.shortest.supersequence;

/**
 * Problem Statement #
 * Given two sequences ‘s1’ and ‘s2’, write a method to find the length of the shortest sequence which has ‘s1’ and ‘s2’ as subsequences.
 */
public class Solution {

	/*
	s1: "abcf"
	s2: "bdcf"

	i = 4, s1.charAt(i - 1) = f
	j = 4, s2.charAt(j - 1) = f
	dp=[
		[0,1,2,3,4],
		[1,2,3,4,5],
		[2,2,3,4,5],
		[3,3,4,4,5],
		[4,4,5,5,5],
	]
	 */
	public int shortestCommonSupersequence(String s1, String s2) {
		if (s1 == null || s2 == null) return 0;

		int l1 = s1.length(), l2 = s2.length();
		// the shortest common supersequence between first i chars of s1 and first j chars of s2
		int[][] dp = new int[l1 + 1][l2 + 1];

		for (int i = 0; i <= l1; i++) {
			for (int j = 0; j <= l2; j++) {
				if (i == 0) {
					dp[i][j] = j;
				} else if (j == 0) {
					dp[i][j] = i;
				} else if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					dp[i][j] = 1 + dp[i - 1][j - 1];
				} else {
					dp[i][j] = 1 + Integer.min(dp[i - 1][j], dp[i][j - 1]);
				}
			}
		}

		return dp[l1][l2];
	}
}
