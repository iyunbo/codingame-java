package io.github.iyunbo.dp.palindromic.min.deletion;

/**
 * Here are a couple of similar problems:
 * <p>
 * 1. Minimum insertions in a string to make it a palindrome #
 * Will the above approach work if we make insertions instead of deletions?
 * <p>
 * Yes, the length of the Longest Palindromic Subsequence is the best palindromic subsequence we can have.
 * <p>
 * So the minimum insertion is the minimum number of chars we need to delete to make the palindrome
 */
public class MinInsertion {

	public int minDeletions(String str) {
		Solution solution = new Solution();
		return solution.minDeletions(str);
	}


}
