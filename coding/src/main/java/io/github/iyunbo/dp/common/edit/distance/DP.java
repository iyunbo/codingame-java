package io.github.iyunbo.dp.common.edit.distance;

/**
 * Problem Statement #
 * Given strings s1 and s2, we need to transform s1 into s2 by deleting, inserting, or replacing characters.
 * Write a function to calculate the count of the minimum number of edit operations.
 */
public class DP {

	/*
	s1=ab
	s2=bc

	i1=0, s1.charAt(i1)=a
	i2=0, s2.charAt(i2)=c
	dp=[
	 [0,1,0],
	 [0,1,0],
	 [0,0,0]
	]
	 */
	public int editDistance(String s1, String s2) {
		if (s1 == null) return s2.length();
		if (s2 == null) return s1.length();

		int len1 = s1.length(), len2 = s2.length();
		// dp[i1][i2] = the edit distance between
		// 	- remaining chars (exclusive) at i1 of s1
		// 	and
		// 	- remaining chars (exclusive) at i2 of s2
		int[][] dp = new int[len1 + 1][len2 + 1];
		// at the end of len1, remaining distance will be the lenth of s2 minus prefix length
		for (int i = len2; i >= 0; i--) {
			dp[len1][i] = len2 - i;
		}
		// at the end of len2, remaining distance will be the lenth of s1 minus prefix length
		for (int i = len1; i >= 0; i--) {
			dp[i][len2] = len1 - i;
		}
		for (int i1 = len1 - 1; i1 >= 0; i1--) {
			for (int i2 = len2 - 1; i2 >= 0; i2--) {
				if (s1.charAt(i1) == s2.charAt(i2)) {
					dp[i1][i2] = dp[i1 + 1][i2 + 1];
				} else {
					dp[i1][i2] = 1 + Integer.min(
							// deletion
							dp[i1 + 1][i2],
							Integer.min(
									// insertion
									dp[i1][i2 + 1],
									// replacement
									dp[i1 + 1][i2 + 1]
							));
				}
			}
		}

		return dp[0][0];
	}


}
