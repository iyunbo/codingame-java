package io.github.iyunbo.dp.dice.simulation;

import java.util.Arrays;

public class Solution {
	public int dieSimulator(int n, int[] rollMax) {
		int[][] dp = new int[n + 1][7];
		Arrays.fill(dp[0], 1);

		for (int roll = 1; roll <= n; roll++) {
			for (int dice = 0; dice <= 6; dice++) {
				for (int num = 1; num <= 6; num++) {
					if (num == dice) continue;
					for (int suffix = 1; suffix <= rollMax[num-1] && suffix <= roll; suffix++) {
						dp[roll][dice] = (dp[roll][dice] + dp[roll-suffix][num]) % 1000000007;
					}
				}
			}
		}
		return dp[n][0];
	}
}
