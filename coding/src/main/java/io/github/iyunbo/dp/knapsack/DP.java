package io.github.iyunbo.dp.knapsack;

public class DP {


	public int knapsack(int maxWeight, int[] weights, int[] values, int n) {
		if (n == 0) return 0;
		if (n == 1) return values[0];
		int[][] dp = new int[n][maxWeight + 1];

		for (int i = 1; i < n; i++) {
			int weight = weights[i];
			for (int c = 1; c <= maxWeight; c++) {
				if (weight > c) {
					dp[i][c] = dp[i - 1][c];
				} else {
					dp[i][c] = Integer.max(dp[i - 1][c], dp[i - 1][c - weight] + values[i]);
				}
			}
		}

		return dp[n - 1][maxWeight];
	}
}
