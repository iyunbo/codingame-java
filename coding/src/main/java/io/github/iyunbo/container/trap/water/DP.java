package io.github.iyunbo.container.trap.water;

public class DP {

	//T = O(N), S = O(N)
	public int trap(int[] height) {
		if (height.length == 0) return 0;

		final int[] leftHighest = new int[height.length];
		final int[] rightHighest = new int[height.length];

		leftHighest[0] = height[0];
		for (int i = 1; i < height.length; i++) {
			leftHighest[i] = Integer.max(height[i], leftHighest[i - 1]);
		}

		rightHighest[height.length - 1] = height[height.length - 1];
		for (int i = height.length - 2; i >= 0; i--) {
			rightHighest[i] = Integer.max(height[i], rightHighest[i + 1]);
		}

		int trapped = 0;
		for (int i = 0; i < height.length; i++) {
			trapped += Integer.min(leftHighest[i], rightHighest[i]) - height[i];
		}

		return trapped;
	}

}
