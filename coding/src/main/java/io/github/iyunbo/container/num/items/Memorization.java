package io.github.iyunbo.container.num.items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Memorization {
	public List<Integer> numberOfItems(String s, List<Integer> startIndices, List<Integer> endIndices) {
		final List<Integer> result = new ArrayList<>(startIndices.size());

		final int[] containers = createContainers(s);

		//T = O(N), S = O(N)
		for (int i = 0; i < startIndices.size(); i++) {
			result.add(countItems(startIndices.get(i) - 1, endIndices.get(i) - 1, containers));
		}

		return result;
	}

	private int countItems(int start, int end, int[] containers) {
		int left = start;
		int right = end;
		while (containers[left] == -1 && left < right) left++;
		while (containers[right] == -1 && left < right) right--;

		if (left < right) {
			return containers[right] - containers[left];
		}

		return 0;
	}

	/*
	s=|**|*|*
	left=3
	right=4
	 */
	private int[] createContainers(String s) {
		final int[] counter = new int[s.length()];
		Arrays.fill(counter, -1);

		int count = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '|') {
				counter[i] = count;
			} else {
				count++;
			}
		}

		return counter;

	}
}
