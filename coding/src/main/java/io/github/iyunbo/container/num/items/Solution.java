package io.github.iyunbo.container.num.items;

import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<Integer> numberOfItems(String s, List<Integer> startIndices, List<Integer> endIndices) {
		final List<Integer> result = new ArrayList<>(startIndices.size());

		for (int i = 0; i < startIndices.size(); i++) {
			result.add(countItems(s, startIndices.get(i), endIndices.get(i)));
		}

		return result;
	}

	/*
	s=|**|*|*
	left=3
	right=4
	 */
	private int countItems(String s, int start, int end) {
		int left = start - 1;
		while (left < end && s.charAt(left) != '|') {
			left++;
		}
		if (left == end) return 0;
		int right = end;
		while (right > left + 1 && s.charAt(right - 1) != '|') {
			right--;
		}
		if (right == left + 1) return 0;

		return s.substring(left, right).replaceAll("\\|", "").length();
	}
}
