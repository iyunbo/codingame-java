package io.github.iyunbo.container.parenthese.valid;

import java.util.List;
import java.util.Stack;

public class Solution {
	// T = O(N), S=O(N)
	public boolean isValid(String s) {
		final Stack<Character> stack = new Stack<>();
		final List<Character> openChars = List.of('(', '{', '[');

		for(char ch : s.toCharArray()){
			if(openChars.contains(ch)){
				stack.add(ch);
			} else {
				if(stack.isEmpty()) return false;
				if(!match(stack.pop(), ch)) return false;
			}
		}
		return stack.isEmpty();
	}

	private boolean match(char open, char closed){
		switch(open){
			case '{': return closed == '}';
			case '(': return closed == ')';
			case '[': return closed == ']';
			default: throw new IllegalArgumentException();
		}
	}
}
