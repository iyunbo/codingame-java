package io.github.iyunbo.container.with.most.water;

public class Solution {
	public int maxArea(int[] height) {
		int left = 0, right = height.length - 1;
		int maxArea = 0;
		// T = O(n), S = O(1)
		while (left < right) {
			maxArea = Math.max(maxArea, (right - left) * Math.min(height[left], height[right]));
			if (height[left] <= height[right]) {
				while (height[left] > height[left + 1]) left++;
				left++;
			} else {
				while (height[right] > height[right - 1]) right--;
				right--;
			}
		}
		return maxArea;
	}
}
