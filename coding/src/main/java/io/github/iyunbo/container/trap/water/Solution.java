package io.github.iyunbo.container.trap.water;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class Solution {

	private class Bar {
		private final int height;
		private final int index;

		private Bar(int index, int height) {
			this.height = height;
			this.index = index;
		}

		public String toString() {
			return this.index + "->" + this.height;
		}
	}

	//T = O(N*N), S = O(N)
	public int trap(int[] height) {
		final Queue<Bar> heap = new PriorityQueue<>(Comparator.comparing(b -> -b.height));
		for (int i = 0; i < height.length; i++) {
			heap.add(new Bar(i, height[i]));
		}
		int trapped = 0;
		for (int i = 0; i < height.length; i++) {
			trapped += trappedWater(i, height[i], new PriorityQueue<>(heap));
		}

		return trapped;
	}

	/*
	heap.peep()=(3,7)
	i=5
	h=0
	leftHighest=2
	*/
	private int trappedWater(int i, int h, Queue<Bar> heap) {
		// highest is at i
		final Bar highest = heap.peek();
		if (i == highest.index) return 0;
		Bar candidate = heap.remove();
		if (i < highest.index) {
			//highest is on the right, we need to find the highest on the left
			while (!heap.isEmpty() && candidate.index >= i) {
				candidate = heap.remove();
			}
			if (candidate.index > i || candidate.height <= h) {
				return 0;
			} else {
				return candidate.height - h;
			}

		} else {
			// highest is on the left, we need to find the highest on the right
			while (!heap.isEmpty() && candidate.index <= i) {
				candidate = heap.remove();
			}
			if (candidate.index < i || candidate.height <= h) {
				return 0;
			} else {
				return candidate.height - h;
			}
		}
	}
}
