package io.github.iyunbo.design.codec;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

import io.github.iyunbo.bst.validation.TreeNode;

public class Codec {

	/*
	                     1
	            2               5
	       3        4

	 1,2,3,#,#,4,#,#,5,#,#
	 */


	// T = O(N), S = O(N)
	// Encodes a tree to a single string.
	public String serialize(TreeNode root) {
		return dfsSer(root, "");
	}

	private String dfsSer(TreeNode node, String prefix) {
		if (node == null) return prefix + "#,";
		prefix = prefix + node.val + ",";
		prefix = dfsSer(node.left, prefix);
		prefix = dfsSer(node.right, prefix);
		return prefix;
	}

	// T = O(N), S = O(N)
	// Decodes your encoded data to tree.
	public TreeNode deserialize(String data) {
		if (data.isEmpty()) return null;
		final Queue<String> chain = new LinkedList<>(Arrays.asList(data.split(",")));
		return dfsDeser(chain);
	}

	private TreeNode dfsDeser(Queue<String> chain) {
		if (chain.isEmpty()) return null;

		final String firstVal = chain.poll();
		if (firstVal.equals("#")) return null;

		final TreeNode root = new TreeNode(Integer.parseInt(firstVal));
		root.left = dfsDeser(chain);
		root.right = dfsDeser(chain);

		return root;
	}
}
