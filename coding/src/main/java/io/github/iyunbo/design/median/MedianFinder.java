package io.github.iyunbo.design.median;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class MedianFinder {

	//S = O(L)s
	private final Queue<Integer> left;
	private final Queue<Integer> right;
	private double median;

	public MedianFinder() {
		left = new PriorityQueue<>(Comparator.comparing(i -> -i));
		right = new PriorityQueue<>();
		median = Integer.MAX_VALUE;
	}


	// T = O(logL)
	public void addNum(int num) {
		if (num <= median) {
			addToLeft(num);
		} else {
			addToRight(num);
		}
	}

	private void addToRight(int num) {
		right.add(num);
		if (right.size() - left.size() == 1) {
			Integer minRight = right.poll();
			left.add(minRight);
		}
		calculateMedian();
	}

	private void calculateMedian() {
		if ((left.size() + right.size()) % 2 == 1) {
			this.median = left.peek();
		} else {
			this.median = (left.peek() + right.peek()) / 2.0;
		}
	}

	private void addToLeft(int num) {
		left.add(num);
		if (left.size() - right.size() == 2) {
			Integer maxLeft = left.poll();
			right.add(maxLeft);
		}
		calculateMedian();
	}

	// T = O(1)
	public double findMedian() {
		return this.median;
	}
}

