package io.github.iyunbo.design.median;

import java.util.HashMap;
import java.util.Map;

/*
if all numbers are from 0 and 100
 */
public class MedianForSmallNum {

	//S = O(L)
	private final Map<Integer, Integer> left;
	private final Map<Integer, Integer> right;
	private double median;

	public MedianForSmallNum() {
		left = new HashMap<>(101);
		right = new HashMap<>(101);
		median = Integer.MAX_VALUE;
	}


	// T = O(1)
	public void addNum(int num) {
		if (num <= median) {
			addToLeft(num);
		} else {
			addToRight(num);
		}
	}

	// T = O(1)
	private void addToRight(int num) {
		int count = right.getOrDefault(num, 0);
		right.put(num, count + 1);
		if (size(right) - size(left) == 1) {
			Integer minRight = getMin(right);
			removeFrom(right, minRight);
			addTo(minRight, left);
		}
		calculateMedian();
	}

	// T = O(1)
	private void removeFrom(Map<Integer, Integer> counter, Integer num) {
		if (counter.containsKey(num)) {
			counter.put(num, counter.get(num) - 1);
		}
	}

	// T = O(1)
	private Integer getMin(Map<Integer, Integer> map) {
		for (int num = 0; num <= 100; num++) {
			if (map.getOrDefault(num, 0) > 0) {
				return num;
			}
		}
		return -1;
	}

	// T = O(1)
	private Integer getMax(Map<Integer, Integer> map) {
		for (int num = 100; num >= 0; num--) {
			if (map.getOrDefault(num, 0) > 0) {
				return num;
			}
		}
		return -1;
	}

	// T = O(1)
	private void addTo(Integer num, Map<Integer, Integer> map) {
		int count = map.getOrDefault(num, 0);
		map.put(num, count + 1);
	}

	// T = O(1)
	private int size(Map<Integer, Integer> map) {
		return map.values()
				.stream()
				.reduce(Integer::sum)
				.orElse(0);
	}

	// T = O(1)
	private void calculateMedian() {
		if ((size(left) + size(right)) % 2 == 1) {
			this.median = getMax(left);
		} else {
			this.median = (getMax(left) + getMin(right)) / 2.0;
		}
	}

	// T = O(1)
	private void addToLeft(int num) {
		int count = left.getOrDefault(num, 0);
		left.put(num, count + 1);
		if (size(left) - size(right) == 2) {
			Integer maxLeft = getMax(left);
			removeFrom(left, maxLeft);
			addTo(maxLeft, right);
		}
		calculateMedian();
	}

	// T = O(1)
	public double findMedian() {
		return this.median;
	}
}

