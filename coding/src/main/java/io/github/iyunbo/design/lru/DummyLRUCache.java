package io.github.iyunbo.design.lru;

import java.util.Deque;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

public class DummyLRUCache {

	private final Map<Integer, Integer> cache;
	private final Deque<Integer> queue;
	private final int capacity;

	//S = O(C)
	public DummyLRUCache(int capacity) {
		this.capacity = capacity;
		this.cache = new ConcurrentHashMap<>(capacity + 1);
		this.queue = new ConcurrentLinkedDeque<>();
	}

	//T = O(C)
	public int get(int key) {
		if (cache.containsKey(key)) {
			refresh(key);
			return cache.get(key);
		}
		return -1;
	}

	//T = O(C) ==> use double linked list with hash map to get O(1)
	private void refresh(int key) {
		if (queue.remove(key)) {
			queue.add(key);
		}
	}

	//T = O(C)
	public void put(int key, int value) {
		if (!cache.containsKey(key)) {
			allocate(key);
		} else {
			refresh(key);
		}
		cache.put(key, value);
	}

	//T = O(1)
	private void allocate(int key) {
		queue.add(key);
		if (queue.size() > capacity) {
			cache.remove(queue.removeFirst());
		}
	}
}
