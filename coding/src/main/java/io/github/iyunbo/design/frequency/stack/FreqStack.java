package io.github.iyunbo.design.frequency.stack;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class FreqStack {


	private final Map<Integer, Integer> frequencies;
	private final Map<Integer, Stack<Integer>> freqToNumbers;
	private int maxFrequency;

	public FreqStack() {
		this.frequencies = new HashMap<>();
		this.freqToNumbers = new HashMap<>();
		this.maxFrequency = 0;
	}


	// T = O(1)
	public void push(int x) {
		int freq = frequencies.getOrDefault(x, 0) + 1;
		frequencies.put(x, freq);
		if (this.maxFrequency < freq) {
			this.maxFrequency = freq;
		}
		freqToNumbers.computeIfAbsent(freq, f -> new Stack<>()).push(x);
	}


	// T = O(1)
	public int pop() {
		Stack<Integer> mostFrequent = freqToNumbers.get(maxFrequency);
		Integer result = mostFrequent.pop();
		frequencies.put(result, maxFrequency - 1);
		if (mostFrequent.isEmpty()) {
			maxFrequency--;
		}
		return result;
	}
}