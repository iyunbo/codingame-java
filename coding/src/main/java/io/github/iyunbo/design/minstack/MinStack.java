package io.github.iyunbo.design.minstack;

import java.util.Stack;

public class MinStack {

	private static class Node {
		int value;
		int minValue;
	}

	private final Stack<Node> stack;

	public MinStack() {
		this.stack = new Stack<>();
	}

	//T = O(1)
	public void push(int x) {
		Node e = new Node();
		e.value = x;
		e.minValue = Math.min(x, getMin());
		stack.push(e);
	}

	//T = O(1)
	public void pop() {
		stack.pop();
	}

	//T = O(1)
	public int top() {
		return stack.peek().value;
	}

	//T = O(1)
	public int getMin() {
		return stack.isEmpty() ? Integer.MAX_VALUE : stack.peek().minValue;
	}
}
