package io.github.iyunbo.design.lru;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LRUCache {

	private static class Node {
		private Node last;
		private Node next;
		private final int key;
		private int value;

		private Node(int key, int value) {
			this.key = key;
			this.value = value;
		}
	}

	private final Map<Integer, Node> cache;
	private final Node head;
	private final Node tail;
	private final int capacity;

	//S = O(C)
	public LRUCache(int capacity) {
		this.capacity = capacity;
		this.cache = new ConcurrentHashMap<>(capacity + 1);
		this.head = new Node(-1, -1);
		this.tail = new Node(-1, -1);

		this.head.next = tail;
		this.tail.last = head;
	}

	//T = O(1)
	public int get(int key) {
		refresh(key);
		return cache.getOrDefault(key, new Node(key, -1)).value;
	}

	//T = O(1)
	public void put(int key, int value) {
		final Node node = new Node(key, value);
		if (cache.containsKey(key)) {
			update(node);
		} else {
			allocate(node);
		}
	}

	private void update(Node node) {
		cache.get(node.key).value = node.value;
		refresh(node.key);
	}

	private void appendToQueue(Node node) {
		this.tail.last.next = node;
		node.next = this.tail;
		node.last = this.tail.last;
		this.tail.last = node;
	}

	private void removeFromQueue(Node node) {
		node.last.next = node.next;
		node.next.last = node.last;
	}

	private Node pollFromQueue() {
		final Node node = this.head.next;
		this.head.next = node.next;
		node.next.last = this.head;
		return node;
	}

	//T = O(1)
	private void refresh(int key) {
		if (cache.containsKey(key)) {
			final Node node = cache.get(key);
			removeFromQueue(node);
			appendToQueue(node);
		}
	}

	//T = O(1)
	private void allocate(Node node) {
		cache.put(node.key, node);
		appendToQueue(node);
		if (cache.size() > capacity) {
			cache.remove(pollFromQueue().key);
		}
	}
}
