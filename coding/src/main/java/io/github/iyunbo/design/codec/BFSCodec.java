package io.github.iyunbo.design.codec;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

import io.github.iyunbo.bst.validation.TreeNode;

public class BFSCodec {

	/*
	                     1
	            2               5
	       3        4

	 1,2,3,#,#,4,#,#,5,#,#
	 */


	// T = O(N), S = O(N)
	// Encodes a tree to a single string.
	public String serialize(TreeNode root) {
		return bfs(root);
	}

	private String bfs(TreeNode node) {
		if (node == null) return "";

		StringBuilder result = new StringBuilder();

		final Queue<TreeNode> queue = new LinkedList<>();
		queue.add(node);
		while (!queue.isEmpty()) {
			node = queue.remove();
			if (node == null) {
				result.append("#");
			} else {
				result.append(node.val);
				queue.add(node.left);
				queue.add(node.right);
			}
			if (!queue.isEmpty()) {
				result.append(",");
			}
		}

		return result.toString();
	}

	// T = O(N), S = O(N)
	// Decodes your encoded data to tree.
	public TreeNode deserialize(String data) {
		if (data.isEmpty()) return null;
		final Queue<String> chain = new LinkedList<>(Arrays.asList(data.split(",")));
		return bfsDeser(chain);
	}

	private TreeNode bfsDeser(Queue<String> chain) {
		if (chain.isEmpty()) return null;
		TreeNode root = new TreeNode(Integer.parseInt(chain.poll()));
		Deque<TreeNode> parents = new LinkedList<>();
		parents.add(root);
		while (!parents.isEmpty()) {
			TreeNode parent = parents.poll();
			String s = chain.poll();
			if (s.equals("#")) {
				parent.left = null;
			} else {
				parent.left = new TreeNode(Integer.parseInt(s));
				parents.add(parent.left);
			}
			s = chain.poll();
			if (s.equals("#")) {
				parent.right = null;
			} else {
				parent.right = new TreeNode(Integer.parseInt(s));
				parents.add(parent.right);
			}
		}
		return root;
	}
}
