package io.github.iyunbo.arrays.course.schedule;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Backtracking {
	public boolean canFinish(int numCourses, int[][] prerequisites) {
		if (numCourses <= 1) return true;

		final Map<Integer, List<Integer>> courseDict = new HashMap<>();

		// S = O(E + V)
		// T = O(E)
		for (int[] edge : prerequisites) {
			courseDict.putIfAbsent(edge[0], new LinkedList<>());
			courseDict.get(edge[0]).add(edge[1]);
		}

		// T = O(sum(V + V-1 + V-2 + .. +1)) = O(V(1 + V)/2) = O(V*V)
		// S = O(2V)
		for (int v = 0; v < numCourses; v++) {
			final boolean[] path = new boolean[numCourses];
			if (isCyclic(v, path, courseDict)) return false;
		}

		return true;
	}

	private boolean isCyclic(int currentCourse, boolean[] path, Map<Integer, List<Integer>> courseDict) {
		if (path[currentCourse]) return true;

		if (!courseDict.containsKey(currentCourse)) return false;

		// breadcrumb
		path[currentCourse] = true;

		// backtracking
		for (Integer nextCourse : courseDict.get(currentCourse)) {
			if (isCyclic(nextCourse, path, courseDict)) return true;
		}

		// remove breadcrumb
		path[currentCourse] = false;

		return false;
	}

}
