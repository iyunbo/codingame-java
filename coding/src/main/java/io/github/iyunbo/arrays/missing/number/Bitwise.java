package io.github.iyunbo.arrays.missing.number;

public class Bitwise {
	// T = O(n), S = O(1)
	public int missingNumber(int[] nums) {
		// initialize with n
		int n = nums.length;
		// xor every index and number
		for (int i = 0; i < nums.length; i++) {
			n ^= i ^ nums[i];
		}
		return n;
	}
}