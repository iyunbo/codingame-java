package io.github.iyunbo.arrays.linkedlist;

import java.util.HashMap;
import java.util.Map;

public class Solution {
	public Node copyRandomList(Node head) {
		int i = 0;
		Node copiedHead = null;
		Node copiedTail = null;
		final Map<Node, Node> nodeToRandom = new HashMap<>();
		// T = O(n), S = O(n)
		while (head != null) {

			Node newNode = new Node(head.val);
			newNode.random = head.random;
			nodeToRandom.put(head, newNode);

			if (copiedHead == null) {
				copiedHead = newNode;
				copiedTail = copiedHead;
			} else {
				copiedTail.next = newNode;
				copiedTail = newNode;
			}

			head = head.next;
		}

		head = copiedHead;
		while (head != null) {
			Node randomPointer = head.random;
			if (randomPointer != null) {
				head.random = nodeToRandom.get(randomPointer);
			}
			head = head.next;
		}


		return copiedHead;
	}
	/*
	[0, 1] -> [1, 2] -> [2, null]
	n = *[2, null]
	map = [0 -> *[0, 1], 1 -> *[1, 2], 2 -> *[2, null]]
	ct = *[2, null]
	h = null

	ch = *[0, *1] -> *[1, *2] -> *[2, null]
	h = *2
	rp = 2

	 */
}