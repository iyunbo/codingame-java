package io.github.iyunbo.arrays.course.schedule;

import java.util.HashSet;
import java.util.Set;

public class Solution {
	public boolean canFinish(int numCourses, int[][] prerequisites) {
		if (numCourses <= 1) return true;
		final Set<int[]> visitedEdges = new HashSet<>();
		// T = O(V * E)
		for (int i = 0; i < numCourses; i++) {
			final Set<Integer> visited = new HashSet<>();
			visited.add(i);
			if (hasCircle(i, visited, visitedEdges, prerequisites))
				return false;
		}
		return true;
	}

	/*
	[0,1], [0,2], [1,2]

	l=2
	ve=[[0,1],[1,2]]
	v=[0,1]
	p=[0,1,2]

	 */
	private boolean hasCircle(int lastCourse, Set<Integer> visited, Set<int[]> visitedEdges, int[][] prerequisites) {
		for (int[] edge : prerequisites) {
			if (!visitedEdges.contains(edge)) {
				if (lastCourse == edge[0] && visited.contains(edge[1])) {
					return true;
				}
				if (lastCourse == edge[0]) {
					final Set<Integer> newPath = new HashSet<>(visited);
					newPath.add(edge[1]);
					visitedEdges.add(edge);
					if (hasCircle(edge[1], newPath, visitedEdges, prerequisites)) {
						return true;
					}
				}
			}
		}
		return false;
	}

}
