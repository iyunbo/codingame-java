package io.github.iyunbo.arrays.reverse.kgroup;

import io.github.iyunbo.arrays.two.numbers.ListNode;

public class ConstantSpace {
	public ListNode reverseKGroup(ListNode head, int k) {

		ListNode nextHead = head;
		ListNode currentHead = head, lastTail = null;
		boolean finalHeadFixed = false;

		while (nextHead != null) {

			int i = 0;
			while (i < k && nextHead != null) {
				nextHead = nextHead.next;
				i++;
			}
			if (i < k) break;

			ListNode reversedHead = reverse(currentHead, nextHead);
			if (!finalHeadFixed) {
				head = reversedHead;
				finalHeadFixed = true;
			}
			if (lastTail != null) {
				lastTail.next = reversedHead;
			}
			lastTail = currentHead;
			lastTail.next = nextHead;
			currentHead = nextHead;

		}
		return head;
	}

/*
k = 2

               t
r                   h
                    n
2 -> 1 -> 4 -> 3 -> 5


 */

	public ListNode reverse(ListNode head, ListNode endNode) {
		ListNode resersedHead = null;
		ListNode pointer = head;
		ListNode next;
		while (pointer != endNode) {
			next = pointer.next;
			resersedHead = insert(resersedHead, pointer);
			pointer = next;
		}
		return resersedHead;
	}

	private ListNode insert(ListNode head, ListNode node) {
		node.next = head;
		return node;
	}


}

