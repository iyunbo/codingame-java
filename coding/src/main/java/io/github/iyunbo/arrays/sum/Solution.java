package io.github.iyunbo.arrays.sum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {

	public static final int FINAL_TARGET = 0;

	public List<List<Integer>> threeSum(int[] ints) {
		return twoPointers(ints);
	}

	public List<List<Integer>> twoPointers(int[] ints) {
		final List<List<Integer>> result = new ArrayList<>();
		// T = O(n * log(n))
		Arrays.sort(ints);

		// T = O(n * n)
		for (int pivot = 0; pivot < ints.length; pivot++) {
			if (pivot == 0 || ints[pivot] != ints[pivot - 1]) {
				twoSum(ints, result, pivot);
			}
		}

		return result;
	}

	private void twoSum(int[] ints, List<List<Integer>> result, int pivot) {
		int low = pivot + 1;
		int high = ints.length - 1;
		int target = FINAL_TARGET - ints[pivot];
		while (low < high) {
			if (ints[low] + ints[high] == target) {
				result.add(List.of(ints[pivot], ints[low], ints[high]));
			}
			if (ints[low] + ints[high] <= target) {
				low++;
				while (ints[low] == ints[low - 1] && low < high) low++;
			} else {
				high--;
			}
		}
	}


}
