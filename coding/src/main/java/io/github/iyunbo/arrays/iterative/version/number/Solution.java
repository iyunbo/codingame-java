package io.github.iyunbo.arrays.iterative.version.number;

/*
1.0.1 - 1.0.01 : 0
1.0.1 - 1.1.0  : -1
1.0.1 - 1.0    : 1
""    - 1      : -1
*/
public class Solution {
	public int compareVersion(String version1, String version2) {
		// split into arrays
		final String[] v1 = version1.split("\\.");
		final String[] v2 = version2.split("\\.");
		// compare each element (be aware of preceeding 0s)
		final int n = Integer.max(v1.length, v2.length);
		int num1 = 0;
		int num2 = 0;
		// T = O(N*Z)
		for(int i = 0; i < n; i++){
			if(i == v1.length) num1 = 0;
			if(i == v2.length) num2 = 0;
			if(i < v1.length) {
				num1 = toInt(v1[i]);
			}
			if(i < v2.length) {
				num2 = toInt(v2[i]);
			}
			if(num1 != num2) return Integer.compare(num1, num2);
		}
		return 0;
	}

	/*
	s=""
	start=0
	r=0
	*/
	private int toInt(String s){
		int start = 0;
		while(start < s.length() && s.charAt(start) == '0'){
			start++;
		}
		return start == s.length() ? 0 : Integer.parseInt(s.substring(start));
	}
}
