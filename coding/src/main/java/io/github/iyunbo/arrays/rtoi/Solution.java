package io.github.iyunbo.arrays.rtoi;

public class Solution {
	// T = O(n), S = O(n)
	public int romanToInt(String num) {
		Object[] result = new Object[]{0, num};
		extractRoman(result, "M", 1000);
		extractRoman(result, "CM", 900);
		extractRoman(result, "D", 500);
		extractRoman(result, "CD", 400);
		extractRoman(result, "C", 100);
		extractRoman(result, "XC", 90);
		extractRoman(result, "L", 50);
		extractRoman(result, "XL", 40);
		extractRoman(result, "X", 10);
		extractRoman(result, "IX", 9);
		extractRoman(result, "V", 5);
		extractRoman(result, "IV", 4);
		extractRoman(result, "I", 1);
		return (int) result[0];
	}

	private void extractRoman(Object[] result, String romainNumber, int number) {
		int value = (int) result[0];
		String input = (String) result[1];
		while (input.startsWith(romainNumber)) {
			value += number;
			input = input.substring(romainNumber.length());
		}
		result[0] = value;
		result[1] = input;
	}
}
