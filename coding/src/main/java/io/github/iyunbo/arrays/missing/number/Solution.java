package io.github.iyunbo.arrays.missing.number;

public class Solution {
	// T = O(n), S = O(1)
	public int missingNumber(int[] nums) {
		// calculate the sum of all 0-n
		int sum = nums.length * (nums.length + 1) / 2;
		// minus by every num
		for(int num : nums){
			sum -= num;
		}
		return sum;
	}
}