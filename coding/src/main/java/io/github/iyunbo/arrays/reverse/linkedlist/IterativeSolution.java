package io.github.iyunbo.arrays.reverse.linkedlist;


import io.github.iyunbo.arrays.two.numbers.ListNode;

public class IterativeSolution {

	public ListNode reverseList(ListNode head) {
		ListNode reversed = null;

		while (head != null) {
			ListNode next = head.next;
			head.next = reversed;
			reversed = head;
			head = next;
		}

		return reversed;
	}

	/*
r = [null]
h    n
1 -> 2 -> 3 -> 4 -> 5 -> null

r = [2 -> 1 -> null]
n
3 -> 4 -> 5 -> null

r                   h
5 -> 4 -> 3 -> 2 -> 1 -> null

h    r
1 -> 2 -> null

r    h
2 -> 1 -> null

h  r
1  null
1 -> null





 */

}
