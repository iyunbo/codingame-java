package io.github.iyunbo.arrays.two.numbers;

public class Solution {

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode root = new ListNode();
		ListNode nodeS = root;
		int carrier = 0;
		int val1, val2;
		while (l1 != null || l2 != null) {
			nodeS.next = new ListNode();
			nodeS = nodeS.next;

			val1 = l1 != null ? l1.val : 0;
			val2 = l2 != null ? l2.val : 0;

			nodeS.val = val1 + val2 + carrier;
			if (nodeS.val >= 10) {
				nodeS.val = nodeS.val - 10;
				carrier = 1;
			} else {
				carrier = 0;
			}

			if (l1 != null) l1 = l1.next;
			if (l2 != null) l2 = l2.next;
		}

		if (carrier > 0) {
			nodeS.next = new ListNode(carrier);
		}

		return root.next;
	}

}
