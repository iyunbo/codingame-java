package io.github.iyunbo.arrays.product.except.self;

public class Solution {
	public int[] productExceptSelf(int[] nums) {
		//init
		int[] left = new int[nums.length];
		int[] right = new int[nums.length];
		int[] result = new int[nums.length];
		// construct left and right arrays
		int prefix = 1;
		for(int i = 0; i < nums.length; i++){
			if(i > 0) prefix *= nums[i - 1];
			result[i] = prefix;
		}
		int suffix = 1;
		for(int i = nums.length - 1; i >= 0; i--){
			if(i < nums.length - 1) suffix *= nums[i + 1];
			result[i] = result[i] * suffix;
		}

		return result;
	}
}