package io.github.iyunbo.arrays.reverse.kgroup;

import io.github.iyunbo.arrays.two.numbers.ListNode;

public class Solution {
	public ListNode reverseKGroup(ListNode head, int k) {
		if (head == null || head.next == null) return head;

		ListNode nextHead = head;
		int i = 0;
		while (i < k && nextHead != null) {
			nextHead = nextHead.next;
			i++;
		}
		if (i < k) return head;

		ListNode reversedHead = reverse(head, nextHead);
		head.next = reverseKGroup(nextHead, k);
		return reversedHead;
	}

	public ListNode reverse(ListNode head, ListNode endNode) {
		ListNode resersedHead = null;
		ListNode pointer = head;
		ListNode next;
		while (pointer != endNode) {
			next = pointer.next;
			resersedHead = insert(resersedHead, pointer);
			pointer = next;
		}
		return resersedHead;
	}

	private ListNode insert(ListNode head, ListNode node) {
		node.next = head;
		return node;
	}


}

/*
k = 3
1 -> 2 -> 3 -> [4] -> 5 -> 6 -> 7

r = [null]
p    n
1 -> 2 -> 3 -> [4] -> 5 -> 6 -> 7

r = [3, 2, 1, null]
p    n
[4] -> 5 -> 6 -> 7

4 <- 1 <- 2 <- 3

3 -> 2 -> 1 -> 6 -> 5 -> 4 -> 7
 */