package io.github.iyunbo.arrays.merge.two.linkedlist;

import io.github.iyunbo.arrays.two.numbers.ListNode;

public class Solution {
	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		final ListNode head = new ListNode();
		ListNode node = head;
		while (l1 != null || l2 != null) {
			if (compare(l1, l2) <= 0) {
				node.next = new ListNode(l1.val);
				l1 = l1.next;
			} else {
				node.next = new ListNode(l2.val);
				l2 = l2.next;
			}
			node = node.next;
		}
		return head.next;
	}

	private int compare(ListNode l1, ListNode l2) {
		if (l2 == null) return -1;
		if (l1 == null) return 1;
		return Integer.compare(l1.val, l2.val);
	}
}

/*
1 -> 3 -> 4
1 -> 2 -> 4

head -> 1 -> 1 -> 2 -> 3 -> 4 -> 4

 */

