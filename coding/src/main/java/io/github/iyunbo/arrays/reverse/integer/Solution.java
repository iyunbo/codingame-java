package io.github.iyunbo.arrays.reverse.integer;

public class Solution {
	public int reverse(int x) {
		final int sign = x < 0 ? -1 : 1;
		if(x == Integer.MIN_VALUE) return 0;

		x = Math.abs(x);
		long num = 0;
		while (x != 0) {
			num = num * 10 + x % 10;
			x = x / 10;
		}

		if (num > Integer.MAX_VALUE) return 0;

		return ((int) num) * sign;
	}
}