package io.github.iyunbo.arrays.course.schedule;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PostOrderDFS {
	public boolean canFinish(int numCourses, int[][] prerequisites) {
		if (numCourses <= 1) return true;

		final Map<Integer, List<Integer>> courseDict = new HashMap<>();

		// S = O(E + V)
		// T = O(E)
		for (int[] edge : prerequisites) {
			courseDict.putIfAbsent(edge[0], new LinkedList<>());
			courseDict.get(edge[0]).add(edge[1]);
		}

		final boolean[] checked = new boolean[numCourses];
		// T = O(V + E), worst case: not cycle, every vertex is visited once, but only once (thanks to checked array),
		// every edge is visited once because of the traversal search (thanks to backtracking DFS)
		// S = O(V + V + V) = O(V)
		for (int v = 0; v < numCourses; v++) {
			final boolean[] path = new boolean[numCourses];
			if (isCyclic(v, path, courseDict, checked)) return false;
		}

		return true;
	}

	private boolean isCyclic(int currentCourse,
							 boolean[] path,
							 Map<Integer, List<Integer>> courseDict,
							 boolean[] checked) {
		if (checked[currentCourse]) return false;

		if (path[currentCourse]) return true;

		if (!courseDict.containsKey(currentCourse)) return false;

		// breadcrumb
		path[currentCourse] = true;

		// backtracking
		for (Integer nextCourse : courseDict.get(currentCourse)) {
			if (isCyclic(nextCourse, path, courseDict, checked)) return true;
		}
		// all subgraph are checked
		checked[currentCourse] = true;
		// remove breadcrumb
		path[currentCourse] = false;

		return false;
	}

}
