package io.github.iyunbo.arrays.two.sum;

import java.util.LinkedHashMap;
import java.util.Map;

class FasterSolution {
	public int[] twoSum(int[] nums, int target) {
		if (nums == null || nums.length < 2) {
			throw new IllegalArgumentException("require at least two elements in array");
		}

		final Map<Integer, Integer> dict = new LinkedHashMap<>(nums.length);
		for (int i = 0; i < nums.length; i++) {
			dict.put(nums[i], i);
		}

		for (int i = 0; i < nums.length; i++) {
			dict.remove(nums[i], i);
			if (dict.containsKey(target - nums[i])) {
				return new int[]{i, dict.get(target - nums[i])};
			}
		}

		throw new IllegalArgumentException("no solution");
	}
}