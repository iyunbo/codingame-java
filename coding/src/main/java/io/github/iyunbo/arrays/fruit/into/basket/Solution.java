package io.github.iyunbo.arrays.fruit.into.basket;

import java.util.HashMap;

public class Solution {
	/*
	input=1,2,3,2,2
	counter=[2->3, 3->1]

	left=1  : 2
	right=4 : 2

	max=4

	*/
	public int totalFruit(int[] tree) {
		int max = 0, left = 0, n = tree.length;
		final Counter counter = new Counter();

		for(int right = 0; right < n; right++){
			counter.addOne(tree[right]);
			while(counter.size() >= 3){
				counter.removeOne(tree[left]);
				left++;
			}
			max = Integer.max(max, right - left +1);
		}

		return max;
	}

	private class Counter extends HashMap<Integer, Integer> {

		public Integer get(Integer key){
			return this.getOrDefault(key, 0);
		}

		public void addOne(Integer key){
			this.put(key, this.get(key) + 1);
		}

		public void removeOne(Integer key){
			this.put(key, this.get(key) - 1);
			if(this.get(key) <= 0){
				this.remove(key);
			}
		}
	}

}