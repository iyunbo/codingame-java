package io.github.iyunbo.arrays.course.schedule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.IntStream;

class GNode {
	public Integer inDegrees = 0;
	public List<Integer> outNodes = new LinkedList<>();
}

public class DAG {
	public boolean canFinish(int numCourses, int[][] prerequisites) {
		if (numCourses <= 1) return true;

		final Map<Integer, GNode> graph = createAdjacentList(numCourses, prerequisites);

		final List<Integer> selectedCourses = new ArrayList<>();
		final Queue<Integer> startingNodes = getStartingNodes(graph);
/*
4 -> [1, 2], 0
1 -> [3, 0], 0
3 -> [2, 0], 0
0 -> [], 0
2 -> [], 0

st = []
sl = [4,1,3,2,0]
c = 3
n = 1


 */
		//T = O(V+E)
		//S = O(1)
		while (!startingNodes.isEmpty()) {
			// select the course
			final Integer course = startingNodes.remove();
			selectedCourses.add(course);
			// for every next course, remove an edge and see if new starting node exists
			for (Integer nextCourse : graph.get(course).outNodes) {
				GNode dependencyNode = graph.get(nextCourse);
				dependencyNode.inDegrees--;
				if (dependencyNode.inDegrees == 0) {
					startingNodes.add(nextCourse);
				}
			}
		}

		System.out.println("Selected courses: " + selectedCourses);
		return selectedCourses.size() == numCourses;
	}

	// T = O(V)
	// S = O(V)
	private Queue<Integer> getStartingNodes(Map<Integer, GNode> graph) {
		final Queue<Integer> startingNodes = new LinkedList<>();
		for (Integer course : graph.keySet()) {
			final GNode node = graph.get(course);
			if (node.inDegrees == 0) {
				startingNodes.add(course);
			}
		}
		return startingNodes;
	}

	// T = O(V+E)
	// S = O(V+E)
	private Map<Integer, GNode> createAdjacentList(int numCourses, int[][] prerequisites) {
		final Map<Integer, GNode> graph = new HashMap<>();

		IntStream.range(0, numCourses).forEach(c -> graph.put(c, new GNode()));

		for (int[] edge : prerequisites) {
			GNode inNode = graph.get(edge[0]);
			inNode.outNodes.add(edge[1]);
			GNode outNode = graph.get(edge[1]);
			outNode.inDegrees++;
		}
		return graph;
	}


}
