package io.github.iyunbo.arrays.merge.k.linkedlist;

import io.github.iyunbo.arrays.two.numbers.ListNode;

public class DivideConquer {
	public ListNode mergeKLists(ListNode[] lists) {
		int k = lists.length;
		if (k == 0) {
			return null;
		} else if (k == 1) {
			return lists[0];
		} else if (k == 2) {
			return merge2Lists(lists[0], lists[1]);
		} else {
			int half = k / 2;
			ListNode[] firstHalf = new ListNode[half];
			ListNode[] secondHalf = new ListNode[k - half];
			System.arraycopy(lists, 0, firstHalf, 0, half);
			System.arraycopy(lists, half, secondHalf, 0, k - half);
			return merge2Lists(mergeKLists(firstHalf), mergeKLists(secondHalf));
		}
	}

	private ListNode merge2Lists(ListNode first, ListNode second) {
		ListNode head = first;
		if (comapre(first, second) > 0) {
			head = second;
		}
		ListNode tail = null;
		while (first != null || second != null) {
			ListNode node;
			if (comapre(first, second) <= 0) {
				node = first;
				first = first.next;
			} else {
				node = second;
				second = second.next;
			}
			if (tail != null) {
				tail.next = node;
			}

			tail = node;
		}
		return head;
	}
	/*
	1 -> 2 -> 3 -> 4 -> null
	2 -> 3 -> null
	first = null
	second = null
	head = 1 -> 2 -> 2 -> 3 -> 3 -> 4 -> null
	node = 4
	tail = 4
	 */

	private int comapre(ListNode first, ListNode second) {
		if (second == null) return -1;
		if (first == null) return 1;
		return Integer.compare(first.val, second.val);
	}

}
