package io.github.iyunbo.arrays.sparse.arrays;

import java.util.HashMap;
import java.util.Map;

/*
length of queries and strings ?
what if strings is empty: return 0 for each word
what if queries is empty: return empty array
 */
public class Solution {
	public int[] matchingStrings(String[] strings, String[] queries) {
		// construct the counter map
		final Map<String, Integer> counter = new HashMap<>();
		for (final String word : strings) {
			int count = counter.getOrDefault(word, 0);
			counter.put(word, count + 1);
		}
		// find the word count for each query string
		final int[] result = new int[queries.length];
		for (int i = 0; i < queries.length; i++) {
			result[i] = counter.getOrDefault(queries[i], 0);
		}
		return result;
	}
}
