package io.github.iyunbo.arrays.fivestars;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/*
max number of product ?
threashold <= 100 ?
invalid inputs ? (need check ?)
 */
public class Solution {

	public int fiveStarReviews(List<List<Integer>> productRatings, int ratingsThreshold) {
		// initialization
		final Queue<List<Integer>> heap = new PriorityQueue<>(Comparator.comparing(l -> l.get(0) * 1.0 / l.get(1) - (l.get(0) + 1) * 1.0 / (l.get(1) + 1)));
		double rating = 0D;
		int extraFiveStars = 0;
		// T = O(N), S=O(N)
		// calculate current rating and enqueue each product rating
		for (List<Integer> productRating : productRatings) {
			rating += calculateRating(productRating);
			heap.add(productRating);
		}
		// T = O(M*longN) = O(N*logN), worst case: M=N*(200-1), threshold=99
		// add five stars until reach the threashold
		while (rating < ratingsThreshold * productRatings.size()) {
			List<Integer> topProductRating = heap.poll();
			rating -= calculateRating(topProductRating);
			List<Integer> newProductRating = List.of(topProductRating.get(0) + 1, topProductRating.get(1) + 1);
			rating += calculateRating(newProductRating);
			extraFiveStars++;
			heap.add(newProductRating);
		}
		return extraFiveStars;
	}

	private double calculateRating(List<Integer> rating) {
		return rating.get(0) * 100.0 / rating.get(1);
	}
}
/*
productRatings=[[4,4],[3,4],[3,6]], threshold=77*3=231
heap.peek=[3,4]
rating=(1+0.5+0.5)*100=225

 */
