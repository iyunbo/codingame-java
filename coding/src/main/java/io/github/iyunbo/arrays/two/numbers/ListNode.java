package io.github.iyunbo.arrays.two.numbers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ListNode {
	public int val;
	public ListNode next;

	public ListNode() {
	}

	public ListNode(int val) {
		this.val = val;
	}

	public ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}

	public List<Integer> toList(){
		return toList(true);
	}

	public List<Integer> toList(boolean reverse) {
		ListNode next = this.next;
		List<Integer> list = new ArrayList<>();
		list.add(this.val);
		while (next != null) {
			list.add(next.val);
			next = next.next;
		}
		if(reverse) Collections.reverse(list);
		return list;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ListNode listNode = (ListNode) o;
		return val == listNode.val &&
				Objects.equals(next, listNode.next);
	}

	@Override
	public int hashCode() {
		return Objects.hash(val, next);
	}

	public static ListNode of(int... numbers) {
		ListNode root = new ListNode();
		ListNode node = root;
		for (int number : numbers) {
			node.next = new ListNode(number);
			node = node.next;
		}
		return root.next;
	}
}

