package io.github.iyunbo.arrays.sum.closest;

import java.util.Arrays;

public class Solution {
	public int threeSumClosest(int[] nums, int target) {
		return twoPointers(nums, target);
	}

	public int twoPointers(int[] ints, int target) {
		int closestSum = target - Integer.MAX_VALUE;
		// T = O(n * log(n))
		Arrays.sort(ints);

		// T = O(n * n)
		for (int pivot = 0; pivot < ints.length; pivot++) {
			if (pivot == 0 || ints[pivot] != ints[pivot - 1]) {
				closestSum = twoSum(ints, closestSum, pivot, target);
			}
		}

		return closestSum;
	}

	private int twoSum(int[] ints, int closestSum, int pivot, int target) {
		int low = pivot + 1;
		int high = ints.length - 1;
		while (low < high) {
			int sum = ints[low] + ints[high] + ints[pivot];
			int distance = Math.abs(sum - target);
			if (distance < Math.abs(closestSum - target)) {
				closestSum = sum;
			}
			if (sum <= target) {
				low++;
				while (ints[low] == ints[low - 1] && low < high) low++;
			} else {
				high--;
			}
		}
		return closestSum;
	}
}
