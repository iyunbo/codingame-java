package io.github.iyunbo.arrays.atoi;

public class Solution {

	public int myAtoi(String s) {
		char[] chars = s.toCharArray();

		int left = calculateLeftPosition(chars);

		if (left >= chars.length) return 0;

		int sign = calculateSign(chars, left);
		if (sign == 0) {
			sign = 1;
		} else {
			left++;
		}

		final int right = calculateRightPosition(chars, left);

		final double result = atoD(chars, left, right);

		return toI(sign, result);
	}

	private int calculateSign(char[] chars, int end) {
		int sign = 0;
		if (chars[end] == '-') {
			sign = -1;
		} else if (chars[end] == '+') {
			sign = 1;
		}
		return sign;
	}

	private int toI(int sign, double result) {
		if (result > Integer.MAX_VALUE) {
			if (sign > 0) {
				return Integer.MAX_VALUE;
			} else {
				return Integer.MIN_VALUE;
			}
		}
		return Double.valueOf(result).intValue() * sign;
	}

	private double atoD(char[] chars, int left, int right) {
		double result = 0;
		int exponent = 0;
		for (int i = right; i >= left; i--) {
			result += (chars[i] - '0') * Math.pow(10, exponent);
			exponent++;
		}
		return result;
	}

	private int calculateRightPosition(char[] chars, int end) {
		int start = chars.length - 1;
		for (int i = chars.length - 1; i >= end; i--) {
			if (!Character.isDigit(chars[i])) {
				start = i - 1;
			}
		}
		return start;
	}

	private int calculateLeftPosition(char[] chars) {
		int end = 0;
		while (end < chars.length) {
			if (chars[end] == ' ') end++;
			else break;
		}
		return end;
	}
}
