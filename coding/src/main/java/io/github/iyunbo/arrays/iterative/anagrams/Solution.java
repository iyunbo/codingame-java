package io.github.iyunbo.arrays.iterative.anagrams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
["eat", "tea", "tan", "ate", "nat", "bat"]
=> [["eat","tea","ate"],["tan","nat"],["bat"]]
*/
class Solution {
	public List<List<String>> groupAnagrams(String[] strs) {
		// init
		final Map<String, List<String>> groups = new HashMap<>();
		// loop over every string
		// T = O(NlogN)
		for (String str : strs) {
			char[] chars = str.toCharArray();
			Arrays.sort(chars);
			String key = String.valueOf(chars);
			groups.putIfAbsent(key, new ArrayList<>());
			groups.get(key).add(str);
		}
		// result list
		return new ArrayList<>(groups.values());
	}
}
