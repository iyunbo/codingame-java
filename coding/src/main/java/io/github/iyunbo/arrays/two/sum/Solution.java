package io.github.iyunbo.arrays.two.sum;

class Solution {
	public int[] twoSum(int[] nums, int target) {
		if (nums == null || nums.length < 2) {
			throw new IllegalArgumentException("require at least two elements in array");
		}

		for (int i = 0; i < nums.length; i++) {
			int secondIndex = findNum(nums, i + 1, target - nums[i]);
			if (secondIndex >= 0) {
				return new int[]{i, secondIndex};
			}
		}

		throw new IllegalArgumentException("no solution");
	}

	private int findNum(int[] nums, int start, int target) {
		for (int i = start; i < nums.length; i++) {
			if (nums[i] == target) {
				return i;
			}
		}
		return -1;
	}
}