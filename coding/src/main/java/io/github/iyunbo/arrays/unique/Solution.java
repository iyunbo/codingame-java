package io.github.iyunbo.arrays.unique;

import java.util.HashMap;
import java.util.Map;

public class Solution {
	//T = O(N), S=O(1)
	public int firstUniqChar(String s) {
		final Map<Character, Integer> counter = new HashMap<>();
		for (char ch : s.toCharArray()) {
			int count = counter.getOrDefault(ch, 0);
			counter.put(ch, count + 1);
		}
		for (int i = 0; i < s.length(); i++) {
			if (counter.get(s.charAt(i)) == 1) return i;
		}
		return -1;
	}
}
