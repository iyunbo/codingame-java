package io.github.iyunbo.arrays.merge.k.linkedlist;

import io.github.iyunbo.arrays.two.numbers.ListNode;

public class Solution {
	public ListNode mergeKLists(ListNode[] lists) {
		int k = lists.length;
		ListNode tail = null;
		final ListNode[] pointers = new ListNode[k];
		int i = 0;
		for(ListNode node: lists) pointers[i++] = node;

		ListNode min = extractMin(pointers);
		final ListNode head = min;

		while (min != null) {
			tail = append(tail, min);
			min = extractMin(pointers);
		}

		return head;
	}
/*
1 -> 2 -> 3 -> 4 -> 5
1 -> 1 -> 3 -> 6
2 -> 3 -> 4 -> 4

k = 3
t = l0(3)
ptrs = [l0(4), l1(3), l2(3)]
min = l0(3), head = l0(1) -> l1(1) -> l1(1) -> l0(2) -> l2(2) -> l0(3)

 */

	private ListNode append(ListNode tail, ListNode min) {
		if (tail != null) {
			tail.next = min;
		}
		return min;
	}

	private ListNode extractMin(ListNode[] pointers) {
		ListNode min = null;
		int minValue = Integer.MAX_VALUE;
		int i = 0;
		int minIndex = -1;
		for (ListNode pointer : pointers) {
			if (pointer != null) {
				if (pointer.val <= minValue) {
					minValue = pointer.val;
					min = pointer;
					minIndex = i;
				}
			}
			i++;
		}
		if (minIndex >=0) {
			pointers[minIndex] = pointers[minIndex].next;
		}
		return min;
	}
	/*
	1 -> null
	2 -> 3 -> null
	null

	minVal = 1, minIndex = 0, min = pointers[0]
	null
	2 -> 3 -> null
	null

	 */


}
