package io.github.iyunbo.arrays.itor;

public class Solution {
	// T = O(n), S = O(n)
	public String intToRoman(int num) {
		final StringBuilder result = new StringBuilder();
		int value = num;
		value = extractNumber(result, value, "M", 1000);
		value = extractNumber(result, value, "CM", 900);
		value = extractNumber(result, value, "D", 500);
		value = extractNumber(result, value, "CD", 400);
		value = extractNumber(result, value, "C", 100);
		value = extractNumber(result, value, "XC", 90);
		value = extractNumber(result, value, "L", 50);
		value = extractNumber(result, value, "XL", 40);
		value = extractNumber(result, value, "X", 10);
		value = extractNumber(result, value, "IX", 9);
		value = extractNumber(result, value, "V", 5);
		value = extractNumber(result, value, "IV", 4);
		extractNumber(result, value, "I", 1);
		return result.toString();
	}

	private int extractNumber(StringBuilder romain, int value, String romainNumber, int number) {
		while (value >= number) {
			romain.append(romainNumber);
			value -= number;
		}
		return value;
	}
}
