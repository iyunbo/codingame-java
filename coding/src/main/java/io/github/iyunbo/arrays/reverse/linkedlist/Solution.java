package io.github.iyunbo.arrays.reverse.linkedlist;


import io.github.iyunbo.arrays.two.numbers.ListNode;

public class Solution {

	public ListNode reverseList(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode reversedRemaining = reverseList(head.next);
		ListNode node = reversedRemaining;

		if (node != null) {
			while (node.next != null) node = node.next;
			node.next = head;
		}

		head.next = null;

		return reversedRemaining;
	}

/*

h
1 -> 2 -> 3 -> 4 -> 5 -> null

h    r
1    5 -> 4 -> 3 -> 2 -> null

r                   h
5 -> 4 -> 3 -> 2 -> 1 -> null

h    r
1 -> 2 -> null

r    h
2 -> 1 -> null

h  r
1  null
1 -> null





 */

}
