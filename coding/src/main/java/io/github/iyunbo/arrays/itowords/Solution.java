package io.github.iyunbo.arrays.itowords;

import java.util.HashMap;
import java.util.Map;

public /*
1 - 2**31 ~ 2 * 10**9 ~ 2 Billion
1234567891 -> 1, 234, 567, 891
234 -> 2, 34 (<999)
34 -> 30 + 4 (20<n<99)
19 -> 19 (0-20)
0 -> zero or ""
*/
class Solution {

	private final Map<Integer, String> numToWord = new HashMap<>();

	public Solution(){
		numToWord.put(0, "Zero");
		numToWord.put(1, "One");
		numToWord.put(2, "Two");
		numToWord.put(3, "Three");
		numToWord.put(4, "Four");
		numToWord.put(5, "Five");
		numToWord.put(6, "Six");
		numToWord.put(7, "Seven");
		numToWord.put(8, "Eight");
		numToWord.put(9, "Nine");
		numToWord.put(10, "Ten");
		numToWord.put(11, "Eleven");
		numToWord.put(12, "Twelve");
		numToWord.put(13, "Thirteen");
		numToWord.put(14, "Fourteen");
		numToWord.put(15, "Fifteen");
		numToWord.put(16, "Sixteen");
		numToWord.put(17, "Seventeen");
		numToWord.put(18, "Eighteen");
		numToWord.put(19, "Nineteen");
		numToWord.put(20, "Twenty");
		numToWord.put(30, "Thirty");
		numToWord.put(40, "Forty");
		numToWord.put(50, "Fifty");
		numToWord.put(60, "Sixty");
		numToWord.put(70, "Seventy");
		numToWord.put(80, "Eighty");
		numToWord.put(90, "Ninety");
		numToWord.put(100, "Hundred");
		numToWord.put(1_000, "Thousand");
		numToWord.put(1_000_000, "Million");
		numToWord.put(1_000_000_000, "Billion");
	}

	public String numberToWords(int num) {
		if(num == 0) return numToWord.get(0);

		int part;
		int remaining = num;
		int factor = 1;
		StringBuilder result = new StringBuilder();

		while(remaining > 0){
			part = remaining % 1000;
			if(factor > 1 && part > 0) {
				// add unit
				result.insert(0, numToWord.get(factor));
				result.insert(0, " ");
			}
			remaining = (remaining - part) / 1000;
			String word = convert3Digits(part);
			if(part != 0){
				result.insert(0, word);
				result.insert(0, " ");
			}
			if(remaining == 0) result.deleteCharAt(0);
			factor = factor * 1000;
		}

		return result.toString();
	}

	private String convert3Digits(int n){
		if(n > 999) throw new IllegalArgumentException("invalid input: " + n);
		if(n <= 99) return convert2Digits(n);
		int twoDigits = n % 100;
		String result = convert2Digits(twoDigits);
		int oneDigit = (n - twoDigits) / 100;
		if(twoDigits == 0) return numToWord.get(oneDigit) + " Hundred";
		return numToWord.get(oneDigit) + " Hundred " + result;
	}

	private String convert2Digits(int n){
		if(n > 99) throw new IllegalArgumentException("invalid input: " + n);
		if(n == 0) return "";
		if(n <= 20) return numToWord.get(n);
		int lastDigit = n % 10;
		if(lastDigit == 0) return numToWord.get(n - (lastDigit));
		return numToWord.get(n - (lastDigit)) + " " + numToWord.get(lastDigit);
	}

}