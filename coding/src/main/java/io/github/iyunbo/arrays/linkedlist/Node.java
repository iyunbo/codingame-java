package io.github.iyunbo.arrays.linkedlist;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Node {
	int val;
	Node next;
	Node random;

	public Node(int val) {
		this.val = val;
		this.next = null;
		this.random = null;
	}

	public static Node of(int... numbers) {
		Node tail = null;
		Node head = null;
		for (int number : numbers) {
			Node node = new Node(number);
			node.random = node;
			if (head == null) head = node;
			if (tail != null) {
				tail.next = node;
			}
			tail = node;
		}
		return head;
	}

	public List<Node> toList() {
		final List<Node> list = new ArrayList<>();
		Node node = this;
		while (node != null) {
			list.add(node);
			node = node.next;
		}
		return list;
	}

	public List<Integer> toInts() {
		return toList()
				.stream()
				.map(n -> n.val)
				.collect(Collectors.toList());
	}
}
