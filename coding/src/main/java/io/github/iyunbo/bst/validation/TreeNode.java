package io.github.iyunbo.bst.validation;

public class TreeNode {
	public int val;
	public TreeNode left;
	public TreeNode right;

	public TreeNode() {
	}

	public TreeNode(int val) {
		this.val = val;
	}

	public TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}

	public static TreeNode of(Integer... array) {
		return buildTree(0, array);
	}

	private static TreeNode buildTree(int rootIndex, Integer[] array) {
		if (rootIndex >= array.length || array[rootIndex] == null) return null;
		return new TreeNode(array[rootIndex], buildTree(rootIndex * 2 + 1, array), buildTree(rootIndex * 2 + 2, array));
	}

	@Override
	public String toString() {
		return "{" + val + "}";
	}

	public boolean equals(Object other) {
		if (!(other instanceof TreeNode)) return false;
		TreeNode autre = (TreeNode) other;
		if (this.val == autre.val) {
			if (this.left == autre.left && this.right == autre.right) {
				return true;
			} else if ((this.left == null && autre.left != null) || (this.right == null && autre.right != null)) {
				return false;
			} else {
				return this.left.equals(autre.left) && this.right.equals(autre.right);
			}
		}
		return false;
	}
}
