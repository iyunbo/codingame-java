package io.github.iyunbo.bst.lowest.common;

import io.github.iyunbo.bst.validation.TreeNode;

public class Recursion {

	private TreeNode result = null;

	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		visitTree(root, p, q);
		return this.result;
	}
/*
				3
			5       1
		6     2   0    8
	  n   n 7   4
p=5, q=4
c=5
l=0,r=1,m=1
ret=5
 */
	//T = O(N), worst case: visits every node
	//S = O(N), worst case: tree is a list
	private boolean visitTree(TreeNode currentNode, TreeNode p, TreeNode q) {
		if (currentNode == null) return false;
		if (this.result != null) return false;

		final int left = visitTree(currentNode.left, p, q) ? 1 : 0;
		final int right = visitTree(currentNode.right, p, q) ? 1 : 0;

		final int middle = (currentNode == p || currentNode == q) ? 1 : 0;

		if (left + right + middle >= 2) {
			this.result = currentNode;
		}

		return left + right + middle > 0;
	}
}
