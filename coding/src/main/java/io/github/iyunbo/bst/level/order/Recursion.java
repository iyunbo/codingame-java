package io.github.iyunbo.bst.level.order;

import java.util.ArrayList;
import java.util.List;

import io.github.iyunbo.bst.validation.TreeNode;

public class Recursion {
	public List<List<Integer>> levelOrder(TreeNode root) {
		final List<List<Integer>> result = new ArrayList<>();
		insertLevel(result, root, 0);
		return result;
	}

	// T = O(n), S = O(n)
	private void insertLevel(List<List<Integer>> result, TreeNode root, int level) {
		if (root == null) return;
		int currentLevel = result.size();
		if (currentLevel == level) result.add(new ArrayList<>());
		final List<Integer> currentList = result.get(level);
		currentList.add(root.val);
		insertLevel(result, root.left, level + 1);
		insertLevel(result, root.right, level + 1);
	}

}
