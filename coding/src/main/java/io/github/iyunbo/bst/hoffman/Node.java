package io.github.iyunbo.bst.hoffman;

public class Node {
	private final char data;
	private int frequency = 0;
	private Node left;
	private Node right;

	public char getData() {
		return data;
	}

	public Node getLeft() {
		return left;
	}


	public Node getRight() {
		return right;
	}

	public Node(Node left, Node right) {
		this.data = ' ';
		this.left = left;
		this.right = right;
		if (this.left != null) {
			this.frequency += this.left.frequency;
		}
		if (this.right != null) {
			this.frequency += this.right.frequency;
		}
	}

	public Node(int frequency, char data) {
		this.frequency = frequency;
		this.data = data;
	}

	public int getFrequency() {
		return frequency;
	}

}
