package io.github.iyunbo.bst.symmetry;

import io.github.iyunbo.bst.validation.TreeNode;

public class Solution {
	public boolean isSymmetric(TreeNode root) {
		if (root == null) return true;
		return areSymmetricTrees(root.left, root.right);
	}

	private boolean areSymmetricTrees(TreeNode left, TreeNode right) {
		if (left == null && right == null) return true;
		if (left == null && right != null || right == null && left != null)
			return false;
		if (left.val != right.val) return false;
		return areSymmetricTrees(left.left, right.right) && areSymmetricTrees(left.right, right.left);
	}
}
