package io.github.iyunbo.bst.level.order;

import java.util.ArrayList;
import java.util.List;

import io.github.iyunbo.bst.validation.TreeNode;

public class Solution {
	public List<List<Integer>> levelOrder(TreeNode root) {
		final List<List<Integer>> result = new ArrayList<>();
		final List<TreeNode> currentLevel = new ArrayList<>();
		if (root != null) currentLevel.add(root);
		final List<TreeNode> nextLevel = new ArrayList<>();

		while (!currentLevel.isEmpty()) {
			List<Integer> currentValues = new ArrayList<>();
			for (TreeNode node : currentLevel) {
				if (node != null) {
					currentValues.add(node.val);
				}
			}
			result.add(currentValues);

			nextLevel.clear();
			for (TreeNode node : currentLevel) {
				if (node.left != null) nextLevel.add(node.left);
				if (node.right != null) nextLevel.add(node.right);
			}
			currentLevel.clear();
			currentLevel.addAll(nextLevel);
		}
		return result;
	}

}
