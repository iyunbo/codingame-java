package io.github.iyunbo.bst.lowest.common;

import java.util.Deque;
import java.util.LinkedList;

import io.github.iyunbo.bst.validation.TreeNode;

public class Solution {
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		Deque<TreeNode> pathP = new LinkedList<>();
		pathP.add(root);
		// T = O(2N)=O(N), worst case: visit every node
		// S = O(2N)=O(N), worst case: tree is a list, p=q=lowest leaf
		pathP = findPath(pathP, p);
		Deque<TreeNode> pathQ = new LinkedList<>();
		pathQ.add(root);
		pathQ = findPath(pathQ, q);
		TreeNode lastCommon = root;
		// T = O(N), worst case: p = q = lowest leaf and tree is a list
		// S = (1)
		while (!pathP.isEmpty() && !pathQ.isEmpty()) {
			TreeNode currentP = pathP.pop();
			TreeNode currentQ = pathQ.pop();
			if (currentP != currentQ) {
				return lastCommon;
			} else {
				lastCommon = currentP;
			}
		}
		return lastCommon;
	}

	private Deque<TreeNode> findPath(Deque<TreeNode> currentPath, TreeNode node) {
		if (currentPath.isEmpty()) return currentPath;

		final TreeNode currentRoot = currentPath.getLast();
		if (currentRoot == null || currentRoot == node) return currentPath;

		if (currentRoot.left == node) {
			currentPath.add(currentRoot.left);
			return currentPath;
		}

		if (currentRoot.right == node) {
			currentPath.add(currentRoot.right);
			return currentPath;
		}

		if (currentRoot.left != null) {
			currentPath.add(currentRoot.left);
			Deque<TreeNode> leftPath = findPath(currentPath, node);
			if (leftPath.getLast() == node) {
				return leftPath;
			} else {
				currentPath.removeLast();
			}
		}

		if (currentRoot.right != null) {
			currentPath.add(currentRoot.right);
			Deque<TreeNode> rightPath = findPath(currentPath, node);
			if (rightPath.getLast() == node) {
				return rightPath;
			} else {
				currentPath.removeLast();
			}
		}
		return currentPath;
	}
}
