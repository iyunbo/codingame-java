package io.github.iyunbo.bst.hoffman;

public class Decode {

	private StringBuilder builder;

	public String decode(String s, Node root) {
		this.builder = new StringBuilder();
		int index = 0;
		while (index < s.length()) {
			int newIndex = decode(s, index, root);
			if (newIndex == index)
				throw new IllegalArgumentException("invalid Hoffman tree!");
			index = newIndex;
		}
		return this.builder.toString();
	}

	private int decode(String s, int index, Node node) {
		if (node == null)
			throw new IllegalArgumentException("invalid Hoffman tree!");
		if (index > s.length()) return index;
		if (node.getLeft() == null && node.getRight() == null) {
			builder.append(node.getData());
			return index;
		} else {
			char ch = s.charAt(index);
			if (ch == '0') {
				return decode(s, index + 1, node.getLeft());
			} else {
				return decode(s, index + 1, node.getRight());
			}
		}
	}
}
