package io.github.iyunbo.bst.hoffman;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class Encode {

	public Node buildTree(final String input) {
		// construct the counter of chars
		final Map<Character, Integer> counter = new HashMap<>();
		for (char ch : input.toCharArray()) {
			counter.put(ch, counter.getOrDefault(ch, 0) + 1);
		}
		return build(counter);
	}

	private Node build(Map<Character, Integer> counter) {
		// initially we have forest of leaves
		// T = O(N), S = O(N)
		final Queue<Node> heap = new PriorityQueue<>(Comparator.comparing(Node::getFrequency));
		for (Character ch : counter.keySet()) {
			heap.offer(new Node(counter.get(ch), ch));
		}
		// loop until only one tree left
		// T = O(N*logN)
		while (heap.size() > 1) {
			Node leastFrequentOne = heap.poll();
			Node leastFrequentOther = heap.poll();
			heap.offer(new Node(leastFrequentOne, leastFrequentOther));
		}
		return heap.poll();
	}

	//T = O(N*logN + M), S = O(N)
	public String encode(String input, Node tree) {
		final Map<Character, String> dict = new HashMap<>();
		// T = O(N*logN)
		traverse(dict, "", tree);
		// T = O(M)
		final StringBuilder encoded = new StringBuilder();
		for (char ch : input.toCharArray()) {
			encoded.append(dict.get(ch));
		}
		return encoded.toString();
	}

	private void traverse(Map<Character, String> dict, String prefix, Node tree) {
		if (tree == null) return;
		if (tree.getLeft() == null && tree.getRight() == null) {
			dict.put(tree.getData(), prefix);
		} else {
			if (tree.getLeft() != null) {
				traverse(dict, prefix + "0", tree.getLeft());
			}
			if (tree.getLeft() != null) {
				traverse(dict, prefix + "1", tree.getRight());
			}
		}
	}
}
