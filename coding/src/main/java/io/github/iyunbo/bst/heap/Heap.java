package io.github.iyunbo.bst.heap;

public class Heap {

	public void heapifyMin(int... array) {
		if (array.length == 0) return;
		// T = O(N)
		for (int i = array.length / 2; i >= 0; i--) {
			heapify(true, array, i);
		}
	}

	public void heapifyMax(int... array) {
		if (array.length == 0) return;
		// T = O(N)
		for (int i = array.length / 2; i >= 0; i--) {
			heapify(false, array, i);
		}
	}

	public int extractMin(int... heap) {
		final int min = heap[0];
		heap[0] = Integer.MAX_VALUE;
		swap(heap, 0, heap.length - 1);
		heapify(true, heap, 0);
		return min;
	}

	public int extractMax(int... heap) {
		final int min = heap[0];
		heap[0] = Integer.MIN_VALUE;
		swap(heap, 0, heap.length - 1);
		heapify(false, heap, 0);
		return min;
	}

	// T = (log(N))
	// S = (1)
	private void heapify(boolean minimum, int[] heap, int index) {

		final int leftChild = index * 2 + 1;
		final int rightChild = index * 2 + 2;
		final int length = heap.length;

		int best = index;

		if (leftChild < length && (heap[leftChild] < heap[best] && minimum || heap[leftChild] > heap[best] && !minimum)) {
			best = leftChild;
		}

		if (rightChild < length && (heap[rightChild] < heap[best] && minimum || heap[rightChild] > heap[best] && !minimum)) {
			best = rightChild;
		}

		if (best != index) {
			swap(heap, index, best);
			heapify(minimum, heap, best);
		}

	}

	private void swap(int[] array, int a, int b) {
		final int tmp = array[a];
		array[a] = array[b];
		array[b] = tmp;
	}
}
