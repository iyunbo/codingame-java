package io.github.iyunbo.bst.heap.klargest;

import io.github.iyunbo.bst.heap.Heap;

public class Solution {
	public int findKthLargest(int[] nums, int k) {
		Heap heap = new Heap();
		// T = O(N)
		// S = O(1)
		heap.heapifyMax(nums);
		int result = -1;
		// T = O(K * log(N))
		for (int i = 0; i < k; i++)
			result = heap.extractMax(nums);
		return result;
	}
}
