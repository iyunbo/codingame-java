package io.github.iyunbo.bst.validation;

public class Solution {
	public boolean isValidBST(TreeNode root) {
		if (root == null) return true;
		if (root.left == null && root.right == null) return true;
		if (isValidBST(root.left) && isValidBST(root.right)) {
			Integer leftBiggest = findBiggest(root.left);
			Integer rightSmallest = findSmallest(root.right);
			boolean leftValid = true;
			if (leftBiggest != null) {
				leftValid = leftBiggest < root.val;
			}
			boolean rightValid = true;
			if (rightSmallest != null) {
				rightValid = rightSmallest > root.val;
			}
			return leftValid && rightValid;
		}
		return false;
	}

	private Integer findSmallest(TreeNode bst) {
		if (bst == null) return null;
		if (bst.left == null) return bst.val;
		return findBiggest(bst.left);
	}

	private Integer findBiggest(TreeNode bst) {
		if (bst == null) return null;
		if (bst.right == null) return bst.val;
		return findBiggest(bst.right);
	}
	/*
	simple case:
	1 => true
	limit case:
	null => true
	base case 1 => true
	     4
	3         10
2	       5      12
             6  11   15
   base case 2 => false
	     4
	3         10
2	       6      12
         1   8  11   15
	 */
}
