package io.github.iyunbo.bst.level.zigzag;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.github.iyunbo.bst.validation.TreeNode;

public class Solution {
	public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
		final List<List<Integer>> result = new ArrayList<>();
		final List<TreeNode> currentLevel = new ArrayList<>();
		if (root != null) currentLevel.add(root);
		final List<TreeNode> nextLevel = new ArrayList<>();
		boolean leftFirst = true;
		while (!currentLevel.isEmpty()) {
			List<Integer> currentValues = new ArrayList<>();
			for (TreeNode node : currentLevel) {
				if (node != null) {
					currentValues.add(node.val);
				}
			}
			if(!leftFirst) Collections.reverse(currentValues);
			result.add(currentValues);

			nextLevel.clear();
			for (TreeNode node : currentLevel) {
				if (node.left != null) nextLevel.add(node.left);
				if (node.right != null) nextLevel.add(node.right);
			}
			currentLevel.clear();
			currentLevel.addAll(nextLevel);
			leftFirst = !leftFirst;
		}
		return result;
	}

/*
				1
		   2		    3
		4	  5      6     7
	  8   9 10  11 12 13 14 15

6, 7, 4, 5

r=2
result=[*[1], [3, 2]]
left
l=1

 */
}
