package io.github.iyunbo.connected.largest.item.association;


import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class UnionFind {
	// T = O(E*logV + V)
	// S = O(V)
	public List<String> largestItemAssociation(List<PairString> itemAssociation) {

		// S = O(V)
		final Map<String, String> parent = new HashMap<>();

		// T = O(E*logV)
		for (PairString pair : itemAssociation) {
			unionFind(parent, pair);
		}

		// T = O(V)
		// S = O(V)
		final Queue<TreeSet<String>> components = getComponents(parent);

		return List.copyOf(components.peek());
	}

	private Queue<TreeSet<String>> getComponents(Map<String, String> parent) {
		final Queue<TreeSet<String>> q = new PriorityQueue<>(
				(g1, g2) -> {
					if (g1.size() == g2.size())
						return g1.first().compareTo(g2.first());
					return Integer.compare(g2.size(), g1.size());
				}
		);

		final Map<String, TreeSet<String>> components = new HashMap<>();
		final Set<String> vertices = new HashSet<>(parent.keySet());
		vertices.addAll(parent.values());
		final Set<String> seen = new HashSet<>();
		for (String child : vertices) {
			if (!seen.contains(child)) {
				String root = find(parent, child);
				TreeSet<String> component = components.getOrDefault(root, new TreeSet<>());
				component.add(child);
				components.put(root, component);
				seen.addAll(component);
			}
		}
		q.addAll(components.values());

		return q;
	}

	private void unionFind(Map<String, String> parent, PairString pair) {
		final String first = find(parent, pair.first);
		final String second = find(parent, pair.second);
		if (!first.equals(second)) {
			parent.put(first, second);
		}
	}

	private String find(Map<String, String> parent, String vertex) {
		if (!parent.containsKey(vertex)) return vertex;
		return find(parent, parent.get(vertex));
	}


}

