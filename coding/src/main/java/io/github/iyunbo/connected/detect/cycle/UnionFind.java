package io.github.iyunbo.connected.detect.cycle;

import java.util.Arrays;

public class UnionFind {
	public boolean hasCircle(int[][] pairs, int n) {
		final int[] parents = new int[n];
		Arrays.fill(parents, -1);
		for (int[] pair : pairs) {
			int first = find(parents, pair[0]);
			int second = find(parents, pair[1]);
			if (first == second) {
				return true;
			}
			union(parents, first, second);
		}
		return false;
	}

	private int find(int[] parents, int i) {
		if (parents[i] == -1) return i;
		return find(parents, parents[i]);
	}

	private void union(int[] parents, int one, int other) {
		int first = find(parents, one);
		int second = find(parents, other);
		parents[first] = second;
	}
}
