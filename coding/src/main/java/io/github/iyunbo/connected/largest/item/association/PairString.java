package io.github.iyunbo.connected.largest.item.association;

public class PairString {
	String first;
	String second;

	public PairString(String first, String second) {
		this.first = first;
		this.second = second;
	}
}
