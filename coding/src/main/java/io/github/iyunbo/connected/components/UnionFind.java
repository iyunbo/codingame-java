package io.github.iyunbo.connected.components;

import java.util.Arrays;

public class UnionFind {
	// T = O(V+E*logV)
	public int countComponents(int n, int[][] edges) {
		final int[] parent = new int[n];
		// T = O(V)
		Arrays.fill(parent, -1);

		// T = O(E*logV), see: https://en.wikipedia.org/wiki/Proof_of_O(log*n)_time_complexity_of_union%E2%80%93find
		for (int[] edge : edges) {
			unionFind(parent, edge);
		}

		// T = O(V)
		int count = 0;
		for (int p : parent) {
			if (p == -1) count++;
		}

		return count;
	}

	private void unionFind(int[] parent, int[] edge) {
		int first = find(parent, edge[0]);
		int second = find(parent, edge[1]);
		// union
		if (first != second) {
			parent[first] = second;
		}
	}

	private int find(int[] parent, int v) {
		if (parent[v] == -1) return v;
		return find(parent, parent[v]);
	}

}
