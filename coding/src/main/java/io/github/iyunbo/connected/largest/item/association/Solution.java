package io.github.iyunbo.connected.largest.item.association;


import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class Solution {
	public List<String> largestItemAssociation(List<PairString> itemAssociation) {
		if (itemAssociation.isEmpty()) return List.of();
		final Map<String, TreeSet<String>> groups = new HashMap<>();
		//T = O(N*N), S = O(N*N), worst case: all items are in the same group
		for (PairString pair : itemAssociation) {
			addPair(pair, groups);
		}
		final Set<String> checked = new HashSet<>();
		final Queue<TreeSet<String>> candidates = new PriorityQueue<>((l1, l2) -> {
			if (l1.size() == l2.size()) {
				return l1.first().compareTo(l2.first());
			} else {
				return Integer.compare(l2.size(), l1.size());
			}
		});
		// T = O(N), S = O(N)
		for (String item : groups.keySet()) {
			if (!checked.contains(item)) {
				candidates.add(groups.get(item));
				checked.addAll(groups.get(item));
			}
		}
		return List.copyOf(candidates.peek());
	}

	private void addPair(PairString pair, Map<String, TreeSet<String>> groups) {
		TreeSet<String> group = groups.getOrDefault(pair.first, new TreeSet<>());
		group.add(pair.first);
		group.add(pair.second);
		group.addAll(groups.getOrDefault(pair.second, new TreeSet<>()));
		groups.put(pair.first, group);
		groups.put(pair.second, group);
	}
}

