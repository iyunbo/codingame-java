package io.github.iyunbo.connected.detect.cycle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DFS {
	public boolean hasCircle(int[][] pairs, int n) {
		final Map<Integer, List<Integer>> adjList = new HashMap<>();
		for (int[] edge : pairs) {
			List<Integer> list = adjList.getOrDefault(edge[0], new ArrayList<>());
			list.add(edge[1]);
			adjList.put(edge[0], list);
		}

		final boolean[] seen = new boolean[n];
		for (Integer v : adjList.keySet()) {
			final boolean[] recStack = new boolean[n];
			if (checkCircle(recStack, v, adjList, seen)) return true;
		}

		return false;

	}

	private boolean checkCircle(boolean[] recStack, int v, Map<Integer, List<Integer>> adjList, boolean[] seen) {
		if (recStack[v]) return true;
		if (seen[v]) return false;
		recStack[v] = true;
		seen[v] = true;
		for (int next : adjList.getOrDefault(v, List.of())) {
			if (checkCircle(recStack, next, adjList, seen)) return true;
		}

		return false;
	}
}
