package io.github.iyunbo.sliding.window.longest.k.distinct;

import java.util.HashMap;
import java.util.Map;

/**
 * Problem Statement #
 * Given a string, find the length of the longest substring in it with no more than K distinct characters.
 */
public class Solution {
	/*
	 * str=araaci, k=2
	 * maxLen = 4
	 * left = 3, charAt(left)=a
	 * counter={a=1,c=1,i=1},size=3
	 *
	 * right=5, charAt(right)=i
	 */
	public int longestDistinct(String str, int k) {
		// edge case check
		if (str == null || str.length() == 0) return 0;
		int maxLen = 0;
		int left = 0, right = 0;
		final Map<Character, Integer> counter = new HashMap<>();
		while (right < str.length()) {
			// go to the first index that exeeds k distinct chars
			while (counter.size() <= k && right < str.length()) {
				increment(counter, str.charAt(right++));
			}
			// more than k distinct chars means did not reach the endk
			if (counter.size() > k) {
				maxLen = Integer.max(maxLen, right - left - 1);
			} else {
				// reach the end
				maxLen = Integer.max(maxLen, right - left);
			}
			// remove until k distinct chars left
			while (counter.size() > k) {
				decrement(counter, str.charAt(left++));
			}
		}
		return maxLen;
	}

	private void decrement(Map<Character, Integer> counter, char ch) {
		int count = counter.get(ch);
		if (count == 1) {
			counter.remove(ch);
		} else {
			counter.put(ch, count - 1);
		}
	}

	private void increment(Map<Character, Integer> counter, char ch) {
		int count = counter.getOrDefault(ch, 0);
		counter.put(ch, count + 1);
	}
}
