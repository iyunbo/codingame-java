package io.github.iyunbo.sliding.window.min.sum.subarray;

/**
 * Problem Statement #
 * Given an array of positive numbers and a positive number ‘sum’ find the length of the smallest contiguous subarray whose sum is greater than or equal to ‘sum’.
 * Return 0 if no such subarray exists.
 */
public class Solution {

	/*
	arr=[2, 1, 5, 2, 3, 2], sum=7
	currentSum=7
	left=3, right=6
	arr[left]=3, arr[right]=
	minLen=2
	 */
	public int minSubarray(int sum, int[] arr) {
		int minLen = arr.length + 1;
		int currentSum = arr[0];
		int left = 0, right = left + 1;
		while (left < arr.length) {
			if (currentSum < sum && right < arr.length) {
				currentSum += arr[right++];
			} else {
				currentSum -= arr[left++];
			}
			if (currentSum >= sum) {
				minLen = Integer.min(minLen, right - left);
			}
		}
		return minLen == arr.length + 1 ? 0 : minLen;
	}
}
