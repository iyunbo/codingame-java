package io.github.iyunbo.sliding.window.longest.substring.with.same.letter.after.replacement;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a string with lowercase letters only, if you are allowed to replace no more than ‘k’ letters with any letter,
 * find the length of the longest substring having the same letters after replacement.
 */
public class Solution {

	public int findLength(String str, int k) {
		if (str == null || str.isEmpty()) {
			return 0;
		}
		int n = str.length(), left = 0, maxLen = 0, maxRepeatCount = 0;
		Map<Character, Integer> counter = new HashMap<>();
		for (int right = 0; right < n; right++) {
			// expanding
			char rightChar = str.charAt(right);
			counter.put(rightChar, counter.getOrDefault(rightChar, 0) + 1);
			maxRepeatCount = Integer.max(maxRepeatCount, counter.get(rightChar));

			// shrinking
			if (right - left + 1 - maxRepeatCount > k) {
				char leftChar = str.charAt(left++);
				counter.put(leftChar, counter.get(leftChar) - 1);
			}
			maxLen = Integer.max(maxLen, right - left + 1);
		}

		return maxLen;
	}


}
