package io.github.iyunbo.sliding.window.kaverage;

/**
 * Given an array, find the average of all contiguous subarrays of size ‘K’ in it.
 * <p>
 * Let’s understand this problem with a real input:
 * Array: [1, 3, 2, 6, -1, 4, 1, 8, 2], K=5
 * Here, we are asked to find the average of all contiguous subarrays of size ‘5’ in the given array. Let’s solve this:
 * <p>
 * For the first 5 numbers (subarray from index 0-4), the average is: (1+3+2+6-1)/5 => 2.2(1+3+2+6−1)/5=>2.2
 * The average of next 5 numbers (subarray from index 1-5) is: (3+2+6-1+4)/5 => 2.8(3+2+6−1+4)/5=>2.8
 * For the next 5 numbers (subarray from index 2-6), the average is: (2+6-1+4+1)/5 => 2.4(2+6−1+4+1)/5=>2.4
 */
public class Solution {

	/*
	arr=1,2,3,4,5,6  K=2
	left=4, right=6
	sum=11
	avgs=[1.5,2.5,3.5,4.5,5.5]
	T = O(N), S = O(N - K)
	 */
	public double[] average(int K, int[] arr) {
		if (arr == null || arr.length < K) return new double[0];

		int left = 0, right = 0;
		int sum = 0;
		double[] avgs = new double[arr.length - K + 1];

		while (right < arr.length) {
			if (right - left < K) {
				sum += arr[right++];
			} else {
				avgs[left] = sum * 1.0 / K;
				sum = sum - arr[left++] + arr[right++];
			}
		}
		// add last avg
		avgs[left] = sum * 1.0 / K;

		return avgs;
	}
}
