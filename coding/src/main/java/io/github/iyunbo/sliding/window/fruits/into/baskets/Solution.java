package io.github.iyunbo.sliding.window.fruits.into.baskets;

import java.util.HashMap;

/**
 * Problem Statement #
 * Given an array of characters where each character represents a fruit tree, you are given two baskets,
 * and your goal is to put maximum number of fruits in each basket. The only restriction is that each basket can have only one type of fruit.
 * <p>
 * You can start with any tree, but you can’t skip a tree once you have started.
 * You will pick one fruit from each tree until you cannot, i.e., you will stop when you have to pick from a third fruit type.
 * <p>
 * Write a function to return the maximum number of fruits in both the baskets.
 */
public class Solution {

	/*
	trees = ['A', 'B', 'C', 'A', 'C']
	n = 5
	maxFruits = 2
	counter={C=1,A=1,C=1}
	left = 2, ch=B
	right = 5, ch=C
	T=O(N)
	S=O(1)
	 */
	public int maxFruits(char[] trees) {
		if (trees == null || trees.length == 0) return 0;

		int maxFruits = 0;
		int n = trees.length;
		int left = 0;
		Counter counter = new Counter();
		for (int right = 0; right < n; right++) {
			counter.addOne(trees[right]);
			while (counter.size() == 3) {
				counter.removeOne(trees[left++]);
			}
			maxFruits = Integer.max(maxFruits, right - left + 1);
		}

		return maxFruits;
	}

	private class Counter extends HashMap<Character, Integer> {

		public Integer get(Character key) {
			return this.getOrDefault(key, 0);
		}

		public void addOne(Character key) {
			this.put(key, this.get(key) + 1);
		}

		public void removeOne(Character key) {
			this.put(key, this.get(key) - 1);
			if (this.get(key) <= 0) {
				this.remove(key);
			}
		}
	}
}
