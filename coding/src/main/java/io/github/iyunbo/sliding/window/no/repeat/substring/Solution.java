package io.github.iyunbo.sliding.window.no.repeat.substring;

import java.util.HashSet;
import java.util.Set;

/**
 * Problem Statement #
 * Given a string, find the length of the longest substring, which has no repeating characters.
 */
public class Solution {
	/*
	"aabccbb" => "abc"
	"abbbbb" => "ab"
	"aaaabbbbbcccc" => "ab" or "bc"
	"" = > ""
	T = O(N)
	S = O(C)
	 */
	public int longestNoRepeatSubstr(String str) {
		if (str == null || str.length() == 0) return 0;
		int maxLen = 0;

		int left = 0, n = str.length();
		Set<Character> seen = new HashSet<>();
		for (int right = 0; right < n; right++) {
			while (seen.contains(str.charAt(right))) {
				seen.remove(str.charAt(left++));
			}
			seen.add(str.charAt(right));
			maxLen = Integer.max(maxLen, right - left + 1);
		}

		return maxLen;
	}
}
