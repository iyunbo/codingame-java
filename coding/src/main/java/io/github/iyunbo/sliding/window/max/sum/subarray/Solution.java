package io.github.iyunbo.sliding.window.max.sum.subarray;

/**
 * Problem Statement #
 * Given an array of positive numbers and a positive number ‘k,’
 * find the maximum sum of any contiguous subarray of size ‘k’.
 */
public class Solution {
	/*
	arr=1,2,3,4,5,6  k=2
	left=4, right=6, sum=11, maxSum=11
	T = O(N), S=O(1)
	 */
	public int maxSumSubArray(int k, int[] arr) {
		if (arr == null || arr.length < k)
			throw new IllegalArgumentException("array size is less than k: " + k);

		int left = 0, right = 0;
		int sum = 0, maxSum = 0;

		while (right < arr.length) {
			if(right - left < k){
				sum += arr[right++];
			}
			maxSum = Integer.max(maxSum, sum);
			sum = sum - arr[left++] + arr[right++];
		}
		maxSum = Integer.max(maxSum, sum);

		return maxSum;
	}
}
