package io.github.iyunbo.graph.islands.cut.trees;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;

public class Solution {
	private int xLimit;
	private int yLimit;

	private static class Place implements Comparable<Place> {
		private final int treeHeight;
		private final int x;
		private final int y;

		Place(int height, int x, int y) {
			this.treeHeight = height;
			this.x = x;
			this.y = y;
		}

		@Override
		public int compareTo(Place place) {
			if (place == null) return -1;
			return Integer.compare(this.treeHeight, place.treeHeight);
		}

		@Override
		public int hashCode() {
			return Objects.hash(this.treeHeight);
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof Place)) return false;
			return Objects.equals(this.treeHeight, ((Place) obj).treeHeight);
		}

		@Override
		public String toString() {
			return "{" + treeHeight + '}';
		}
	}

	/*
	[
	[1,2,3,4],
	[0,0,0,5],
	[9,8,7,6]
	]
	p=[4,5,6,7,8,9]
	c=3
	d=3
	n=4
	 */
	public int cutOffTree(List<List<Integer>> forest) {
		if (forest.isEmpty() || forest.get(0).isEmpty()) return -1;

		xLimit = forest.size();
		yLimit = forest.get(0).size();

		final Queue<Place> placesWithTrees = getOrderedTreePositions(forest);

		Place currentPlace = new Place(forest.get(0).get(0), 0, 0);
		int distance = 0;
		//T = O(N*N), worst case: move from every cell
		//S = O(N), worst case: enqueue every cell
		while (!placesWithTrees.isEmpty()) {
			final Place nextPlace = placesWithTrees.remove();
			final int dist = distance(currentPlace, nextPlace, forest);
			if (dist < 0) {
				return -1;
			}
			distance += dist;
			currentPlace = nextPlace;
		}

		return distance;
	}

	private LinkedList<Place> getOrderedTreePositions(List<List<Integer>> forest) {
		final LinkedList<Place> placesWithTrees = new LinkedList<>();

		for (int x = 0; x < forest.size(); x++) {
			List<Integer> line = forest.get(x);
			for (int y = 0; y < line.size(); y++) {
				if (line.get(y) > 1) {
					placesWithTrees.add(new Place(line.get(y), x, y));
				}
			}
		}

		Collections.sort(placesWithTrees);
		return placesWithTrees;
	}

	/*
	[
	[1,6,3,4],
	[0,0,0,0],
	[9,8,7,2]
	]
	t=2
	s=[1,6,3,4]
	q=[]
	n=[]
	d=-1
	 */
	// T = O(N), worst case: visit every cell
	// S = O(N), worst case: enqueue every cell
	private int distance(Place origine, Place target, List<List<Integer>> forest) {
		int distance = 0;
		final Queue<Place> levelQueue = new LinkedList<>();
		final Set<Place> seen = new HashSet<>();
		final Set<Place> nextLevel = new HashSet<>();
		levelQueue.add(origine);

		loop:
		while (!levelQueue.isEmpty()) {
			while (!levelQueue.isEmpty()) {
				final Place current = levelQueue.remove();
				seen.add(current);
				if (current.compareTo(target) == 0) {
					break loop;
				}
				nextLevel.addAll(getNextSteps(seen, current, forest));
			}
			if (nextLevel.isEmpty()) return -1;
			levelQueue.addAll(nextLevel);
			nextLevel.clear();
			distance++;
		}

		return distance;
	}

	private List<Place> getNextSteps(Set<Place> seen, Place place, List<List<Integer>> forest) {
		final List<Place> next = new ArrayList<>();
		addNextValidPlace(seen, forest, next, place.x - 1, place.y);
		addNextValidPlace(seen, forest, next, place.x, place.y - 1);
		addNextValidPlace(seen, forest, next, place.x + 1, place.y);
		addNextValidPlace(seen, forest, next, place.x, place.y + 1);
		return next;
	}

	private void addNextValidPlace(Set<Place> seen,
								   List<List<Integer>> forest,
								   List<Place> next,
								   int newX, int newY) {
		if (newX >= 0 && newX < xLimit && newY >= 0 && newY < yLimit) {
			final Integer treeHeight = forest.get(newX).get(newY);
			final Place newPlace = new Place(treeHeight, newX, newY);
			if (treeHeight > 0 && !seen.contains(newPlace)) {
				next.add(newPlace);
			}
		}
	}
}
