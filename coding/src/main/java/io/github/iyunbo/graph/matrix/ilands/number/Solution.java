package io.github.iyunbo.graph.matrix.ilands.number;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import io.github.iyunbo.other.util.Pair;

public class Solution {

	private final Set<Pair<Integer, Integer>> visited = new HashSet<>();

	public int numIslands(char[][] grid) {
		final Queue<Pair<Integer, Integer>> ones = getOnes(grid);
		int num = 0;
		while (!ones.isEmpty()) {
			final Pair<Integer, Integer> one = ones.remove();
			this.visited.add(one);
			final Set<Pair<Integer, Integer>> neigners = getNextOnes(one, grid);
			neigners.forEach(ones::remove);
			num++;
		}
		return num;
	}


	private Set<Pair<Integer, Integer>> getNextOnes(Pair<Integer, Integer> one, char[][] grid) {
		final Set<Pair<Integer, Integer>> lands = new HashSet<>();
		final int xlimit = grid.length;
		final int ylimit = xlimit == 0 ? 0 : grid[0].length;

		if (xlimit > one.left() - 1 && one.left() - 1 >= 0) {
			addIfNewLand(lands, Pair.of(one.left() - 1, one.right()), grid);
		}
		if (ylimit > one.right() - 1 && one.right() - 1 >= 0) {
			addIfNewLand(lands, Pair.of(one.left(), one.right() - 1), grid);
		}
		if (xlimit > one.left() + 1 && one.left() + 1 >= 0) {
			addIfNewLand(lands, Pair.of(one.left() + 1, one.right()), grid);
		}
		if (ylimit > one.right() + 1 && one.right() + 1 >= 0) {
			addIfNewLand(lands, Pair.of(one.left(), one.right() + 1), grid);
		}
		if (!lands.isEmpty()) {
			final List<Pair<Integer, Integer>> neighbers = lands
					.stream()
					.map(p -> getNextOnes(p, grid))
					.flatMap(Set::stream)
					.collect(Collectors.toList());
			lands.addAll(neighbers);
		}
		return lands;
	}

	private void addIfNewLand(Set<Pair<Integer, Integer>> result, Pair<Integer, Integer> coord, char[][] grid) {
		if (!this.visited.contains(coord) && grid[coord.left()][coord.right()] == '1') {
			result.add(coord);
		}
		this.visited.add(coord);
	}

	private Queue<Pair<Integer, Integer>> getOnes(char[][] grid) {
		final Queue<Pair<Integer, Integer>> ones = new LinkedList<>();
		for (int x = 0; x < grid.length; x++) {
			for (int y = 0; y < grid[x].length; y++) {
				if (grid[x][y] == '1') {
					ones.add(Pair.of(x, y));
				}
			}
		}
		return ones;
	}
}
