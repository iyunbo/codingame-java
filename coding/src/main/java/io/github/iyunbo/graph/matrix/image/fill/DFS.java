package io.github.iyunbo.graph.matrix.image.fill;

public class DFS {
	public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
		final int color = image[sr][sc];
		if (color != newColor) dfs(image, sr, sc, color, newColor);
		return image;
	}

	// T = O(N), worst case: very cell needs to be filled
	// S = O(N), idem
	private void dfs(int[][] image, int r, int c, int color, int newColor) {
		if (image[r][c] == color) {
			image[r][c] = newColor;
			if (r >= 1) dfs(image, r - 1, c, color, newColor);
			if (c >= 1) dfs(image, r, c - 1, color, newColor);
			if (r < image.length - 1) dfs(image, r + 1, c, color, newColor);
			if (c < image[0].length - 1) dfs(image, r, c + 1, color, newColor);
		}
	}
}
