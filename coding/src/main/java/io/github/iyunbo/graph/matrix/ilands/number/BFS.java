package io.github.iyunbo.graph.matrix.ilands.number;

import java.util.LinkedList;
import java.util.Queue;

import io.github.iyunbo.other.util.Pair;

public class BFS {

	public int numIslands(char[][] grid) {
		int num = 0;
		if (grid == null || grid.length == 0) {
			return num;
		}
		final Queue<Pair<Integer, Integer>> queue = new LinkedList<>();
		for (int x = 0; x < grid.length; x++) {
			for (int y = 0; y < grid[x].length; y++) {
				if (grid[x][y] == '1') {
					++num;
					queue.add(Pair.of(x, y));
					while (!queue.isEmpty()) {
						Pair<Integer, Integer> one = queue.remove();
						if (grid[one.left()][one.right()] == '1') {
							grid[one.left()][one.right()] = '0';
							if (one.left() - 1 >= 0) {
								queue.add(Pair.of(one.left() - 1, one.right()));
							}
							if (one.right() - 1 >= 0) {
								queue.add(Pair.of(one.left(), one.right() - 1));
							}
							if (one.left() + 1 < grid.length) {
								queue.add(Pair.of(one.left() + 1, one.right()));
							}
							if (one.right() + 1 < grid[x].length) {
								queue.add(Pair.of(one.left(), one.right() + 1));
							}
						}
					}
				}
			}
		}
		return num;
	}


}
