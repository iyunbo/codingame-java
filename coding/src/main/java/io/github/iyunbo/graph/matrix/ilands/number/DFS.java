package io.github.iyunbo.graph.matrix.ilands.number;

public class DFS {

	public int numIslands(char[][] grid) {
		int num = 0;
		if (grid == null || grid.length == 0) {
			return num;
		}
		for (int x = 0; x < grid.length; x++) {
			for (int y = 0; y < grid[x].length; y++) {
				if (grid[x][y] == '1') {
					++num;
					dfs(grid, x, y);
				}
			}
		}
		return num;
	}

	private void dfs(char[][] grid, int x, int y) {
		if (x < 0 || x >= grid.length) {
			return;
		}
		if (y < 0 || y >= grid[x].length) {
			return;
		}
		if (grid[x][y] == '1') {
			grid[x][y] = '0';
			dfs(grid, x + 1, y);
			dfs(grid, x, y + 1);
			dfs(grid, x - 1, y);
			dfs(grid, x, y - 1);
		}
	}

}
