package io.github.iyunbo.graph.matrix.image.rotate;

public class Solution {
	public void rotate(int[][] matrix) {
		final int n = matrix.length;
		for (int layer = 0; layer < n / 2; layer++) {
			for (int y = layer; y < n - layer - 1; y++) {
				rotate4Numbers(matrix, layer, y, n - 2 * layer);
			}
		}
	}

	protected void rotate4Numbers(int[][] matrix, int layer, int startY, int len) {
		int[] coord = new int[]{layer, startY};
		int val = matrix[coord[0]][coord[1]];
		for (int i = 0; i < 4; i++) {
			rotate90(coord, len, layer);
			int swap = matrix[coord[0]][coord[1]];
			matrix[coord[0]][coord[1]] = val;
			val = swap;
		}
	}

	protected void rotate90(int[] coord, int n, int layer) {
		coord[0] -= layer;
		coord[1] -= layer;
		int swap = coord[0];
		coord[0] = coord[1] + layer;
		coord[1] = swap;
		coord[1] = n - coord[1] - 1 + layer;
	}
}

/*
	[1, 2, 3]
	[4, 5, 6]
	[7, 8, 9]

 ==>

 	[7, 4, 1]
	[8, 5, 2]
	[9, 6, 3]

*/