package io.github.iyunbo.graph.matrix.image.fill;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;

public class Solution {
	static class Coordinate {
		private final int x;
		private final int y;

		Coordinate(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public boolean equals(Object other) {
			if (!(other instanceof Coordinate)) return false;
			return Objects.equals(this.x, ((Coordinate) other).x) &&
					Objects.equals(this.y, ((Coordinate) other).y);
		}

		public int hashCode() {
			return Objects.hash(this.x, this.y);
		}
	}

	public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
		if (image == null || image.length == 0 || image[0].length == 0)
			return image;
		final int xLimit = image.length;
		final int yLimit = image[0].length;
		final int matchedColor = image[sr][sc];
		final Set<Coordinate> nextLevel = new HashSet<>();
		final Set<Coordinate> seen = new HashSet<>();
		final Queue<Coordinate> levelQueue = new LinkedList<>();
		levelQueue.add(new Coordinate(sr, sc));

		// T = O(N), worst case: every cell needs to be colored
		// S = O(N + N + N) = O(N)
		while (!levelQueue.isEmpty()) {
			while (!levelQueue.isEmpty()) {
				Coordinate coord = levelQueue.remove();
				if (image[coord.x][coord.y] == matchedColor) {
					image[coord.x][coord.y] = newColor;
					seen.add(coord);
					nextLevel.addAll(getConnectedCells(image, coord, xLimit, yLimit, matchedColor, seen));
				}
			}
			if (nextLevel.isEmpty()) break;
			levelQueue.addAll(nextLevel);
			nextLevel.clear();
		}
		return image;
	}

	// T = O(1)
	// S = O(1)
	private Collection<Coordinate> getConnectedCells(int[][] image, Coordinate coord, int xLimit, int yLimit, int matchedColor, Set<Coordinate> seen) {
		final List<Coordinate> connectedCells = new ArrayList<>();
		for (int[] delta : new int[][]{{-1, 0}, {0, -1}, {1, 0}, {0, 1}}) {
			Coordinate newCoord = new Coordinate(coord.x + delta[0], coord.y + delta[1]);
			if (newCoord.x >= 0 && newCoord.x < xLimit
					&& newCoord.y >= 0 && newCoord.y < yLimit
					&& !seen.contains(newCoord)
					&& image[newCoord.x][newCoord.y] == matchedColor) {
				connectedCells.add(newCoord);
			}
		}
		return connectedCells;
	}
}
