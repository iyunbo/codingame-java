package io.github.iyunbo.strings.plus.one;

public class Solution {
	public int[] plusOne(int[] digits) {
		return plus1(digits, digits.length);
	}

	private int[] plus1(int[] digits, int n) {
		if (n == 1) {
			if (digits[0] == 9) {
				return new int[]{1, 0};
			} else {
				return new int[]{digits[0] + 1};
			}
		}

		int lastDigit = digits[n - 1];
		int plusOne = lastDigit + 1;
		digits[n - 1] = plusOne % 10;

		if (plusOne == 10) {
			int[] prefix = plus1(digits, n - 1);
			int[] result = new int[prefix.length + 1];
			System.arraycopy(prefix, 0, result, 0, prefix.length);
			return result;
		} else {
			int[] result = new int[n];
			System.arraycopy(digits, 0, result, 0, n);
			return result;
		}
	}
}