package io.github.iyunbo.strings.min.window.substring;

import java.util.HashMap;
import java.util.Map;

public class Solution {
	public String minWindow(String s, String t) {
		// check empty inputs
		if(s == null || t == null || t.length() == 0) return "";
		// initialization
		int left = 0;
		int right = left;
		String minWindow = s + "#";
		// the target string map
		final Map<Character, Integer> target = getCharacterToInteger(t);
		// the current window letters counter
		final Map<Character, Integer> counter = new HashMap<>();
		int required = target.size();
		int achieved = 0;
		// loop with sliding window
		while(left < s.length()){
			// current right to right until end or achieved = required
			while(achieved < required && right < s.length()){
				char ch = s.charAt(right);
				if(target.containsKey(ch)){
					counter.put(ch, counter.getOrDefault(ch, 0) + 1);
				}
				// if counter < target: continue, if counter > target: continue, if counter == target: increment
				if(target.containsKey(ch) && counter.get(ch).equals(target.get(ch))) achieved++;
				right++;
			}

			// if achieved and min, then record result
			if(achieved == required && right - left < minWindow.length()){
				minWindow = s.substring(left, right);
			}

			// move left to right one step
			char ch = s.charAt(left);
			if(target.containsKey(ch)){
				int count = counter.get(ch);
				if(count == 1) {
					counter.remove(ch);
				}else{
					counter.put(ch, count -1);
				}
				if(count - 1 < target.get(ch)){
					achieved --;
				}
			}
			left++;

		}

		if(minWindow.length() > s.length()) {
			return "";
		} else {
			return minWindow;
		}

	}

	private Map<Character, Integer> getCharacterToInteger(String t) {
		Map<Character, Integer> target = new HashMap<>();
		for(char ch : t.toCharArray()){
			int count = target.getOrDefault(ch, 0);
			target.put(ch, count + 1);
		}
		return target;
	}
}
