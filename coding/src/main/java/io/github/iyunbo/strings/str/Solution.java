package io.github.iyunbo.strings.str;

public class Solution {
	public int strStr(String haystack, String needle) {
		if (needle.isEmpty()) return 0;
		// T = O((n-m)*m) = O(n*n)
		for (int i = 0; i <= haystack.length() - needle.length(); i++) {
			if (match(haystack, i, needle)) return i;
		}
		return -1;
	}

	private boolean match(String haystack, int start, String needle) {
		for (int j = 0; j < needle.length(); j++) {
			if (haystack.charAt(start + j) != needle.charAt(j)) return false;
		}
		return true;
	}
}
