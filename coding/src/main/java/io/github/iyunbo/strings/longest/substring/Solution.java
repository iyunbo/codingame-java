package io.github.iyunbo.strings.longest.substring;

import java.util.LinkedHashMap;
import java.util.Map;

public class Solution {

	private final Map<Integer, Integer> map = new LinkedHashMap<>();

	public int lengthOfLongestSubstring(final String input) {
		int longest = 0;
		int i = 0;
		char[] chars = input.toCharArray();
		while (i < chars.length) {
			int ch = chars[i];
			if (map.containsKey(ch)) {
				longest = Math.max(longest, map.size());
				i = map.get(ch);
				map.clear();
			} else {
				map.put(ch, i);
			}
			i++;
		}
		return Math.max(longest, map.size());
	}

}
