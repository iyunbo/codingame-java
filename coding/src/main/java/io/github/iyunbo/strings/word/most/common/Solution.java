package io.github.iyunbo.strings.word.most.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution {
	// T = O(N+B), S=O(N+B)
	public String mostCommonWord(String paragraph, String[] banned) {
		final List<String> words = Stream.of(paragraph.split("[!?',;.\\s]+"))
				.map(String::toLowerCase)
				.collect(Collectors.toList());
		int maxFrequency = 0;
		String mostCommonWord = null;
		final Map<String, Integer> counter = new HashMap<>();
		for (String banWord : banned) {
			counter.put(banWord, Integer.MIN_VALUE);
		}
		for (String word : words) {
			int count = counter.getOrDefault(word, 0) + 1;
			counter.put(word, count);
			if (count > maxFrequency) {
				maxFrequency = count;
				mostCommonWord = word;
			}
		}

		return mostCommonWord;

	}
}
