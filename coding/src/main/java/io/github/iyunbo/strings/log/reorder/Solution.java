package io.github.iyunbo.strings.log.reorder;

import java.util.ArrayList;
import java.util.List;

class Solution {
	//logs=["log1 a b c de fg hig", "log2 ", "log3 1 2"]
	// "log 2" ==> letters
	// letters first, in alpha order, digits after, in original order
	// no empty
	// identifier is guaranteed
	// length = [0, 100]
	// return = [0, 100]
	public String[] reorderLogFiles(String[] logs) {
		// extract digit logs
		// S = O(L)
		List<String[]> alphaList = new ArrayList<>();
		List<String> digitList = new ArrayList<>();
		// T = O(L)
		for (String log : logs) {
			String identifier = getId(log);
			if (log.length() == identifier.length()) {
				alphaList.add(new String[]{"", identifier});
			} else {
				addToOneList(identifier, log, alphaList, digitList);
			}
		}
		// sorting
		// T = O(LlogL)
		alphaList.sort((log1, log2) -> {
			String full1 = log1[0] + log1[1];
			String full2 = log2[0] + log2[1];
			if ((full1).equals(full2)) {
				return log1[1].compareTo(log2[1]);
			} else {
				return full1.compareTo(full2);
			}
		});
		// T = O(L)
		// combine
		for (int i = 0; i < logs.length; i++) {
			if (i < alphaList.size()) {
				logs[i] = alphaList.get(i)[1] + " " + alphaList.get(i)[0];
			} else {
				logs[i] = digitList.get(i - alphaList.size());
			}
		}
		return logs;
	}

	private void addToOneList(String identifier, String log, List<String[]> alphaList, List<String> digitList) {
		char firstChar = log.charAt(identifier.length() + 1);
		if (Character.isDigit(firstChar)) {
			digitList.add(log);
		} else {
			alphaList.add(new String[]{log.substring(identifier.length() + 1), identifier});
		}
	}

	/*
	log1 xxx yyy
	id="log1"
	i=3
	ch=' '
	*/
	private String getId(String log) {
		// loop until find space
		StringBuilder id = new StringBuilder();
		int i = 0;
		char ch;
		do {
			ch = log.charAt(i++);
			id.append(ch);
		}
		while (ch != ' ');
		return id.substring(0, i - 1);
	}
}
