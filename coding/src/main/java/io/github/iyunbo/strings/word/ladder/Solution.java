package io.github.iyunbo.strings.word.ladder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import io.github.iyunbo.other.util.Pair;

public class Solution {

	public static final char CHAR_WILDCARD = '*';

	// S = O(W * L * L) + O(W) + O(W)
	public int ladderLength(String beginWord, String endWord, List<String> wordList) {
		// T = O(W * L * L)
		final Map<String, List<String>> comboDict = preprocess(wordList);
		final Set<String> visited = new HashSet<>();
		final Queue<Pair<String, Integer>> wordQueue = new LinkedList<>();
		visit(beginWord, visited, wordQueue, 1);
		// T = O(W * L * W)
		while (!wordQueue.isEmpty()) {
			final Pair<String, Integer> current = wordQueue.remove();
			if (searchNextWords(current, endWord, wordQueue, comboDict, visited)) {
				return current.right() + 1;
			}
		}
		return 0;
	}

	private void visit(String beginWord, Set<String> visited, Queue<Pair<String, Integer>> wordQueue, int level) {
		wordQueue.add(Pair.of(beginWord, level));
		visited.add(beginWord);
	}

	// T = O(L*W)
	private boolean searchNextWords(Pair<String, Integer> current,
									String endWord,
									Queue<Pair<String, Integer>> wordQueue,
									Map<String, List<String>> comboDict,
									Set<String> visited) {
		final String currentWord = current.left();
		final Integer currentLevel = current.right();
		for (String genericWord : getGenericForms(currentWord)) {
			final List<String> nextWords = comboDict.getOrDefault(genericWord, List.of());
			for (final String word : nextWords) {
				if (!visited.contains(word)) {
					if (endWord.equals(word)) {
						return true;
					} else {
						visit(word, visited, wordQueue, currentLevel + 1);
					}
				}
			}
		}
		return false;
	}

	private Map<String, List<String>> preprocess(List<String> wordList) {
		final Map<String, List<String>> dict = new HashMap<>();
		for (String word : wordList) {
			for (String genericWord : getGenericForms(word)) {
				dict.putIfAbsent(genericWord, new ArrayList<>());
				dict.get(genericWord).add(word);
			}
		}
		return dict;
	}

	private List<String> getGenericForms(String word) {
		final char[] chars = word.toCharArray();
		final List<String> result = new ArrayList<>();
		for (int i = 0; i < word.length(); i++) {
			char tmp = chars[i];
			chars[i] = CHAR_WILDCARD;
			result.add(String.copyValueOf(chars));
			chars[i] = tmp;
		}
		return result;
	}

}
