package io.github.iyunbo.strings.longest.substring;

public class SlidingWindow {


	public int lengthOfLongestSubstring(final String input) {
		char[] chars = input.toCharArray();
		int len = chars.length;
		final int[] window = new int[128];
		int max = 0, i = 0, j = 0;
		while (i < len && j < len) {
			if (window[chars[j]] == 0) {
				window[chars[j++]] = 1;
			} else {
				max = Math.max(max, j - i);
				window[chars[i++]] = 0;
			}
		}
		return Math.max(max, j - i);
	}
}
