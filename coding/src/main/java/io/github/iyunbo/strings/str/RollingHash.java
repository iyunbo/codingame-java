package io.github.iyunbo.strings.str;

public class RollingHash {

	private final static long MODULO = Integer.MAX_VALUE + 1L;
	private final static char HEAD_CHAR = 'a';
	private final static char TAIL_CHAR = 'z';
	private final static int BASE = TAIL_CHAR - HEAD_CHAR + 1;

	public int strStr(String haystack, String needle) {
		final long targetHash = hash(needle, needle.length());
		if (needle.length() > haystack.length()) return -1;
		long currentHash = hash(haystack, needle.length());
		if (currentHash == targetHash) return 0;

		long highestFactor = 1;
		for (int i = 0; i < needle.length(); i++)
			highestFactor = highestFactor * BASE % MODULO;
		// O(T) = O(n - L) = O(n)
		for (int i = 1; i <= haystack.length() - needle.length(); i++) {
			int lastVal = charToInt(haystack.charAt(i - 1));
			int newVal = charToInt(haystack.charAt(i + needle.length() - 1));
			currentHash = currentHash * BASE - highestFactor * lastVal + newVal;
			currentHash %= MODULO;
			if (currentHash == targetHash) return i;
		}
		return -1;
	}

	protected long hash(String str, int end) {
		long hash = 0L;
		for (int i = 0; i < end; i++) {
			hash = hash * BASE + charToInt(str.charAt(i));
			hash %= MODULO;
		}
		return hash;
	}

	private int charToInt(char ch) {
		return ch - HEAD_CHAR;
	}
}
