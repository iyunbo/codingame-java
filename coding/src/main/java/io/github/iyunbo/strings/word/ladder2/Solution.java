package io.github.iyunbo.strings.word.ladder2;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
- same length
- if no path: return []
- wordList has non dup
- only lower letters
- begin != end, non empty
- one letter can change at a time
- transformation must exit
*/
public class Solution {

	private final List<List<String>> result = new ArrayList<>();
	private int minPath;

	public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
		result.clear();
		minPath = Integer.MAX_VALUE;
		// dict of generic format: hit -> h*t, *it, hi*
		final Map<String, Set<String>> dict = new HashMap<>();
		final Set<String> seen = new HashSet<>();

		for (String word : wordList) {
			insertWildcardToWords(dict, word);
		}

		// dfs
		final Deque<String> path = new LinkedList<>();
		path.add(beginWord);
		dfs(path, seen, dict, endWord);

		return result;
	}

	private void dfs(Deque<String> path, Set<String> seen, Map<String, Set<String>> dict, String endWord) {
		String lastWord = path.getLast();
		seen.add(lastWord);
		if (lastWord.equals(endWord)) {
			if (path.size() < minPath) {
				minPath = path.size();
				result.clear();
				result.add(List.copyOf(path));
			} else if (path.size() == minPath) {
				result.add(List.copyOf(path));
			}
		} else if (path.size() < minPath) {
			for (String generic : generateGeneric(lastWord)) {
				for (String newWord : dict.getOrDefault(generic, Set.of())) {
					if (!seen.contains(newWord)) {
						path.add(newWord);
						dfs(path, seen, dict, endWord);
						path.removeLast();
					}
				}
			}
		}
		seen.remove(lastWord);
	}

	private void insertWildcardToWords(Map<String, Set<String>> dict, String word) {
		for (String genericForm : generateGeneric(word)) {
			dict.putIfAbsent(genericForm, new HashSet<>());
			Set<String> words = dict.get(genericForm);
			words.add(word);
		}
	}

	private List<String> generateGeneric(String word) {
		final List<String> result = new ArrayList<>();
		char[] chars = word.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			char tmp = chars[i];
			chars[i] = '*';
			String genericForm = String.valueOf(chars);
			result.add(genericForm);
			chars[i] = tmp;
		}
		return result;
	}


}