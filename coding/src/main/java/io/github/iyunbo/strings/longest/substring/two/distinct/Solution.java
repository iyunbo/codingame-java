package io.github.iyunbo.strings.longest.substring.two.distinct;

import java.util.HashMap;
import java.util.Map;

/*
input: "aaaabaaaaabcde"
input: "aaaaaaaaaaaa"
*/
class Solution {

	private final Map<Character, Integer> counter = new HashMap<>();

	/*
	s=ccaabbb : 6
	left=4
	right=7
	len=5
	counter=a -> 2, b -> 3
	T = O(N), S=O(1)
	*/
	public int lengthOfLongestSubstringTwoDistinct(String s) {
		if(s.length() <= 2) return s.length();

		int left = 0;
		int right = 1;
		int len = 0;
		counter.clear();
		counter.put(s.charAt(0), 1);
		while(right < s.length()){
			right = findNextRight(s, right);
			len = Integer.max(right - left, len);
			left = findNextLeft(s, left);
		}

		return len;
	}

	private int findNextRight(String s, int right){
		char ch = ' ';
		while(right < s.length() && counter.size() < 3){
			ch = s.charAt(right++);
			counter.put(ch, counter.getOrDefault(ch, 0) + 1);
		}
		if(counter.size() == 3){
			right --;
			counter.remove(ch);
		}
		return right;
	}

	private int findNextLeft(String s, int left){
		while(counter.size() == 2){
			char ch = s.charAt(left ++);
			int count = counter.get(ch);
			count--;
			if(count == 0){
				counter.remove(ch);
			}else{
				counter.put(ch, count);
			}
		}
		return left;
	}
}
