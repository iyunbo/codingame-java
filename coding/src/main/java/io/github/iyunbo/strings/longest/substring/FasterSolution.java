package io.github.iyunbo.strings.longest.substring;


class FasterSolution {
	public int lengthOfLongestSubstring(String s) {
		char[] arr = s.toCharArray();
		if (arr.length <= 1) return arr.length;
		int max = 1;
		int start = 0;
		for (int end = 1; end < arr.length; end++) {
			for (int i = start; i < end; i++) {
				if (arr[i] == arr[end]) {
					start = i + 1;
					break;
				}
			}
			max = Math.max(max, end - start + 1);
		}
		return max;
	}
}
