package io.github.iyunbo.strings.multipy;

public /*
num1, num2 = only digits ? yes
num1, num2 empty ? no
num1, num2 = "0" ? yes
num1.length, num2.length <= 200
*/
class Solution {
	public String multiply(String num1, String num2) {
		int i = 0;
		String ans = "";
		while (i < num1.length()) {
			String res = multiply(num1.charAt(i) - '0', num2);
			ans = multiply(10, ans);
			ans = add(ans, res);
			i++;
		}

		return ans;
	}

	private String add(String n1, String n2) {
		StringBuilder result = new StringBuilder();

		int i = 0;
		int carrier = 0;
		int v1, v2;
		while (i < n1.length() || i < n2.length()) {
			if (i < n1.length()) {
				v1 = n1.charAt(n1.length() - i - 1) - '0';
			} else {
				v1 = 0;
			}
			if (i < n2.length()) {
				v2 = n2.charAt(n2.length() - i - 1) - '0';
			} else {
				v2 = 0;
			}
			int val = v1 + v2 + carrier;
			result.insert(0, val % 10);
			carrier = val / 10;
			i++;
		}
		if (carrier > 0) {
			result.insert(0, carrier);
		}

		return result.toString();
	}

	private String multiply(int n, String num) {
		if(n == 0) return "0";
		int i = num.length() - 1;
		int carrier = 0;
		StringBuilder result = new StringBuilder();
		while (i >= 0) {
			int val = carrier + n * (num.charAt(i) - '0');
			result.insert(0, val % 10);
			carrier = val / 10;
			i--;
		}
		if (carrier > 0) {
			result.insert(0, carrier);
		}
		return result.toString();
	}


}