package io.github.iyunbo.search.meeting.rooms;

import java.util.Arrays;
import java.util.Comparator;

public class Solution {
	/*
	{0, 30}, {5, 10}, {15, 20}, {16, 23}, {21, 25}, {12, 30}
	0----------------------------------------------30
	     5----------10
	     	            12-------------------------30
	                      15-----20
	                         16---------23
	                               21-------25

	 */
	// T = O(N*N)
	// S = O(1)
	public int minMeetingRooms(int[][] intervals) {
		if (intervals.length == 0) return 0;

		// T = O(N*log(N))
		Arrays.sort(intervals, Comparator.comparing(i -> i[0]));

		int maxOverlap = 0;
		for (int i = 0; i < intervals.length; i++) {
			int overlap = 0;
			for (int j = 0; j < intervals.length; j++) {
				// small optimization: save some overlap checks
				if (intervals[j][0] > intervals[i][1]) break;
				if (i != j) {
					if (inside(intervals[i][0], intervals[j])) {
						overlap++;
					}
				}
			}
			maxOverlap = Integer.max(maxOverlap, overlap);
		}
		return maxOverlap + 1;
	}

	private boolean inside(int edge, int[] interval) {
		return edge >= interval[0] && edge < interval[1];
	}
}
