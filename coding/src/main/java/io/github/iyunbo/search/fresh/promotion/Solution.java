package io.github.iyunbo.search.fresh.promotion;

public class Solution {
	public int winPrize(String[][] codeList, String[] shoppingCart) {
		int iGroup = 0;
		int iCode = 0;
		if (codeList == null || codeList.length == 0) return 1;
		if (shoppingCart == null || shoppingCart.length == 0) return 0;

		//T = O(C), S=(1)
		for (String fruit : shoppingCart) {
			if (match(fruit, codeList[iGroup][iCode])) {
				iCode++;
				if (iCode == codeList[iGroup].length) {
					iGroup++;
					iCode = 0;
				}
				if (iGroup == codeList.length) return 1;
			} else {
				iCode = 0;
			}
		}
		return 0;
	}

	private boolean match(String fruit, String code) {
		return fruit.equals(code) || code.equals("anything");
	}
}
