package io.github.iyunbo.search.rotate.array;

public class Solution {
	public int search(int[] nums, int target) {
		return searchRotate(nums, 0, nums.length, target);
	}

	/*
	nums=[4,5,6,7,0,1,2]
	t=0
	s=3->7
	e=5->0
	n/2=1
	 */
	// T= O(log(N))
	// S= O(1)
	private int searchRotate(int[] nums, int start, int end, int target) {
		if (start >= end) return -1;
		final int startVale = nums[start];
		final int endValue = nums[end - 1];
		if (target == startVale) return start;
		if (target == endValue) return end - 1;
		final int n = end - start;

		if (target < startVale) {
			if (nums[start + n / 2] > nums[start]) {
				start = start + n / 2;
			} else {
				start = start + 1;
			}
			end = end - 1;
		} else {
			if (nums[start + n / 2] > target) {
				end = start + n / 2;
			} else {
				end = end - 1;
			}
			start = start + 1;
		}

		return searchRotate(nums, start, end, target);
	}
}
