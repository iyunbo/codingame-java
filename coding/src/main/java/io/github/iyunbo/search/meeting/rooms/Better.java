package io.github.iyunbo.search.meeting.rooms;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Better {

	public int minMeetingRooms(int[][] intervals) {
		if (intervals.length == 0) return 0;

		// T = O(N*log(N))
		Arrays.sort(intervals, Comparator.comparing(i -> i[0]));

		// T = O(N)
		final PriorityQueue<int[]> meetings = new PriorityQueue<>(Comparator.comparing(i -> i[1]));
		meetings.addAll(Arrays.asList(intervals));

		int allocatedRooms = 0;
		// T = O(N*log(N))
		for (int[] meeting : intervals) {
			int[] earlyMeeting = meetings.peek();
			if (meeting[0] >= earlyMeeting[1]) {
				meetings.remove();
			} else {
				allocatedRooms++;
			}
		}

		return allocatedRooms;
	}

}
