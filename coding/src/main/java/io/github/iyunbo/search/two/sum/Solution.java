package io.github.iyunbo.search.two.sum;

public class Solution {
	/*
	n=[3, 24, 50, 79, 88, 150, 345],t=200
	l=1,r=7
	m=4
	l+m=82
	 */
	// T = O(N)
	// S = O(1)
	public int[] twoSum(int[] numbers, int target) {
		int left = 0;
		int right = numbers.length - 1;
		while (left < right) {
			int sum = numbers[left] + numbers[right];
			if (sum == target) {
				return new int[]{left + 1, right + 1};
			} else if (sum < target) {
				int mid = left + (right - left) / 2;
				if (numbers[mid] + numbers[right] <= target) {
					if (left == mid) mid++;
					left = mid;
				} else {
					left++;
				}
			} else {
				int mid = right - (right - left) / 2;
				if (numbers[left] + numbers[mid] >= target) {
					if (right == mid) mid--;
					right = mid;
				} else {
					right--;
				}
			}
		}
		throw new IllegalArgumentException("the array does not contain valid solution");
	}
}
