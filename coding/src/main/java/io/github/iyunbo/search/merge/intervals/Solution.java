package io.github.iyunbo.search.merge.intervals;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;

public class Solution {
	public int[][] merge(int[][] intervals) {
		// T = O(log(N))
		// S = O(N)
		Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));
		final Deque<int[]> merged = new LinkedList<>();
		for (int[] current : intervals) {
			if (merged.isEmpty()) {
				merged.add(current);
				continue;
			}
			final int[] last = merged.getLast();
			// overlap
			if (current[0] <= last[1]) {
				// choose biggest
				if (current[1] > last[1]) {
					last[1] = current[1];
				}
			} else {
				// next interval to merge
				merged.add(current);
			}
		}

		return merged.toArray(new int[0][]);
	}
}
