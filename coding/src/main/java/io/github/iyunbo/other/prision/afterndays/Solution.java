package io.github.iyunbo.other.prision.afterndays;

import java.util.Arrays;

public class Solution {

	/*
	T = O(K * min(N,2**K)), S = O(2**K)
	 */
	public int[] prisonAfterNDays(int[] cells, int N) {
		final int[] simulation = new int[256];
		Arrays.fill(simulation, -1);

		int i = 0;
		int[] result = cells;
		int num = encode(result);
		while (i < N) {
			if (simulation[num] == -1) {
				simulation[num] = i;
				result = passOneDay(result);
				num = encode(result);
				i++;
			} else {
				int period = i - simulation[num];
				for (int j = 0; j < (N - i) % period; j++) {
					result = passOneDay(result);
				}
				break;
			}
		}
		return result;
	}

	private int[] passOneDay(int[] cells) {
		int[] result = new int[cells.length];
		result[0] = 0;
		result[cells.length - 1] = 0;
		for (int i = 1; i < cells.length - 1; i++) {
			if ((cells[i - 1] + cells[i + 1]) % 2 == 0) {
				result[i] = 1;
			} else {
				result[i] = 0;
			}
		}
		return result;
	}

	int encode(int[] cells) {
		int encoded = 0;
		for (int c : cells) {
			encoded = (encoded << 1) | c;
		}
		return encoded;
	}
}
